#-----------------------------------------------------------------------------
#
# Copyright (C) 2020  Wilhelm Hagemeister Pose <hm@igh.de>
#
# This file is part of the QtPdWidgets library.
#
# The QtPdWidgets library is free software: you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public License as
# published by the Free Software Foundation, either version 3 of the License,
# or (at your option) any later version.
#
# The QtPdWidgets library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser
# General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with the QtPdWidgets Library. If not, see
# <http://www.gnu.org/licenses/>.
#
#
#-----------------------------------------------------------------------------

TEMPLATE = app
TARGET = pdqmltest
QT += charts qml quick quickcontrols2
QT += svg #QSvgRenderer
QT += xml #QDomDocument

#CONFIG+=qml_debug

MOC_DIR = .moc
OBJECTS_DIR = .obj

DEPENDPATH += ..
INCLUDEPATH += ..

# Path for pdWidgets in qml; they go into the ressource file
include(../qml/de_igh_pdQmlWidgets.pri)

LIBDIR = "lib"
contains(QMAKE_HOST.arch, x86_64): {
    LIBDIR = "lib64"
}

!isEmpty(PDCOMPREFIX) {
    INCLUDEPATH += $${PDCOMPREFIX}/include
    LIBS += -L$${PDCOMPREFIX}/lib$${LIBEXT}
}

QMAKE_LFLAGS += -L$$OUT_PWD/..
QMAKE_LFLAGS_APP += -Wl,--rpath -Wl,..

LIBS += -lQtPdWidgets2

LIBS += -lpdcom5 -lexpat

HEADERS += \
    src/QmlBuddy.h \
    src/ScalarSeries.h 
    
SOURCES += \
    src/main.cpp \
    src/QmlBuddy.cpp \
    src/ScalarSeries.cpp

OTHER_FILES += \
    qml/*.qml 



RESOURCES += \
    resources.qrc

target.path = .
