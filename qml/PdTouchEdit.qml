/****************************************************************************
*
* QML-Widgets for qtPdWidgets
*
* Copyright (C) 2021 Wilhelm Hagemeister
* Contact: hm@igh.de
*
* State: similar component in QtPdWidgets
*
* TODO: Documentation
*
*
****************************************************************************/

import QtQuick 2.7         
import QtQuick.Controls 2.3

import de.igh.pd 2.0

/** Digital display and touch edit.
 */

Label {
    id: control
    property alias variable:scalar
    property alias connection:scalar.connection

    property alias path: scalar.path

    property alias decimals:dialog.decimals

    property alias suffix:dialog.suffix

    property alias lowerLimit:dialog.lowerLimit

    property alias upperLimit:dialog.upperLimit

    property string oldText: ""
    property alias value:scalar.value
    property alias title:dialog.title
    
    enabled:scalar.connected
    onDecimalsChanged:{
        scalar.update()
    }
    onSuffixChanged: {
        scalar.update()
    }

    SystemPalette { id: palette }

    TouchEditDialog {
        id:dialog
        onAccepted: {
            scalar.value = value
        }
    }
    PdScalar {
        id:scalar
        function update() {
            control.text = value.toFixed(control.decimals)+control.suffix
        }
        onValueChanged: {
            update()
        }
    }

    background: Rectangle {
        implicitWidth: 100
        implicitHeight: 30
        color:"transparent" //palette.base
        border.color: parent.enabled ? palette.dark:palette.mid
        MouseArea {
            anchors.fill: parent
            onClicked: {         
                dialog.value = scalar.value
                dialog.open() 
            }
        }
    }

    horizontalAlignment: TextInput.AlignRight
    padding:5
}
