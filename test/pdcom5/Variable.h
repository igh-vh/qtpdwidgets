/*****************************************************************************
 *
 * Copyright (C) 2021 Richard Hacker (lerichi at gmx dot net),
 *                    Florian Pose (fp at igh dot de),
 *                    Bjarne von Horn (vh at igh dot de).
 *
 * This file is part of the QtPdWidgets library.
 *
 * The QtPdWidgets library is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * The QtPdWidgets library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser
 * General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with the QtPdWidgets Library. If not, see
 * <http://www.gnu.org/licenses/>.
 *
 ****************************************************************************/

/** @file */

#ifndef PDCOM5_VARIABLE_H
#define PDCOM5_VARIABLE_H

#include "Selector.h"
#include "SizeTypeInfo.h"

#include <string>

/****************************************************************************/

namespace PdCom {

class Process;

class Variable
{
    public:
        Variable();

        template <typename T>
        void setValue(T const &data,
                const Selector &selector = {nullptr}) const
        {
        }

        TypeInfo getTypeInfo() const { return type_info_; }

        bool empty() const noexcept { return empty_; }
        std::string getPath() const { return path_; }

    // mocking interface
    public:
        Variable(Process *, const std::string &);
        void mockValue(double);
        Process *getProcess() const { return process_; }
        void *getMockedData() const { return (void *) &mocked_value_; }

    private:
        bool empty_;
        Process *process_;
        const TypeInfo type_info_;
        std::string path_;
        double mocked_value_;
};

} // namespace PdCom

/****************************************************************************/

#endif
