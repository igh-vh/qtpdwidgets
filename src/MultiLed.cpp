/*****************************************************************************
 *
 * Copyright (C) 2009 - 2012  Florian Pose <fp@igh-essen.com>
 *
 * This file is part of the QtPdWidgets library.
 *
 * The QtPdWidgets library is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * The QtPdWidgets library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser
 * General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with the QtPdWidgets Library. If not, see
 * <http://www.gnu.org/licenses/>.
 *
 ****************************************************************************/

#include "MultiLed.h"

#include <QtGui>
using Pd::MultiLed;

/****************************************************************************/

#define DEFAULT_DIAMETER 12

QColor MultiLed::disconnectColor = Qt::darkGray;
QTimer MultiLed::blinkTimer;

/****************************************************************************/

struct MultiLed::Impl
{
    Impl(MultiLed *parent):
        parent{parent},
        value{0},
        dataPresent{false},
        diameter{DEFAULT_DIAMETER},
        hash{nullptr},
        currentValue{disconnectColor, Value::Steady},
        blinkState{false},
        currentColor{disconnectColor}
    {
    }

    MultiLed * const parent;

    int value; /**< The current value from the process. */
    bool dataPresent; /**< There is a value to display. */

    quint32 diameter; /**< The LED's diameter in pixel. */
    const Hash *hash; /**< Pointer to the ColorHash
                                    to use. */
    Value currentValue; /**< The currently displayed value attributes. */
    bool blinkState; /**< Blink timer. */
    QColor currentColor; /**< Current color. */

    /** Sets the current LED color.
     */
    void setCurrentColor(QColor c)
    {
        if (c != currentColor) {
            currentColor = c;
            parent->update();
        }
    }

    /** Retranslate the widget.
     */
    void retranslate()
    {
        parent->setWindowTitle(Pd::MultiLed::tr("Multi-colored LED"));
    }

};

/****************************************************************************/

/** Constructor.
 */
MultiLed::MultiLed(
        QWidget *parent /**< Parent widget. */
        ): QWidget(parent),
    impl{std::unique_ptr<Impl>{new Impl(this)}}
{
    setSizePolicy(QSizePolicy::Minimum, QSizePolicy::Minimum);

    connect(&blinkTimer, SIGNAL(timeout()), this, SLOT(blinkEvent()));

    impl->retranslate();
}

/****************************************************************************/

/** Destructor.
 */
MultiLed::~MultiLed()
{
}

/****************************************************************************/

bool MultiLed::hasData() const
{
    return impl->dataPresent;
}

/****************************************************************************/

void MultiLed::clearData()
{
    impl->dataPresent = false;
    updateColor();
}

/****************************************************************************/

/**
 * \return The current process #value.
 */
int MultiLed::getValue() const
{
    return impl->value;
}

/****************************************************************************/

/** Sets the current #value.
 *
 * Looks up the LED color and redraws the widget, if necessary.
 */
void MultiLed::setValue(int value)
{
    if (value != impl->value || !impl->dataPresent) {
        impl->value = value;
        impl->dataPresent = true;
        updateColor();
    }
}

/****************************************************************************/

/**
 * \return The LED #diameter.
 */
quint32 MultiLed::getDiameter() const
{
    return impl->diameter;
}

/****************************************************************************/

/** Sets the LED #diameter.
 */
void MultiLed::setDiameter(quint32 diameter)
{
    if (diameter < 4)
        diameter = 4;

    if (diameter != impl->diameter) {
        impl->diameter = diameter;
        setMinimumSize(diameter + 4, diameter + 4);
        update();
    }
}

/****************************************************************************/

/** Resets the LED #diameter.
 */
void MultiLed::resetDiameter()
{
    setDiameter(DEFAULT_DIAMETER);
}

/****************************************************************************/

/** Sets the hash.
 *
 * Setting a hash is mandatory to use a MultiLed.
 */
void MultiLed::setHash(const Hash *h)
{
    if (h != impl->hash) {
        impl->hash = h;
        updateColor();
    }
}

/****************************************************************************/

/** Gives a hint aboute the optimal size.
 */
QSize MultiLed::sizeHint() const
{
    return QSize(impl->diameter + 4, impl->diameter + 4);
}

/****************************************************************************/

/** Determines the new value of #currentColor.
 *
 * If a color hash is applied with setColorHash() and it contains a color for
 * #value, #currentColor is set to that color. Otherwise it is set to
 * Qt::magenta. If the #currentColor changed, the LED is redrawn.
 *
 * \todo default color property
 */
void MultiLed::updateColor()
{
    Value newValue;

    if (impl->dataPresent) {
        if (impl->hash && impl->hash->contains(impl->value)) {
            newValue = impl->hash->value(impl->value);
        } else {
            newValue.color = Qt::magenta;
            newValue.blink = Value::Steady;
        }
    } else {
        newValue.color = disconnectColor;
        newValue.blink = Value::Steady;
    }

    setCurrentValue(newValue);
}

/****************************************************************************/

/** Sets the current LED color.
 */
void MultiLed::setCurrentValue(Value v)
{
    if (v.color != impl->currentValue.color
            || v.blink != impl->currentValue.blink) {
        impl->currentValue = v;

        if (impl->currentValue.blink == Value::Blink) {
            impl->blinkState = false;
            if (!blinkTimer.isActive())
                blinkTimer.start(500);
        } else {
            impl->setCurrentColor(impl->currentValue.color);
        }
    }
}

/****************************************************************************/

/** Event handler.
 */
bool MultiLed::event(
        QEvent *event /**< Paint event flags. */
        )
{
    if (event->type() == QEvent::LanguageChange) {
        impl->retranslate();
    }

    return QWidget::event(event);
}

/****************************************************************************/

/** Paint function.
 */
void MultiLed::paintEvent(
        QPaintEvent *event /**< Paint event flags. */
        )
{
    Q_UNUSED(event);
    QPainter painter(this);
    int xOff, yOff;

    painter.setPen(Qt::black);
    painter.setBrush(impl->currentColor);
    painter.setRenderHint(QPainter::Antialiasing);

    xOff = (width() - 2 - impl->diameter) / 2;
    yOff = (height() - 2 - impl->diameter) / 2;
    painter.drawEllipse(1 + xOff, 1 + yOff, impl->diameter, impl->diameter);
}

/****************************************************************************/

/** This virtual method is called by the ProcessVariable, if its value
 * changes.
 */
void MultiLed::newValues(std::chrono::nanoseconds)
{
    int32_t value;
    PdCom::details::copyData(&value,
            PdCom::details::TypeInfoTraits<int32_t>::type_info.type,
            getData(), getVariable().getTypeInfo().type, 1);
    value = value * scale + offset;
    setValue(value);
}

/****************************************************************************/

/** Blink event slot.
 */
void MultiLed::blinkEvent()
{
    if (impl->currentValue.blink == Value::Blink) {
        impl->blinkState = !impl->blinkState;
        impl->setCurrentColor(impl->blinkState ?
                impl->currentValue.color :
                impl->currentValue.color.darker(300));
    }
}

/****************************************************************************/

/** Overloads the insert function of QHash.
 */
void MultiLed::Hash::insert(
        int value, /**< Process value. */
        QColor c, /**< LED color. */
        Value::BlinkMode b /**< Blink mode. */
        )
{
    QHash<int, Value>::insert(value, Value(c, b));
}

/****************************************************************************/
