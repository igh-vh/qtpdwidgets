/*****************************************************************************
 *
 * Copyright (C) 2022  Florian Pose <fp@igh.de>
 *
 * This file is part of the QtPdWidgets library.
 *
 * The QtPdWidgets library is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * The QtPdWidgets library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser
 * General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with the QtPdWidgets Library. If not, see
 * <http://www.gnu.org/licenses/>.
 *
 ****************************************************************************/

#include "MessageModelTest.h"

#include <QAbstractItemModelTester>

#include "MessageModel.h"
#include "pdcom5/Process.h"

/****************************************************************************/

void MessageModelTest::test()
{
    PdCom::Process process;
    PdCom::Variable busMonitor0(&process, "/Io/BusMonitor/Error/time/0");
    PdCom::Variable busMonitor1(&process, "/Io/BusMonitor/Error/time/1");

    Pd::MessageModel *model = new Pd::MessageModel();

    model->load("plainmessages.xml", "de");
    QCOMPARE(model->rowCount(QModelIndex()), 0);

    model->connect(&process);

    busMonitor0.mockValue(1.0);
    QCOMPARE(model->rowCount(QModelIndex()), 1);
    auto index = model->index(0, 0, QModelIndex());
    QCOMPARE(model->data(index, Qt::DisplayRole),
            "Feldbus gestört an Panel Gelb");

    busMonitor1.mockValue(1.0);
    QCOMPARE(model->rowCount(QModelIndex()), 2);
    index = model->index(0, 0, QModelIndex());
    QCOMPARE(model->data(index, Qt::DisplayRole),
            "Feldbus gestört an Panel Grün");
    index = model->index(1, 0, QModelIndex());
    QCOMPARE(model->data(index, Qt::DisplayRole),
            "Feldbus gestört an Panel Gelb");

    new QAbstractItemModelTester(model, 
            QAbstractItemModelTester::FailureReportingMode::Fatal, this);
}

/****************************************************************************/
