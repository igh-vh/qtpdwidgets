/*****************************************************************************
 *
 * Copyright (C) 2009  Florian Pose <fp@igh-essen.com>
 *
 * This file is part of the QtPdWidgets library.
 *
 * The QtPdWidgets library is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * The QtPdWidgets library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser
 * General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with the QtPdWidgets Library. If not, see
 * <http://www.gnu.org/licenses/>.
 *
 ****************************************************************************/

#include "Time.h"
using Pd::Time;

#include <QtGui>

/****************************************************************************/

#define SECONDS_PER_MINUTE (60.0)
#define SECONDS_PER_HOUR   (SECONDS_PER_MINUTE * 60.0)

/****************************************************************************/

struct Time::Impl
{
    Impl(Time *parent):
        parent{parent},
        dataPresent{false},
        value{0}
    {
    }

    Time * const parent;

    bool dataPresent; /**< \a true, if data was received. */
    double value; /**< Current time value in seconds. */

    /** Displays the current value.
     */
    void outputValue()
    {
        QString output;
        double rest;
        int tmp;
        bool first = true;

        if (dataPresent) {
            if (value >= 0.0) {
                rest = value;
            } else {
                rest = value * -1.0;
                output = "-";
            }

            // hours
            if (rest >= SECONDS_PER_HOUR) {
                tmp = (int) (rest / SECONDS_PER_HOUR);
                rest -= tmp * SECONDS_PER_HOUR;
                output += QString("%1").arg(tmp);
                first = false;
            }

            // minutes
            if (rest >= SECONDS_PER_MINUTE || !first) {
                if (first) {
                    output += "0:";
                    first = false;
                }
                tmp = (int) (rest / SECONDS_PER_MINUTE);
                rest -= tmp * SECONDS_PER_MINUTE;
                output += QString("%1").arg(tmp, 2, 10, QLatin1Char('0'));
            }

            // seconds
            if (first) {
                output += "0:";
            }
            tmp = (int) rest;
            output += QString("%1").arg(tmp, 2, 10, QLatin1Char('0'));
        }

        if (output != parent->text()) {
            parent->setText(output);
        }
    }


    /** Retranslate the widget.
     */
    void retranslate()
    {
        parent->setWindowTitle(Pd::Time::tr("Time display"));
    }

};

/****************************************************************************/

/** Constructor.
 */
Time::Time(
        QWidget *parent /**< parent widget */
        ): QLabel(parent),
    impl{std::unique_ptr<Impl>{new Impl(this)}}
{
    setFrameStyle(QFrame::Box | QFrame::Raised);
    setAlignment(Qt::AlignRight | Qt::AlignVCenter);
    setMinimumSize(40, 25);
    setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Fixed);

    impl->outputValue();
    impl->retranslate();
}

/****************************************************************************/

/** Destructor.
 */
Time::~Time()
{
}

/****************************************************************************/

void Time::clearData()
{
    impl->dataPresent = false;
    impl->outputValue();
}

/****************************************************************************/

/**
 * \return The current #value.
 */
double Time::getValue() const
{
    return impl->value;
}

/****************************************************************************/

/** Sets the current time value.
 */
void Time::setValue(double v)
{
    if (v != impl->value || !impl->dataPresent) {
        impl->dataPresent = true;
        impl->value = v;
        impl->outputValue();
    }
}

/****************************************************************************/

/** Gives a hint aboute the optimal size.
 */
QSize Time::sizeHint() const
{
    return QSize(60, 25);
}

/****************************************************************************/

/** Event handler.
 */
bool Time::event(
        QEvent *event /**< Paint event flags. */
        )
{
    if (event->type() == QEvent::LanguageChange) {
        impl->retranslate();
    }

    return QLabel::event(event);
}

/****************************************************************************/

/** This virtual method is called by the ProcessVariable, if its value
 * changes.
 */
void Time::newValues(std::chrono::nanoseconds)
{
    double value;
    PdCom::details::copyData(&value,
            PdCom::details::TypeInfoTraits<double>::type_info.type,
            getData(), getVariable().getTypeInfo().type, 1);
    value = value * scale + offset;
    setValue(value);
}

/****************************************************************************/
