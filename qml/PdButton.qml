/****************************************************************************
**
** QML-Widgets for qtPdWidgets
**
** Copyright (C) 2021 Wilhelm Hagemeister
** Contact: hm@igh.de
**
** deprecated, see PdPushButton
**
****************************************************************************/

import QtQuick 2.7         
import QtQuick.Controls 2.3

import de.igh.pd 2.0

/**
 * Button: deprecated, use PdPushButton 
 */
Button {
    id: control
    property alias variable:scalar
    property alias connection:scalar.connection

    property alias path: scalar.path

    property bool invert:false
    checkable:true
    property bool event:false //increments the value by one when pressed
                              //works only when checkable is false
    onEventChanged: {
	if(event) {
	    checkable = false
	}
    }

    enabled: scalar.connected

    PdScalar { id:scalar }
    
    // reactions when button is checkable
    checked: ((scalar.value != 0) != invert) && checkable
    onClicked: {
	if(checkable) {
	    scalar.value = (control.checked != invert)
	}
	if(event) {
	    scalar.inc()
	}
    }
    
    //reactions when button is not checkable
    onPressed: {
	if(!checkable && !event) {
	    scalar.value = !invert
	}
    }
    onReleased: {
	if(!checkable && !event) {
	    scalar.value = invert
	}	
    }
}
