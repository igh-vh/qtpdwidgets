/*****************************************************************************
 *
 * Copyright (C) 2021 Richard Hacker (lerichi at gmx dot net),
 *                    Florian Pose (fp at igh dot de),
 *                    Bjarne von Horn (vh at igh dot de).
 *
 * This file is part of the QtPdWidgets library.
 *
 * The QtPdWidgets library is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * The QtPdWidgets library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser
 * General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with the QtPdWidgets Library. If not, see
 * <http://www.gnu.org/licenses/>.
 *
 ****************************************************************************/

#ifndef PDCOM5_SUBSCRIBER_H
#define PDCOM5_SUBSCRIBER_H

#include <chrono>

#include "Exception.h"

/****************************************************************************/

namespace PdCom {

class Subscription;

constexpr struct event_mode_tag
{
} event_mode;

constexpr struct poll_mode_tag
{
} poll_mode;

class Transmission
{
    double interval_;

    static constexpr double checkInterval(double d)
    {
        return d <= 0 ? throw PdCom::InvalidArgument(
                       "period must be greater than zero")
                      : d;
    }

  public:
    constexpr double getInterval() const noexcept { return interval_; }
    template <typename T, typename R>
    constexpr Transmission(std::chrono::duration<T, R> d) :
        interval_(checkInterval(
                std::chrono::duration_cast<std::chrono::duration<double>>(d)
                        .count()))
    {}
    constexpr Transmission(event_mode_tag) noexcept : interval_(0) {}
    constexpr Transmission(poll_mode_tag) noexcept : interval_(-1) {}
    bool operator==(const Transmission &o) const noexcept
    {
        return o.interval_ == interval_;
    }

    static constexpr Transmission fromDouble(double d)
    {
        return d == 0
                ? Transmission(event_mode)
                : (d == -1 ? Transmission(poll_mode)
                           : Transmission(std::chrono::duration<double>(d)));
    }
};

class Subscriber
{
    public:
        Subscriber(const PdCom::Transmission &);
        virtual void stateChanged(const PdCom::Subscription &) = 0;
        virtual void newValues(std::chrono::nanoseconds ts) = 0;
        const Transmission &getTransmission() const noexcept { return td_; }

    private:
        Transmission td_;
};

}  // namespace PdCom

/****************************************************************************/

#endif
