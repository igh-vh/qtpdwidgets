/*****************************************************************************
 *
 * Copyright (C) 2009 - 2013  Florian Pose <fp@igh-essen.com>
 *
 * This file is part of the QtPdWidgets library.
 *
 * The QtPdWidgets library is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * The QtPdWidgets library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser
 * General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with the QtPdWidgets Library. If not, see
 * <http://www.gnu.org/licenses/>.
 *
 ****************************************************************************/

#include "Graph.h"
using Pd::Graph;

#include "ScalarSubscriber.h"
using Pd::ScalarSubscriber;

#include "ValueRing.h"
using Pd::ValueRing;

#include "TimeScale.h"

#include "Scale.h"
using Pd::Scale;

#include <QtGui>
#include <QMenu>

#ifdef DEBUG_PD_GRAPH
#include <QDebug>
#endif


/****************************************************************************/

#define DEFAULT_MODE                  Roll
#define DEFAULT_TIMERANGE             10.0
#define DEFAULT_SCALEMIN              0.0
#define DEFAULT_SCALEMAX              100.0
#define DEFAULT_TRIGGER_LEVEL_MODE    AutoLevel
#define DEFAULT_MANUAL_TRIGGER_LEVEL  0.0
#define DEFAULT_TRIGGER_POSITION      0.1
#define DEFAULT_TRIGGER_TIMEOUT       0.0
#define DEFAULT_GRID_COLOR            (QColor(160, 160, 160))
#define DEFAULT_AUTO_SCALE_WIDTH      (false)

/****************************************************************************/

class Graph::Layer:
    public ScalarSubscriber
{
    public:
        Layer(Graph *, QColor, Graph::State, std::chrono::nanoseconds);

        void clearData();
        const ValueRing<double> &getValues() const;
        void setState(Graph::State);
        void setTimeRange(std::chrono::nanoseconds);
        void paint(QPainter &, double, const Scale &, const QRect &);
        void resizeExtrema(unsigned int);
        void prepareSample(std::chrono::nanoseconds);
        bool hasSampled() const {
            return timeToSample.count() == 0;
        }

    private:
        Graph * const graph; /**< Parent graph. */
        QColor color; /**< Graph color. */
        Graph::State state; /**< Layer state. */
        double value; /**< Current value. */
        bool dataPresent; /**< \a value contains a valid value. */
        ValueRing<double> values; /**< Ring buffer with time/value
                                    pairs. */
        ValueRing<double> savedValues; /**< Buffer for saved values.
                                        */
        std::chrono::nanoseconds timeToSample;
        typedef QPair<double, double> Extrema; /**< Extrema type with
                                                 minimum and maximum.
                                                */
        QVector<Extrema> extrema; /**< Vector containing the extrema
                                    to display. */
        int offset; /**< Current offset in the #extrema buffer. */
        std::chrono::nanoseconds offsetTime; /**< Absolute time of the
                                                  #offset. */
        unsigned int validExtrema; /**< Number of valid extrema. */
        double lastAppendedValue; /**< The last value appended by
                                    appendToExtrema(). */

        void newValues(std::chrono::nanoseconds) override;
        void fillExtrema();
        bool appendToExtrema(std::chrono::nanoseconds, double);
};

/****************************************************************************/

/** Constructor.
 */
Graph::Layer::Layer(
        Graph *parent,
        QColor c,
        Graph::State state,
        std::chrono::nanoseconds timeRange
        ):
    graph(parent),
    color(c),
    state(state),
    value(0.0),
    dataPresent(false),
    offset(0),
    validExtrema(0U),
    lastAppendedValue(0.0)
{
    setTimeRange(timeRange);
}

/****************************************************************************/

void Graph::Layer::clearData()
{
    savedValues.clear();
    validExtrema = 0U;
    dataPresent = false;
}

/****************************************************************************/

/**
 * \return The values currently displayed.
 */
const ValueRing<double> &Graph::Layer::getValues() const
{
    if (graph->getMode() == Graph::Roll && state == Graph::Run) {
        return values;
    } else {
        return savedValues;
    }
}

/****************************************************************************/

void Graph::Layer::setState(Graph::State s)
{
    if (s == state) {
        return;
    }

    state = s;

    if (graph->getMode() == Graph::Roll) {
        if (state == Graph::Run) {
            fillExtrema();
        }
        else {
            savedValues = values; // copy values
        }
    }
}

/****************************************************************************/

void Graph::Layer::setTimeRange(std::chrono::nanoseconds range)
{
#ifdef DEBUG_PD_GRAPH
    qDebug() << this << "layer time range [ns]" << range.count();
#endif
    /* hold some more values than necessary for synchronisation reasons. */
    values.setRange(range + std::chrono::nanoseconds{500000000});
    fillExtrema();
}

/****************************************************************************/

/** Paint the graph layer.
 */
void Graph::Layer::paint(
        QPainter &painter,
        double scaleFactor,
        const Scale &valueScale,
        const QRect &graphRect
        )
{
    int count = extrema.count();
    const Extrema *ext;
    int x, yMin, yMax;

    if (count <= 0) {
        return;
    }

    painter.setPen(color);

    for (x = count - validExtrema; x < count; x++) {
        ext = &extrema[(x + offset + 1) % count];

        if (ext->second < valueScale.getMin()
                || ext->first > valueScale.getMax()) {
            continue;
        }

        if (ext->first >= valueScale.getMin()) {
            yMin = (int) ((ext->first - valueScale.getMin()) * scaleFactor);
        } else {
            yMin = 0;
        }

        if (ext->second <= valueScale.getMax()) {
            yMax = (int) ((ext->second - valueScale.getMin()) * scaleFactor);
        } else {
            yMax = graphRect.height();
        }

        if (yMin < yMax) {
            painter.drawLine(graphRect.left() + x,
                    graphRect.bottom() - yMax,
                    graphRect.left() + x,
                    graphRect.bottom() - yMin);
        }
        else {
            /* Workaround for
             * https://bugreports.qt-project.org/browse/QTBUG-25153
             * which causes one-pixel lines not to be drawn.
             */
            painter.drawPoint(graphRect.left() + x,
                    graphRect.bottom() - yMin);
        }
    }
}

/****************************************************************************/

/** Resizes the #extrema ring buffer.
 */
void Graph::Layer::resizeExtrema(unsigned int length)
{
#ifdef DEBUG_PD_GRAPH
    qDebug() << this << "resize extrema" << length;
#endif
    extrema.resize(length);
    fillExtrema();
}

/****************************************************************************/

/** Copy current values into trigger buffer.
 */
void Graph::Layer::prepareSample(std::chrono::nanoseconds t)
{
    timeToSample = t;
}

/****************************************************************************/

/** This virtual method is called by the ProcessVariable, if its value
 * changes.
 */
void Graph::Layer::newValues(std::chrono::nanoseconds ts)
{
    double newValue;
    PdCom::details::copyData(&newValue,
            PdCom::details::TypeInfoTraits<double>::type_info.type,
            getData(), getVariable().getTypeInfo().type, 1);
    newValue = newValue * scale + offset;

    if (dataPresent) {
        if (getFilterConstant() > 0.0) {
            value += getFilterConstant() * (newValue - value);
        } else {
            value = newValue;
        }
    } else {
        dataPresent = true;
        value = newValue;
    }


#ifdef DEBUG_PD_GRAPH
    //qDebug() << this << "ts " << ts.count() << " value " << value;
#endif
    values.append(ts, value);

    if (graph->getEffectiveMode() == Graph::Roll && state == Graph::Run) {
        if (extrema.count() && !validExtrema) {
            extrema[offset].first = value;
            extrema[offset].second = value;
            offsetTime = ts;
            validExtrema = 1U;
        }
        if (appendToExtrema(ts, value)) {
            graph->setRedraw();
        }
    } else if (graph->getEffectiveMode() == Graph::Trigger &&
            timeToSample.count() && ts >= timeToSample) {
        savedValues.copyUntil(values, timeToSample);
        timeToSample = std::chrono::nanoseconds{0}; // mark as sampled
        fillExtrema();
        graph->notifySampled();
    }
}

/****************************************************************************/

/** Fills the #extrema with the values from the ring.
 */
void Graph::Layer::fillExtrema()
{
    unsigned int count = extrema.count();
    const ValueRing<double> *v;

    offset = 0;
    validExtrema = 0U;

    if (graph->getEffectiveMode() == Roll && graph->getState() == Run) {
        v = &values;
    } else {
        v = &savedValues;
    }

    if (!count || !v->getLength()) {
        return;
    }

    std::chrono::nanoseconds range{(int64_t) (graph->getTimeRange() * 1e9)};
    std::chrono::nanoseconds dropTime((*v)[-1].first - range);

    unsigned int i = 0; // drop data before drop time
    while (i < v->getLength() && (*v)[i].first < dropTime) {
        i++;
    }

    if (i >= v->getLength()) {
        return;
    }

    offsetTime = (*v)[i].first;
    extrema[0].first = (*v)[i].second;
    extrema[0].second = (*v)[i].second;
    validExtrema = 1U;

    for (; i < v->getLength(); i++) {
        appendToExtrema((*v)[i].first, (*v)[i].second);
    }
}

/****************************************************************************/

/** Appends a new data value to the #extrema ring buffer.
 *
 * \return \a true, if the widget has to be redrawn.
 */
bool Graph::Layer::appendToExtrema(
        std::chrono::nanoseconds time,
        double value
        )
{
    unsigned int feed, count = extrema.count();
    bool redrawNeeded = false;

    if (count) {
        std::chrono::nanoseconds dt{time - offsetTime};
        std::chrono::nanoseconds r{(int64_t) (graph->getTimeRange() * 1e9)};
#ifdef DEBUG_PD_GRAPH
        //qDebug() << this << __func__ << "dt" << dt.count()
        //    << "r" << r.count();
#endif
        if (dt.count() >= 0 and dt < r) {
            // calculate feed and new offset time

            feed = (double) dt.count() / r.count() * count;
#ifdef DEBUG_PD_GRAPH
        qDebug() << this << __func__ << "dt" << dt.count()
            << "r" << r.count() << "count" << count << "feed" << feed;
#endif
            std::chrono::nanoseconds
                f{(int64_t) (graph->getTimeRange() / count * feed)};
#ifdef DEBUG_PD_GRAPH
            if (f.count()) {
                qDebug() << this << " feed " << feed << "f" << f.count();
            }
#endif
            offsetTime += f;
        } else {
            if (dt.count() < 0) {
                qWarning() << "Invalid time step:"
                    << offsetTime.count() << "to" << time.count();
            }
            feed = count; // feed over all extrema
            offsetTime = time;
        }

        if (feed) {
            if (validExtrema + feed >= count) {
                validExtrema = count;
            } else {
                validExtrema += feed;
            }

            while (feed) {
                // feed ring buffer
                offset = (offset + 1) % count;
                extrema[offset].first = lastAppendedValue;
                extrema[offset].second = lastAppendedValue;
                feed--;
            }

            redrawNeeded = true;
        }

        if (value < extrema[offset].first) {
            extrema[offset].first = value;
            redrawNeeded = true;
        } else if (value > extrema[offset].second) {
            extrema[offset].second = value;
            redrawNeeded = true;
        }
    }

    lastAppendedValue = value;

    return redrawNeeded;
}

/****************************************************************************/

class Graph::TriggerDetector:
    public ScalarSubscriber
{
    public:
        TriggerDetector(Graph *);

        void setTimeRange(std::chrono::nanoseconds);
        void reset();
        void setLevel(double);
        double getLevel() const { return level; };
        void updateLevel();

        /** Internal trigger state.
         */
        enum State {
            Armed, /**< Trigger armed. The received data are are
                     analyzed for the trigger condition. */
            Fired, /**< Trigger fired. The trigger condition was
                     detected. The trigger remains in this state,
                     until the extrema buffer is filled and the data
                     can be displayed. */
        };
        State getState() const { return state; }

    private:
        Graph * const graph; /**< Parent graph. */
        ValueRing<double> values; /**< Ring buffer with time/value. */
        bool dataPresent; /**< \a value contains a valid value. */
        State state; /**< The current #State. */
        double level; /**< Effective trigger level. If the data cross
                        the trigger level from negative to positive,
                        the trigger condition is fulfilled. */
        std::chrono::nanoseconds lastEvent; /**< Time of last trigger
                                              event. */

        void newValues(std::chrono::nanoseconds) override;
        void variableEvent(); /* virtual from ScalarSubscriber */
};

/****************************************************************************/

Graph::TriggerDetector::TriggerDetector(
        Graph *graph
        ):
    graph(graph),
    dataPresent(false),
    state(Armed),
    level(0.0)
{
}

/****************************************************************************/

void Graph::TriggerDetector::setTimeRange(std::chrono::nanoseconds range)
{
    values.setRange(range);
}

/****************************************************************************/

/** Resets the trigger detection.
 */
void Graph::TriggerDetector::reset()
{
    state = Armed;
}

/****************************************************************************/

/** Sets the trigger level.
 */
void Graph::TriggerDetector::setLevel(double l)
{
    level = l;
}

/****************************************************************************/

/** Updates the automatic trigger level.
 */
void Graph::TriggerDetector::updateLevel()
{
    if (state != Armed || graph->getTriggerLevelMode() != AutoLevel) {
        return;
    }

    if (values.getLength() <= 0) {
        level = 0.0;
        return;
    }

    unsigned int i;
    double min, max, sum, value, mean;

    min = max = sum = values[0].second;

    for (i = 1; i < values.getLength(); i++) {
        value = values[i].second;
        if (value < min) {
            min = value;
        }
        if (value > max) {
            max = value;
        }
        sum += value;
    }

    mean = sum / values.getLength();

    if (max - mean > mean - min) {
        level = (mean + max) / 2.0;
    }
    else {
        level = (min + mean) / 2.0;
    }
}

/****************************************************************************/

void Graph::TriggerDetector::newValues(std::chrono::nanoseconds ts)
{
    double newValue;
    PdCom::details::copyData(&newValue,
            PdCom::details::TypeInfoTraits<double>::type_info.type,
            getData(), getVariable().getTypeInfo().type, 1);
    newValue = newValue * scale + offset;

    values.append(ts, newValue);

    if (values.getLength() >= 2 && graph->getMode() == Trigger &&
            graph->getState() == Run && state == Armed) {
        if (values[-2].second < level && values[-1].second >= level) {
            state = Fired;
            lastEvent = ts;
            graph->triggerConditionDetected(lastEvent);
        }
        else if (graph->getTriggerTimeout() > 0.0) {
            std::chrono::nanoseconds
                timeout{(int64_t) (graph->getTriggerTimeout() * 1e9)};
            std::chrono::nanoseconds dt{ts - lastEvent};
            if (dt >= timeout) {
                graph->triggerIdle();
            }
        }
    }
}

/****************************************************************************/

void Graph::TriggerDetector::variableEvent()
{
    if (!hasVariable()) {
        values.clear();
    }
}

/****************************************************************************/

struct Graph::Impl
{
    Impl(Graph *parent):
        parent(parent),
        mode(DEFAULT_MODE),
        effectiveMode(DEFAULT_MODE),
        timeRange(DEFAULT_TIMERANGE),
        timeRangeNs{(int64_t) (timeRange * 1e9)},
        timeScale(parent),
        valueScale(parent, Scale::Vertical),
        triggerLevelMode(DEFAULT_TRIGGER_LEVEL_MODE),
        manualTriggerLevel(DEFAULT_MANUAL_TRIGGER_LEVEL),
        triggerPosition(DEFAULT_TRIGGER_POSITION),
        triggerTimeout(DEFAULT_TRIGGER_TIMEOUT),
        gridColor(DEFAULT_GRID_COLOR),
        autoScaleWidth(DEFAULT_AUTO_SCALE_WIDTH),
        state(Run),
        stopPixmap(":/QtPdWidgets/images/media-playback-pause.png"),
        runAction(parent),
        stopAction(parent),
        scaleWidth(0),
        trigger(parent),
        redraw(false)
    {
#ifdef DEBUG_PD_GRAPH
        qDebug() << "Constructing" << this;
#endif
        valueScale.setMin(DEFAULT_SCALEMIN);
        valueScale.setMax(DEFAULT_SCALEMAX);

        timeScale.setLength(1); // FIXME
        updateTimeScale();

        runAction.setIcon(
                QIcon(":/QtPdWidgets/images/media-playback-start.png"));
        stopAction.setIcon(
                QIcon(":/QtPdWidgets/images/media-playback-pause.png"));

    }

    Graph * const parent;

    Mode mode; /**< Current #Mode. */
    Mode effectiveMode; /**< If #mode is Trigger, effective mode can be
                          Roll anyway (if #triggerTimeout is set). */
    double timeRange; /**< Time range. */
    std::chrono::nanoseconds timeRangeNs; /**< Time range. */
    TimeScale timeScale; /**< Time scale. */
    Scale valueScale; /**< Value scale. */
    TriggerLevelMode triggerLevelMode; /**< The current #TriggerLevelMode.
                                        */
    double manualTriggerLevel; /**< Manual trigger level. This is used, if
                                * #triggerLevelMode is #ManualLevel. */
    double triggerPosition; /**< Horizontal trigger position. In #Trigger
                              mode, the data that caused the trigger event
                              will be displayed on the horizontal position
                              determined by this value. It can range from
                              0 (\a left) to 1 (\a right). */
    double triggerTimeout; /**< If no trigger condition has been detected
                             within this time, the graph will display data
                             like in Roll mode. */
    QColor gridColor; /**< Color of the scale grids. */
    bool autoScaleWidth; /**< Align time scale among sibling graphs. */
    State state; /**< Current graph state. */
    QPixmap stopPixmap; /**< Icon diplayed if state is Stop. */
    QAction runAction; /**< Action for setting state to Run. */
    QAction stopAction; /**< Action for setting state to Stop. */
    int scaleWidth; /**< Width of the value scale. */


    QList<Layer *> layers; /**< List of data layers. */

    TriggerDetector trigger;

    QPixmap backgroundPixmap; /**< Pixmap that stores the background. */
    QColor foregroundColor; /**< Foreground color. */
    QFont foregroundFont; /**< Foreground font. */
    QRect graphRect; /**< Area, where the data are displayed. */

    bool redraw; /**< \a true, if the widget shall be redrawn on next
                   redrawEvent(). */

    /** Calculated the widget's layout.
     */
    void updateBackground()
    {
        QRect rect, valueRect, timeRect;
        QPainter painter;

        rect = parent->contentsRect();

#ifdef DEBUG_PD_GRAPH
        qDebug() << this << "updateBackground" << rect;
#endif

        valueRect = rect;
        valueRect.setTop(rect.top() + timeScale.getOuterLength() + 1);
        valueScale.setLength(valueRect.height());

        if (scaleWidth != valueScale.getOuterLength()) {
            scaleWidth = valueScale.getOuterLength();
            notifyScaleWidthChange();
        }

        int effScaleWidth = scaleWidth;
        if (autoScaleWidth) {
            QList<Graph *> siblings = findSiblings();
            while (siblings.count()) {
                Graph *g = siblings.takeFirst();
                if (g->impl->scaleWidth > effScaleWidth) {
                    effScaleWidth = g->impl->scaleWidth;
                }
            }
        }

        timeRect = rect;
        timeRect.setLeft(rect.left() + effScaleWidth + 1);
        if (timeRect.width() != timeScale.getLength()) {
            int length = timeRect.width();
            if (length < 0) {
                length = 0;
            }
#ifdef DEBUG_PD_GRAPH
            qDebug() << this << "timescale length" << length;
#endif
            timeScale.setLength(length);

            for (QList<Layer *>::const_iterator layer = layers.begin();
                    layer != layers.end();
                    layer++) {
                (*layer)->resizeExtrema(length);
            }
        }

        graphRect = rect;
        graphRect.setLeft(timeRect.left());
        graphRect.setTop(valueRect.top());

        // draw new background into pixmap

        backgroundPixmap = QPixmap(parent->size());
        backgroundPixmap.fill(Qt::transparent);

        painter.begin(&backgroundPixmap);
        painter.setPen(foregroundColor);
        painter.setFont(foregroundFont);
        valueScale.draw(painter, valueRect, gridColor, effScaleWidth);

        painter.setPen(foregroundColor);
        painter.setFont(foregroundFont);
        timeScale.draw(painter, timeRect, gridColor);

        parent->update();
    }

    /** Calculates the range of the #timeScale.
     */
    void updateTimeScale()
    {
#ifdef DEBUG_PD_GRAPH
        qDebug() << this << __func__ << timeRange;
#endif

        if (mode == Trigger) {
            timeScale.setMin(-triggerPosition * timeRange);
            timeScale.setMax((1 - triggerPosition) * timeRange);
        }
        else {
            timeScale.setMin(-timeRange);
            timeScale.setMax(0.0);
        }

        updateBackground();
    }

    /** Retranslate the widget.
     */
    void retranslate()
    {
        parent->setWindowTitle(Pd::Graph::tr("Graph"));
        runAction.setText(Pd::Graph::tr("Run"));
        stopAction.setText(Pd::Graph::tr("Stop"));
    }

    void notifyScaleWidthChange()
    {
        if (!autoScaleWidth) {
            return;
        }

        QList<Graph *> siblings = findSiblings();
        while (siblings.count()) {
            Graph *g = siblings.takeFirst();
            if (g->impl->autoScaleWidth) {
                g->impl->updateBackground();
            }
        }
    }

    QList<Graph *> findSiblings() const
    {
        QObject *p = parent->parent();
        QList<Graph *> siblings;

        if (p) {
            siblings = p->findChildren<Pd::Graph *>();
            siblings.removeAll((Pd::Graph *) this);
        }

        return siblings;
    }

};

/****************************************************************************/

/** Constructor.
 */
Graph::Graph(
        QWidget *parent /**< parent widget */
        ): QFrame(parent),
    impl(std::unique_ptr<Impl>(new Impl(this)))
{
    setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
    setMinimumSize(60, 50);

    connect(getTimer(), SIGNAL(timeout()), this, SLOT(redrawEvent()));
    connect(&impl->runAction, SIGNAL(triggered()), this, SLOT(run()));
    connect(&impl->stopAction, SIGNAL(triggered()), this, SLOT(stop()));

    impl->retranslate();
}

/****************************************************************************/

/** Destructor.
 */
Graph::~Graph()
{
#ifdef DEBUG_PD_GRAPH
    qDebug() << "Destructing..." << this;
#endif
    clearVariables();
    clearTriggerVariable();
#ifdef DEBUG_PD_GRAPH
    qDebug() << "Destructed" << this;
#endif
}

/****************************************************************************/

void Graph::setVariable(
        PdCom::Variable pv,
        const PdCom::Selector &selector,
        const Transmission &transmission,
        double gain,
        double offset,
        double tau,
        QColor color
        )
{
    clearVariables();
    addVariable(pv, selector, transmission, gain, offset, tau, color);
}

/****************************************************************************/

void Graph::setVariable(
        PdCom::Process *p,
        const QString &path,
        const PdCom::Selector &selector,
        const Transmission &transmission,
        double gain,
        double offset,
        double tau,
        QColor color
        )
{
    clearVariables();
    addVariable(p, path, selector, transmission, gain, offset, tau, color);
}

/****************************************************************************/

void Graph::addVariable(
        PdCom::Variable pv,
        const PdCom::Selector &selector,
        const Transmission &transmission,
        double gain,
        double offset,
        double tau,
        QColor color
        )
{
    if (pv.empty()) {
        return;
    }

    Layer *layer = new Layer(this, color, impl->state, impl->timeRangeNs);
    layer->setVariable(pv, selector, transmission, gain, offset, tau);
    layer->resizeExtrema(impl->timeScale.getLength());
    impl->layers.append(layer);
}

/****************************************************************************/

void Graph::addVariable(
        PdCom::Process *p,
        const QString &path,
        const PdCom::Selector &selector,
        const Transmission &transmission,
        double gain,
        double offset,
        double tau,
        QColor color
        )
{
    if (not p or path.isEmpty()) {
        return;
    }

    Layer *layer = new Layer(this, color, impl->state, impl->timeRangeNs);
    layer->setVariable(p, path, selector, transmission, gain, offset, tau);
    layer->resizeExtrema(impl->timeScale.getLength());
    impl->layers.append(layer);
}

/****************************************************************************/

void Graph::clearVariables()
{
    for (QList<Layer *>::iterator layer = impl->layers.begin();
            layer != impl->layers.end(); layer++) {
        delete *layer;
    }

    impl->layers.clear();
    update(impl->graphRect);
}

/****************************************************************************/

void Graph::clearData()
{
    for (QList<Layer *>::iterator layer = impl->layers.begin();
            layer != impl->layers.end(); layer++) {
        (*layer)->clearData();
    }

    update(impl->graphRect);
}

/****************************************************************************/

void Graph::setTriggerVariable(
        PdCom::Variable pv,
        const PdCom::Selector &selector,
        const Transmission &transmission,
        double gain,
        double offset,
        double tau
        )
{
    clearTriggerVariable();

    if (pv.empty()) {
        return;
    }

    impl->trigger.setVariable(pv, selector, transmission, gain, offset, tau);
}

/****************************************************************************/

void Graph::setTriggerVariable(
        PdCom::Process *process,
        const QString &path,
        const PdCom::Selector &selector,
        const Transmission &transmission,
        double gain,
        double offset,
        double tau
        )
{
    clearTriggerVariable();

    if (not process or path.isEmpty()) {
        return;
    }

    impl->trigger.setVariable(process, path, selector, transmission, gain,
            offset, tau);
}

/****************************************************************************/

void Graph::clearTriggerVariable()
{
    impl->trigger.clearVariable();
    impl->trigger.reset();
}

/****************************************************************************/

/**
 * \return The #mode.
 */
Graph::Mode Graph::getMode() const
{
    return impl->mode;
}

/****************************************************************************/

/** Sets the widget's #mode.
 */
void Graph::setMode(Mode mode)
{
    if (mode == impl->mode) {
        return;
    }

    impl->mode = mode;
    impl->updateTimeScale();
    clearData();
    impl->trigger.reset();

    if (mode == Roll) {
        impl->effectiveMode = mode;
    }
}

/****************************************************************************/

/** Resets the widget's #mode.
 */
void Graph::resetMode()
{
    setMode(DEFAULT_MODE);
}

/****************************************************************************/

/**
 * \return The #timeRange.
 */
double Graph::getTimeRange() const
{
    return impl->timeRange;
}

/****************************************************************************/

/** Sets the time range.
 */
void Graph::setTimeRange(double range)
{
    if (range == impl->timeRange) {
        return;
    }

    impl->timeRange = range;
    impl->timeRangeNs = std::chrono::nanoseconds{(int64_t) (range * 1e9)};
#ifdef DEBUG_PD_GRAPH
    qDebug() << this << " " << __func__ << " " << impl->timeRange
        << " ns " <<  impl->timeRangeNs.count();
#endif
    impl->trigger.setTimeRange(impl->timeRangeNs);

    for (QList<Layer *>::iterator layer = impl->layers.begin();
            layer != impl->layers.end(); layer++) {
        (*layer)->setTimeRange(impl->timeRangeNs);
    }

    impl->updateTimeScale();
}

/****************************************************************************/

/** Resets the time range.
 */
void Graph::resetTimeRange()
{
    setTimeRange(DEFAULT_TIMERANGE);
}

/****************************************************************************/

/**
 * \return The value scale minimum.
 */
double Graph::getScaleMin() const
{
    return impl->valueScale.getMin();
}

/****************************************************************************/

/** Sets the value scale's minimum value.
 */
void Graph::setScaleMin(double scale)
{
    if (scale != impl->valueScale.getMin()) {
        impl->valueScale.setMin(scale);
        impl->updateBackground();
    }
}

/****************************************************************************/

/** Resets the value scale's minimum value.
 */
void Graph::resetScaleMin()
{
    setScaleMin(DEFAULT_SCALEMIN);
}

/****************************************************************************/

/**
 * \return The value scale maximum.
 */
double Graph::getScaleMax() const
{
    return impl->valueScale.getMax();
}

/****************************************************************************/

/** Sets the value scale's maximum value.
 */
void Graph::setScaleMax(double scale)
{
    if (scale != impl->valueScale.getMax()) {
        impl->valueScale.setMax(scale);
        impl->updateBackground();
    }
}

/****************************************************************************/

/** Resets the value scale's maximum value.
 */
void Graph::resetScaleMax()
{
    setScaleMax(DEFAULT_SCALEMAX);
}

/****************************************************************************/

/**
 * \return The #triggerLevelMode.
 */
Graph::TriggerLevelMode Graph::getTriggerLevelMode() const
{
    return impl->triggerLevelMode;
}

/****************************************************************************/

/** Sets the widget's #triggerLevelMode.
 */
void Graph::setTriggerLevelMode(TriggerLevelMode m)
{
    if (m == impl->triggerLevelMode) {
        return;
    }

    impl->triggerLevelMode = m;
    if (impl->triggerLevelMode == ManualLevel) {
        impl->trigger.setLevel(impl->manualTriggerLevel);
    }
    impl->trigger.reset();
}

/****************************************************************************/

/** Resets the widget's #triggerLevelMode.
 */
void Graph::resetTriggerLevelMode()
{
    setTriggerLevelMode(DEFAULT_TRIGGER_LEVEL_MODE);
}

/****************************************************************************/

/**
 * \return The #manualTriggerLevel.
 */
double Graph::getManualTriggerLevel() const
{
    return impl->manualTriggerLevel;
}

/****************************************************************************/

/** Sets the #manualTriggerLevel.
 */
void Graph::setManualTriggerLevel(double l)
{
    if (l == impl->manualTriggerLevel) {
        return;
    }

    impl->manualTriggerLevel = l;
    if (impl->triggerLevelMode == ManualLevel) {
        impl->trigger.setLevel(impl->manualTriggerLevel);
        impl->trigger.reset();
    }
}

/****************************************************************************/

/** Resets the #manualTriggerLevel.
 */
void Graph::resetManualTriggerLevel()
{
    setManualTriggerLevel(DEFAULT_MANUAL_TRIGGER_LEVEL);
}

/****************************************************************************/

/**
 * \return The #triggerPosition.
 */
double Graph::getTriggerPosition() const
{
    return impl->triggerPosition;
}

/****************************************************************************/

/** Sets the #triggerPosition.
 */
void Graph::setTriggerPosition(double triggerPosition)
{
    if (triggerPosition > 1.0) {
        triggerPosition = 1.0;
    }
    else if (triggerPosition < 0.0) {
        triggerPosition = 0.0;
    }

    if (triggerPosition == impl->triggerPosition) {
        return;
    }

    impl->triggerPosition = triggerPosition;
    impl->updateTimeScale();
    clearData();
}

/****************************************************************************/

/** Resets the #triggerPosition.
 */
void Graph::resetTriggerPosition()
{
    setTriggerPosition(DEFAULT_TRIGGER_POSITION);
}

/****************************************************************************/

double Graph::getTriggerTimeout() const
{
    return impl->triggerTimeout;
}

/****************************************************************************/

/** Sets the #triggerTimeout.
 */
void Graph::setTriggerTimeout(double triggerTimeout)
{
    if (triggerTimeout < 0.0) {
        triggerTimeout = 0.0;
    }

    if (triggerTimeout == impl->triggerTimeout) {
        return;
    }

    impl->triggerTimeout = triggerTimeout;
}

/****************************************************************************/

/** Resets the #triggerTimeout.
 */
void Graph::resetTriggerTimeout()
{
    setTriggerTimeout(DEFAULT_TRIGGER_TIMEOUT);
}

/****************************************************************************/

/**
 * \return The #suffix.
 */
const QString &Graph::getSuffix() const
{
    return impl->valueScale.getSuffix();
}

/****************************************************************************/

/** Sets the #suffix to display after the Y-axis values.
 */
void Graph::setSuffix(const QString &suffix)
{
    if (suffix != getSuffix()) {
        impl->valueScale.setSuffix(suffix);
        impl->updateBackground();
    }
}

/****************************************************************************/

/** Resets the #suffix to display after the Y-axis values.
 */
void Graph::resetSuffix()
{
    impl->valueScale.resetSuffix();
    impl->updateBackground();
}

/****************************************************************************/

QColor Graph::getGridColor() const
{
    return impl->gridColor;
}

/****************************************************************************/

/** Sets the #gridColor.
 */
void Graph::setGridColor(const QColor &col)
{
    if (impl->gridColor == col) {
        return;
    }

    impl->gridColor = col;
    impl->updateBackground();
}

/****************************************************************************/

/** Resets the #gridColor.
 */
void Graph::resetGridColor()
{
    setGridColor(DEFAULT_GRID_COLOR);
}

/****************************************************************************/

bool Graph::getAutoScaleWidth() const
{
    return impl->autoScaleWidth;
}

/****************************************************************************/

/** Sets the #autoScaleWidth flag.
 */
void Graph::setAutoScaleWidth(bool a)
{
    if (impl->autoScaleWidth == a) {
        return;
    }

    impl->autoScaleWidth = a;
    impl->updateBackground();
}

/****************************************************************************/

/** Resets the #gridColor.
 */
void Graph::resetAutoScaleWidth()
{
    setAutoScaleWidth(DEFAULT_AUTO_SCALE_WIDTH);
}

/****************************************************************************/

Graph::Mode Graph::getEffectiveMode() const
{
    return impl->effectiveMode;
}

/****************************************************************************/

/** Gives a hint aboute the optimal size.
 */
QSize Graph::sizeHint() const
{
    return QSize(300, 100);
}

/****************************************************************************/

bool Graph::getState() const
{
    return impl->state;
}

/****************************************************************************/

/** Set the graph state.
 */
void Graph::setState(State s)
{
    if (impl->state == s) {
        return;
    }

    impl->state = s;

#ifdef DEBUG_PD_GRAPH
    qDebug() << this << impl->state;
#endif

    for (QList<Layer *>::iterator layer = impl->layers.begin();
            layer != impl->layers.end(); layer++) {
        (*layer)->setState(impl->state);
    }

    update();
}

/****************************************************************************/

/** Toggle the graph state.
 */
void Graph::toggleState()
{
    switch (impl->state) {
        case Run:
            setState(Stop);
            break;

        default:
            setState(Run);
            break;
    }
}

/****************************************************************************/

/** Event handler.
 */
bool Graph::event(
        QEvent *event /**< Paint event flags. */
        )
{
    switch (event->type()) {
        case QEvent::MouseButtonDblClick:
            toggleState();
            return true;

        case QEvent::LanguageChange:
            impl->retranslate();
            break;

        case QEvent::StyleChange:
            impl->updateBackground();
            break;

        default:
            break;
    }

    return QFrame::event(event);
}

/****************************************************************************/

/** Handles the widget's resize event.
 */
void Graph::resizeEvent(QResizeEvent *event)
{
    Q_UNUSED(event);
    impl->updateBackground();
}

/****************************************************************************/

/** Paint function.
 */
void Graph::paintEvent(
        QPaintEvent *event /**< paint event flags */
        )
{
    QFrame::paintEvent(event);

    QPainter painter(this);
    QPen pen = painter.pen();
    QFont font = painter.font();

    // update style
    if (pen.color() != impl->foregroundColor ||
            font != impl->foregroundFont) {
        impl->foregroundColor = pen.color();
        impl->foregroundFont = font;
        impl->timeScale.update();
        impl->valueScale.update();
        impl->updateBackground();
    }

    // draw background from pixmap
    painter.drawPixmap(event->rect().topLeft(), impl->backgroundPixmap,
            event->rect());

    // draw layers
    if (impl->valueScale.range()
            && event->rect().intersects(impl->graphRect)) {
        painter.setClipRect(impl->graphRect);
        double scale = impl->graphRect.height() / impl->valueScale.range();

        for (QList<Layer *>::const_iterator layer = impl->layers.begin();
                layer != impl->layers.end(); layer++) {
            (*layer)->paint(painter, scale, impl->valueScale,
                    impl->graphRect);
        }
    }

    // draw pause icon
    if (impl->state == Stop) {
        QRect iconRect;
        iconRect.setSize(impl->stopPixmap.size());
        iconRect.moveTop(impl->graphRect.top() + 5);
        iconRect.moveRight(impl->graphRect.right() - 5);

        if (event->rect().intersects(iconRect)) {
            painter.drawPixmap(iconRect.topLeft(), impl->stopPixmap);
        }
    }

#ifdef DEBUG_PD_GRAPH
    if (impl->effectiveMode == Trigger) {
        QRect rect(contentsRect());
        QPen pen2;
        pen2.setColor(Qt::red);
        painter.setClipping(false);
        painter.setPen(pen2);
        painter.drawText(rect, Qt::AlignLeft | Qt::AlignTop, "T");
    }
#endif
}

/****************************************************************************/

/** Shows the context menu.
 */
void Graph::contextMenuEvent(QContextMenuEvent *event)
{
    impl->runAction.setEnabled(impl->state != Run);
    impl->stopAction.setEnabled(impl->state != Stop);

    QMenu menu(this);
    menu.addAction(&impl->runAction);
    menu.addAction(&impl->stopAction);
    menu.exec(event->globalPos());
}

/****************************************************************************/

/** Redraw event slot, that is called by the redraw timer.
 */
void Graph::redrawEvent()
{
    if (impl->mode == Trigger && impl->state == Run) {
        impl->trigger.updateLevel();
    }

    if (impl->redraw) {
        impl->redraw = false;
        update(impl->graphRect);
    }
}

/****************************************************************************/

/** Set the graph to Run state.
 */
void Graph::run()
{
    setState(Run);
}

/****************************************************************************/

/** Set the graph to Stop state.
 */
void Graph::stop()
{
    setState(Stop);
}

/****************************************************************************/

/** Mark the graph widget for redrawing.
 */
void Graph::setRedraw()
{
    impl->redraw = true;
}

/****************************************************************************/

/** Called by the layers if the data were sampled.
 */
void Graph::notifySampled()
{
    bool allSampled = true;

    for (QList<Layer *>::const_iterator layer = impl->layers.begin();
            layer != impl->layers.end(); layer++) {
        if (!(*layer)->hasSampled()){
            allSampled = false;
            break;
        }
    }

    if (allSampled) {
        impl->redraw = true;
        impl->trigger.reset();
    }
}

/****************************************************************************/

/** Called by the trgger detector, if the trigger condition was detected.
 */
void Graph::triggerConditionDetected(std::chrono::nanoseconds triggerTime)
{
    impl->effectiveMode = Trigger;

    // calculate time to sample triggered values
    std::chrono::nanoseconds timeToSample = triggerTime
        + std::chrono::nanoseconds((int64_t) (
                    (1.0 - impl->triggerPosition) * impl->timeRange * 1e9));

    for (QList<Layer *>::const_iterator layer = impl->layers.begin();
            layer != impl->layers.end(); layer++) {
        (*layer)->prepareSample(timeToSample);
    }
}

/****************************************************************************/

/** Called by the trigger detector, if no trigger condition is detected for a
 * while.
 */
void Graph::triggerIdle()
{
    impl->effectiveMode = Roll;
}

/****************************************************************************/
