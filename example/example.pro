#-----------------------------------------------------------------------------
#
# Copyright (C) 2009-2022  Florian Pose <fp@igh.de>
#
# This file is part of the QtPdWidgets library.
#
# The QtPdWidgets library is free software: you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public License as
# published by the Free Software Foundation, either version 3 of the License,
# or (at your option) any later version.
#
# The QtPdWidgets library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser
# General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with the QtPdWidgets Library. If not, see
# <http://www.gnu.org/licenses/>.
#
# vim: syntax=config
#
#-----------------------------------------------------------------------------

TEMPLATE = app
TARGET = pdwidgetsexample
QT += gui widgets network svg
CONFIG += debug c++14

#-----------------------------------------------------------------------------

DEPENDPATH += ..
INCLUDEPATH += ..

#-----------------------------------------------------------------------------

!isEmpty(PDCOMPREFIX) {
    INCLUDEPATH += $${PDCOMPREFIX}/include
    LIBS += -L$${PDCOMPREFIX}/lib$${LIBEXT}
}

QMAKE_LFLAGS += -L$$OUT_PWD/..
QMAKE_LFLAGS_APP += -Wl,--rpath -Wl,..

LIBS += -lQtPdWidgets2 -lpdcom5

# Output code coverage -------------------------------------------------------

!isEmpty(PD_COVERAGE){
    QMAKE_CXXFLAGS_DEBUG -= -O2
    QMAKE_CXXFLAGS_RELEASE -= -O2
    QMAKE_CXXFLAGS += --coverage -g -O0
    LIBS += -lgcov
}

#-----------------------------------------------------------------------------

HEADERS += MainWindow.h
SOURCES += main.cpp MainWindow.cpp
FORMS += MainWindow.ui

#-----------------------------------------------------------------------------
