/****************************************************************************
**
** QML-Widgets for qtPdWidgets
**
** Copyright (C) 2021 Wilhelm Hagemeister
** Contact: hm@igh.de
**
** 
** Quick2 component; if you want a more flat design
** consider using PdLed or PdMultiLed from ./quick1/
**
**
****************************************************************************/

import QtQuick 2.7         
import QtQuick.Controls 2.3
import QtQuick.Extras 1.4

import de.igh.pd 2.0

StatusIndicator {
    id: indicator
    /** variable: var
     *
     * connection to the process variable: to be used like
     *
     * @code{.qml}
     * 
     * variable {
     *     connection: {
     *         "process":pdProcess,
     *         "path":"/control/value",
     *         "scale":1,
     *         "offset":0,
     *         "period":0.1,
     *         "transmission":Pd.periodic
     *     }
     * }
     * @endcode
     */
    property alias variable:scalar
    property alias connection:scalar.connection

    /**type:var
     * convinience connection to the process path
     * see @link PdCheckBox @endlink
     */
    property alias path:scalar.path

    property alias value: scalar.value
    property bool invert:false
    property bool blinking:false
    property bool isOn: ((scalar.value != 0) != invert) 
    opacity:scalar.connected?1:0.3
    //im Gegensatz zu active welches beim blinken wegfällt
    PdScalar {
        id:scalar
        onValueChanged: {
            if(value != invert) {
                if(blinking) {
                    timer.running = true
                } else {
                    indicator.active = true
                }
            } else {
                timer.running = false
                indicator.active = false
            }
        }
    }
    Timer {
        id:timer
        interval: 500;
        repeat: true;
        onTriggered: {
            indicator.active = (!indicator.active)
        }
    }
}
