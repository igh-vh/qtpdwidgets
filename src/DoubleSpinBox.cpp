/*****************************************************************************
 *
 * Copyright (C) 2009 - 2019  Florian Pose <fp@igh-essen.com>
 *
 * This file is part of the QtPdWidgets library.
 *
 * The QtPdWidgets library is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * The QtPdWidgets library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser
 * General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with the QtPdWidgets Library. If not, see
 * <http://www.gnu.org/licenses/>.
 *
 ****************************************************************************/

#include "DoubleSpinBox.h"
using Pd::DoubleSpinBox;

#include <QtGui>
#include <QLineEdit>

/****************************************************************************/

struct DoubleSpinBox::Impl
{
    Impl(DoubleSpinBox *parent):
        parent(parent),
        editing(false),
        internalValue(0.0)
    {
    }

    DoubleSpinBox * const parent;

    bool editing; /**< True, if the control is in editing state (the
                    background is yellow, then). */
    double internalValue; /**< Internal process value, that stores the
                            last process value, even if the user is
                            editing. */

    void setEditing(bool e)
    {
        if (editing != e) {
            QPalette p(parent->lineEdit()->palette());

            editing = e;

            p.setBrush(QPalette::Base,
                    editing ? p.alternateBase() : p.base());
            parent->lineEdit()->setPalette(p);
        }
    }

    void setInternalValue(double v)
    {
        if (v != internalValue) {
            internalValue = v;

            if (!editing) {
                updateFromInternal();
            }
        }
    }

    void updateFromInternal()
    {
        parent->setValue(internalValue);
    }
};

/****************************************************************************/

/** Constructor.
 */
DoubleSpinBox::DoubleSpinBox(
        QWidget *parent /**< parent widget */
        ):
    QDoubleSpinBox(parent),
    impl(std::unique_ptr<Impl>(new Impl(this)))
{
    QLineEdit *le = lineEdit();
    le->setAutoFillBackground(true);

    connect(le, SIGNAL(textChanged(const QString &)),
            this, SLOT(on_textChanged(const QString &)));
    connect(le, SIGNAL(editingFinished()),
            this, SLOT(on_editingFinished()));
}

/****************************************************************************/

/** Destructor.
 */
DoubleSpinBox::~DoubleSpinBox()
{
}

/****************************************************************************/

void DoubleSpinBox::clearData()
{
    impl->setInternalValue(0.0);
    impl->updateFromInternal();
}

/****************************************************************************/

/** Called, when the user changes the text in the line edit.
 */
void DoubleSpinBox::on_textChanged(const QString &text)
{
    Q_UNUSED(text);
    impl->setEditing(true);
}

/****************************************************************************/

/** Called, when the input focus leaves the line edit.
 *
 * Editing shall be canceled in this case.
 */
void DoubleSpinBox::on_editingFinished()
{
    if (impl->editing) {
        impl->setEditing(false);
        impl->updateFromInternal();
    }
}

/****************************************************************************/

/** Handles keybord events from the user.
 *
 * Overloads the keyPressEvent handler from QDoubleSpinBox.
 */
void DoubleSpinBox::keyPressEvent(QKeyEvent *event)
{
    switch (event->key()) {
        case Qt::Key_Enter:
        case Qt::Key_Return:
            if (impl->editing) {
                event->accept();
                impl->setEditing(false);
                writeValue(value());
                impl->updateFromInternal();
                return;
            }
            break;

        case Qt::Key_Escape:
            if (impl->editing) {
                event->accept();
                impl->setEditing(false);
                impl->updateFromInternal();
                return;
            }
            break;
    }

    QDoubleSpinBox::keyPressEvent(event);
}

/****************************************************************************/

/** Called, when the user clicks on the up/down buttons/keys.
 *
 * This is a virtual function from QAbstractSpinBox, that is overloaded here.
 *
 * When the user is editing the text value (the control has base color), the
 * value shall be incremented, but not yet sent to the process.  When no
 * editing is in progress (control is white), the value shall be sent to the
 * process, immediately.
 */
void DoubleSpinBox::stepBy(int steps)
{
    if (impl->editing) {
        QDoubleSpinBox::stepBy(steps);
    } else {
        double v = impl->internalValue + steps * singleStep();
        if (v > maximum())
            v = maximum();
        else if (v < minimum())
            v = minimum();

        writeValue(v);
    }
}

/****************************************************************************/

/** This virtual method is called by the ProcessVariable, if its value
 * changes.
 */
void DoubleSpinBox::newValues(std::chrono::nanoseconds)
{
    double v;
    PdCom::details::copyData(&v,
            PdCom::details::TypeInfoTraits<double>::type_info.type,
            getData(), getVariable().getTypeInfo().type, 1);
    impl->setInternalValue(v * scale + offset);
}

/****************************************************************************/
