/*****************************************************************************
 *
 * Copyright (C) 2022 Florian Pose (fp@igh.de),
 *
 * This file is part of the QtPdWidgets library.
 *
 * The QtPdWidgets library is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * The QtPdWidgets library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser
 * General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with the QtPdWidgets Library. If not, see
 * <http://www.gnu.org/licenses/>.
 *
 ****************************************************************************/

#include "Subscription.h"

#include "Process.h"
#include "Subscriber.h"

#include <QDebug>

using namespace PdCom;

/****************************************************************************/

Subscription::Subscription(
                Subscriber &subscriber,
                const Variable &variable,
                const Selector &selector):
    subscriber_{subscriber},
    process_{variable.getProcess()},
    path_{variable.getPath()},
    variable_{&(Variable &) variable},
    selector_{selector}
{
    (void) subscriber;
    (void) variable;
    (void) selector;

    qDebug() << "Created subscription " << this
        << "via variable" << variable.getPath().c_str();
}

/****************************************************************************/

Subscription::Subscription(
                Subscriber &subscriber,
                Process &process,
                const std::string &path,
                const Selector &selector):
    subscriber_{subscriber},
    process_{&process},
    path_{path},
    variable_{process.getVariable(path)},
    selector_{selector}
{
    qDebug() << "Created subscription " << this
        << "via path" << path.c_str();


    process.addSubscription(this);
}

/****************************************************************************/

void Subscription::poll()
{
    qDebug() << "Polling subscription" << this;
}

/****************************************************************************/

const void *Subscription::getData() const
{
    qDebug() << "Getting mocked data of subscription" << this;
    return variable_->getMockedData();
}

/*****************************************************************************
 * mocked interface
 ****************************************************************************/

void Subscription::mockValue()
{
    if (state_ != State::Active) {
        qDebug() << "Subscription" << this << "setting state to Active";
        state_ = State::Active;
        subscriber_.stateChanged(*this);
    }

    qDebug() << "Subscription" << this << "announcing new values";
    auto now = std::chrono::system_clock::now();
    subscriber_.newValues(now.time_since_epoch());
}

/****************************************************************************/
