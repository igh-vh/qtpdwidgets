/*****************************************************************************
 *
 * Copyright (C) 2009  Florian Pose <fp@igh-essen.com>
 *
 * This file is part of the QtPdWidgets library.
 *
 * The QtPdWidgets library is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * The QtPdWidgets library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser
 * General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with the QtPdWidgets Library. If not, see
 * <http://www.gnu.org/licenses/>.
 *
 ****************************************************************************/

#ifndef PD_DOUBLESPINBOX_H
#define PD_DOUBLESPINBOX_H

#include <QDoubleSpinBox>

#include "Export.h"
#include "ScalarSubscriber.h"

namespace Pd {

/****************************************************************************/

/** Spinbox containing a double value.
 */
class QDESIGNER_WIDGET_EXPORT DoubleSpinBox:
    public QDoubleSpinBox, public ScalarSubscriber
{
    Q_OBJECT

    public:
        DoubleSpinBox(QWidget *parent = 0);
        virtual ~DoubleSpinBox();

        void clearData(); // pure-virtual from ScalarSubscriber

    private slots:
        void on_textChanged(const QString &);
        void on_editingFinished();

    private:
        struct Impl;
        std::unique_ptr<Impl> impl;

        void keyPressEvent(QKeyEvent *);
        void stepBy(int); // virtual from QAbstractSpinBox

        void newValues(std::chrono::nanoseconds) override;
};

/****************************************************************************/

} // namespace

#endif
