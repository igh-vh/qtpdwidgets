#-----------------------------------------------------------------------------
#
# Copyright (C) 2009-2022  Florian Pose <fp@igh.de>
#
# This file is part of the QtPdWidgets library.
#
# The QtPdWidgets library is free software: you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public License as
# published by the Free Software Foundation, either version 3 of the License,
# or (at your option) any later version.
#
# The QtPdWidgets library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser
# General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with the QtPdWidgets Library. If not, see
# <http://www.gnu.org/licenses/>.
#
# vim: syntax=config
#
#-----------------------------------------------------------------------------

TEMPLATE = app
TARGET = pdwidgetstest
QT += gui widgets network svg xml testlib
CONFIG += debug c++14

#-----------------------------------------------------------------------------

DEPENDPATH += ..
INCLUDEPATH += ../QtPdWidgets2
MOC_DIR = .moc
OBJECTS_DIR = .obj

#-----------------------------------------------------------------------------

QMAKE_CXXFLAGS_DEBUG -= -O2
QMAKE_CXXFLAGS_RELEASE -= -O2
QMAKE_CXXFLAGS += --coverage -g -O0

QMAKE_LFLAGS += -L$$OUT_PWD/..
QMAKE_LFLAGS_APP += -Wl,--rpath -Wl,..

LIBS += -lgcov

#-----------------------------------------------------------------------------

HEADERS += \
    ../QtPdWidgets2/Message.h \
    ../QtPdWidgets2/MessageModel.h \
    ../QtPdWidgets2/ScalarSubscriber.h \
    ../QtPdWidgets2/ScalarVariable.h \
    ../QtPdWidgets2/Transmission.h \
    MessageModelTest.h \
    pdcom5/Exception.h \
    pdcom5/Process.h \
    pdcom5/Selector.h \
    pdcom5/SizeTypeInfo.h \
    pdcom5/Subscriber.h \
    pdcom5/Subscription.h \
    pdcom5/Variable.h

SOURCES += \
    ../src/Message.cpp \
    ../src/MessageModel.cpp \
    ../src/ScalarSubscriber.cpp \
    ../src/Transmission.cpp \
    MessageModelTest.cpp \
    main.cpp \
    pdcom5/Process.cpp \
    pdcom5/Selector.cpp \
    pdcom5/Subscriber.cpp \
    pdcom5/Subscription.cpp \
    pdcom5/Variable.cpp

#-----------------------------------------------------------------------------
