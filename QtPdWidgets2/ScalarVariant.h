/*****************************************************************************
 *
 * Copyright (C) 2018-2021  Wilhelm Hagemeister<hm@igh.de>
 *               2018-2022  Florian Pose <fp@igh.de>
 *
 * This file is part of the QtPdWidgets library.
 *
 * The QtPdWidgets library is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * The QtPdWidgets library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser
 * General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with the QtPdWidgets Library. If not, see
 * <http://www.gnu.org/licenses/>.
 *
 ****************************************************************************/

#ifndef PD_SCALARVARIANT_H
#define PD_SCALARVARIANT_H

#include "ScalarSubscriber.h"
#include "Process.h"

#include <QObject>
#include <QVariant>


namespace Pd {

/****************************************************************************/

/** Scalar Variant to be used in QML applications
    which is aware of the process to detect connections/or reconnect
 */

class Q_DECL_EXPORT ScalarVariant:
  public QObject, public Pd::ScalarSubscriber
{
    Q_OBJECT
    Q_PROPERTY(QString path READ getPath WRITE setPath NOTIFY pathChanged)
    /** if one want's to update the variable connection values in one call
       supply a map with {"path":path,
       "period":sampleTime
                          ....
                         }
     */
    Q_PROPERTY(QVariant connection READ getConnection WRITE setConnection NOTIFY connectionUpdated)

    /* indicates that the process is connected and data is transfered */
    Q_PROPERTY(bool connected READ getDataPresent NOTIFY dataPresentChanged)

    Q_PROPERTY(QVariant value READ getValue WRITE setValue NOTIFY valueChanged)
    Q_PROPERTY(double mtime READ getMTimeToDouble NOTIFY valueUpdated)
    public:
        ScalarVariant();
        virtual ~ScalarVariant();

        QString getPath() const { return path; };
        Q_INVOKABLE bool getDataPresent() { return dataPresent; };
        QVariant getConnection();

        void clearData(); // pure-virtual from ScalarSubscriber
        Q_INVOKABLE bool hasData() const { return dataPresent; };
        Pd::Process *getProcess() const { return process; };

        void setPath(QString);
        void setConnection(QVariant);

        void setProcess(Pd::Process *);
        QVariant getValue() const { return value; };
        Q_INVOKABLE void setValue(QVariant);
    std::chrono::nanoseconds getMTime() const { return mTime; };
    double getMTimeToDouble() {
        return std::chrono::duration<double>(mTime).count();
    };
    Q_INVOKABLE void inc();

    private:
    Pd::Process *process;
        QVariant value; /**< Current value. */
        QString path;
        double period;
        double _scale;
        double _offset;
        std::chrono::nanoseconds mTime; /**< Modification Time of Current value. */
        bool dataPresent; /**< There is a process value to display. */
        void updateConnection(); /**< (re)connects to variable */

    // pure-virtual from ScalarSubscriber
    void newValues(std::chrono::nanoseconds);

    template <typename T>
    typename std::enable_if<std::is_arithmetic<T>::value, void>::type
    copyData(T &dest) const
    {
        PdCom::details::copyData(
                &dest, PdCom::details::TypeInfoTraits<T>::type_info.type,
                getData(),
                getVariable().getTypeInfo().type, 1);
    }

    private slots:
        void processConnected();
    void processDisconnected();
    void processError();

    signals:
        void valueChanged(QVariant); /**< Emitted, when the value changes, or the
                                       variable is disconnected. */
        void valueUpdated(double); /**< Emitted also when value does not change but
                                     got an update from the msr process */
        void pathChanged(QString &);
        void dataPresentChanged(bool);
        void connectionUpdated();
};

/****************************************************************************/

} // namespace


#endif
