/****************************************************************************
**
** Copyright (C) 2021 Wilhelm Hagemeister
** Contact: hm@igh.de
**
** !!!!!!!!!!!! this is work in progress !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
**
** livesvg (Bietet die Möglichkeit svgs zu manipulieren (noch nicht fertig)
** und sucht aus dem layer "overlay" alle rects raus und scaliert alle
** child componenten, die im qml den "objectName" auf die id im svg-rect
*gesetzt haben.
**
** TODO: die absolute Position eines "rects" ergibt sich durch die Kette aller
** Transformationen im SVG. Bisher ist nur die "translation" implementiert!
**
** Auch wichtig: qt.svg unterstützt nur das tiny svg format Version: 1.2.
** d.h. nicht alle svg-Elemente werden unterstützt
**
**
****************************************************************************/

#include <QPainter>
#include <QDebug>
#include <QRectF>
#include <QtMath>
#include <QSizeF>
#include <QQuickItem>

#include "QtPdWidgets2/LiveSvg.h"

/* ---------------------------------------------------------------------------------
 */

using Pd::LiveSvg;

LiveSvg::LiveSvg(QQuickItem *parent):
    QQuickPaintedItem(parent),
    m_svgdoc("svg"),
    viewBox(0, 0, 0, 0),
    source(""),
    empty(true),
    invert(false)
{
    //  this->setImplicitWidth(200);
    // this->setImplicitHeight(200);
}

/* ---------------------------------------------------------------------------------
 */

void LiveSvg::clearSource()
{
    overlayElements.clear();
}

void LiveSvg::setInvert(bool value)
{
    if (value != invert) {
        invert = value;
        emit invertedChanged();
        update(QRect(0, 0, this->width(), this->height()));
    }
}
/* ---------------------------------------------------------------------------------
 */

void LiveSvg::setSource(const QString &s)
{
    // read svg-file (aka xml) in a DomDocument

    // to avoid a inconsistency between qml and c++ ressource file locators
    QString source(s);

    source = source.replace("qrc:", ":");

    if (LiveSvg::source == source) {
        return;
    }

    LiveSvg::source = source;

    empty = true;
    clearSource();

    QFile file(source);
    if (!file.open(QIODevice::ReadOnly)) {
        emit sourceChanged();
        qDebug() << "no file";
        return;
    }

    if (!m_svgdoc.setContent(&file)) {
        file.close();
        qDebug() << "svg could not be read";
        emit sourceChanged();
        return;
    }

    // get all recs in the Layer overlay
    getOverlayRects(m_svgdoc.documentElement());
    empty = false;
    emit sourceChanged();
}


/* ---------------------------------------------------------------------------------
 */

// Im SVG sollten sich mehrere Layer befinden. Nur der Layer, der mit msr
// markiert ist, wird dynamisch aktualisiert

// Im Layer Overlay befinden sich rects, die als Platzhalter für
// qml-Komponenten verwendet werden das Layer selber wird nicht gezeichnet

QDomElement
LiveSvg::findLayer(const QString &layerName, const QDomElement &parent)
{
    QList<QDomElement> groups;
    QDomElement elem;
    findElementsWithAttribute(parent, "inkscape:groupmode", groups);
    foreach (elem, groups) {
        if (elem.attribute("inkscape:groupmode") == "layer") {
            if (elem.attribute("inkscape:label") == layerName) {
                return elem;
            }
        }
    }
    return QDomElement();
}

/* ---------------------------------------------------------------------------------
 */

/* TODO we need to react to all transformations, right now only
   translations are taken into account!!
   Also the parser could be made with regular expressions....

   The parser is in:
   /vol/opt/qt/5.12.6/Src/qtsvg/src/svg/qsvghandler.cpp
   Function: static QMatrix parseTransformationMatrix(const QStringRef &value)

   !!!!
   Look at QMatrix QSvgRenderer::matrixForElement(const QString &id) const
   !!!! that should do as well and better ;-)
*/

void LiveSvg::getTransformations(const QDomNode &elem, QPointF &offset)
{
    if (!elem.parentNode().isNull()) {
        QString transform =
                elem.parentNode().toElement().attribute("transform");

        // FIXME where are all the other transformations: shear, scale,
        // matrix, rotate,....
        if (transform.contains("translate", Qt::CaseInsensitive)) {
            QStringList tList = transform.replace('"', "")
                                        .replace("translate", "")
                                        .replace('(', "")
                                        .replace(')', "")
                                        .split(',');  // FIXME regex?
            if (tList.size() == 2) {
                offset += QPointF(tList[0].toDouble(), tList[1].toDouble());
            }
        }

        getTransformations(elem.parentNode(), offset);
    }
}

/* ---------------------------------------------------------------------------------
 */

void LiveSvg::getOverlayRects(const QDomElement &parent)
{
    // find the layer which name is "overlay"
    QDomElement overlay = findLayer("overlay", parent);

    // get all the rects with id, x,y,width and height
    if (!overlay.isNull()) {
        // qDebug() <<  "overlay found";
        QDomNodeList rects = overlay.elementsByTagName("rect");
        for (int i = 0; i < rects.length(); i++) {
            QDomNamedNodeMap attr = rects.at(i).toElement().attributes();
            QVariantMap e;

            // get all translation offsets
            // FIXME take all transformations into account
            QPointF offset = QPointF(0, 0);
            getTransformations(rects.at(i), offset);
            // put into the map of the rect
            // we hope that in svg there will never be a "ox" and "oy"
            e["ox"] = offset.x();
            e["oy"] = offset.y();

            for (int j = 0; j < attr.count(); ++j) {
                QDomAttr attribute = attr.item(j).toAttr();
                e[attribute.name()] = attribute.value();
            }

            overlayElements.append(e);  // global in class
        }
        // qDebug() << "entries" <<  overlayElements;
        // qDebug() << "entries-------------------------";
    }
}

/* ---------------------------------------------------------------------------------
 */

QQuickItem *LiveSvg::findChildItem(QQuickItem *parent, const QString &name)
{
    QList<QQuickItem *> children = parent->childItems();
    foreach (QQuickItem *item, children) {
        if (item->objectName() == name) {
            return item;
        }
        // call recursively and browse through the whole scene graph of this
        // object
        if (QQuickItem *r = findChildItem(item, name))
            return r;
    }
    return NULL;
}

/* ---------------------------------------------------------------------------------
 */
/* ox and oy are scaled all ready ! */
void LiveSvg::scaleQmlChildren(
        double ox,
        double oy,
        double sx,
        double sy,
        QSvgRenderer &renderer)
{
    /* QObject::findChild() does not work with repeater because in QML there
       is a real parent and a visual parent
       https://stackoverflow.com/questions/21623375/qml-repeater-parentness/22556528
       so we implemented a findChildItem method!
    */
    // qDebug() << "viewbox offset" << ox << ", " << oy;
    for (int i = 0; i < overlayElements.count(); i++) {
        QVariantMap e = overlayElements[i].toMap();
        // qDebug() << "search " << e["id"].toString();

        QQuickItem *item = findChildItem(this, e["id"].toString());
        if (item) {
            // qDebug() << "found ";
            /*
            qDebug() << "id: " << e["id"] << " dx: " << e["x"].toDouble() << "
            dy: " << e["y"].toDouble(); qDebug() << "id: " << e["id"] << " w:
            " << e["width"].toDouble() << " h: " << e["height"].toDouble();
            qDebug() << "id: " << e["id"] << " ox: " << e["ox"].toDouble() <<
            " oy: " << e["oy"].toDouble();

            qDebug() << "id: " << e["id"] << " sx: " << sx << " sy: " << sy;
            */
            /*
            item->setProperty("x",ox+(e["x"].toDouble() + e["ox"].toDouble())*
            sx); item->setProperty("y",oy+(e["y"].toDouble() +
            e["oy"].toDouble())* sy);
            item->setProperty("width",e["width"].toDouble() * sx);
            item->setProperty("height",e["height"].toDouble() * sy);
            */
            /*
            double x = ox+(e["x"].toDouble() + e["ox"].toDouble())* sx;
            double y = oy+(e["y"].toDouble() + e["oy"].toDouble())* sy;
            double w = e["width"].toDouble() * sx;
            double h = e["height"].toDouble() * sy;
            qDebug() << "center x"  << x + w/2 << "center y" << y + h/2;
            */


            QRectF bounds = renderer.boundsOnElement(e["id"].toString());
            QPointF center = bounds.center();

#if QT_VERSION >= QT_VERSION_CHECK(5, 15, 0)
            QTransform m = renderer.transformForElement(e["id"].toString());
#else
            QMatrix m = renderer.matrixForElement(e["id"].toString());
            // qDebug() << "bounds :" << bounds << " center: "  << center <<
            // "matrix: " << m;
#endif

            // apply the transformation matrix (FIXME only translation)
            center += QPointF(m.dx(), m.dy());

            // FIXME we do the rotation outside or later

            double w = e["width"].toDouble();
            double h = e["height"].toDouble();
            item->setProperty("x", ox + (center.x() - w / 2) * sx);
            item->setProperty("y", oy + (center.y() - h / 2) * sy);
            item->setProperty("width", w * sx);
            item->setProperty("height", h * sy);
        }
    }
}

// TEST
void LiveSvg::getTransform(QSvgRenderer &renderer)
{
    for (int i = 0; i < overlayElements.count(); i++) {
        QVariantMap e = overlayElements[i].toMap();
        QString eid = e["id"].toString();

#if QT_VERSION >= QT_VERSION_CHECK(5, 15, 0)
        QTransform m = renderer.transformForElement(eid);
#else
        QMatrix m = renderer.matrixForElement(eid);
#endif

        qDebug() << "id: " << eid
                 << " matrix: " << m;
        qDebug() << "id: " << eid
                 << "bounds: " << renderer.boundsOnElement(eid);
    }
}


/* ---------------------------------------------------------------------------------
 */

void LiveSvg::updateBackground()
{
    QPainter painter;

    backgroundPixmap = QPixmap(QSize(this->width(), this->height()));
    backgroundPixmap.fill(Qt::transparent);


    painter.begin(&backgroundPixmap);
    painter.setRenderHints(QPainter::Antialiasing);

    if (invert) {  // FIXME very dirty hack.....
        QString doc = m_svgdoc.toString();
        doc = doc.replace("#000000", "#ffffff");
        m_renderer.load(doc.toUtf8());
    }
    else {
        m_renderer.load(m_svgdoc.toByteArray());
    }
    viewBox = m_renderer.viewBoxF();
    emit viewBoxChanged();

    // calc the bounds for preserveAspectFit (which is most likely wanted?!)
    // image is centered
    double scale =
            qMin(this->width() / viewBox.width(),
                 this->height() / viewBox.height());

    QRectF bounds =
            QRectF((this->width() - viewBox.width() * scale) / 2,
                   (this->height() - viewBox.height() * scale) / 2,
                   viewBox.width() * scale,
                   viewBox.height() * scale);

    /*
        qDebug() << "svg viewbox" <<  viewBox;
        qDebug() << "svg bounds" <<  bounds;
    */

    m_renderer.render(&painter, bounds);
    painter.end();
    if (viewBox.width() > 0 && viewBox.height() > 0) {
        scaleQmlChildren(
                bounds.x() - viewBox.x() * scale,
                bounds.y() - viewBox.y() * scale,
                scale,
                scale,
                m_renderer);
        // DBG getTransform(m_renderer);
        emit scaleChanged(QSizeF(scale, scale));
    }
}

/* ---------------------------------------------------------------------------------
 */

void LiveSvg::paint(QPainter *painter)
{
    // qDebug() << "paint size" << this->width() << " x " << this->height();
    if (!empty) {
        updateBackground();
    }
    painter->drawPixmap(0, 0, backgroundPixmap);
}

/* ---------------------------------------------------------------------------------
 */
/* thanks to
https://stackoverflow.com/questions/17836558/can-i-find-all-xml-nodes-with-a-given-attribute-in-a-qdomdocument
*/

void LiveSvg::findElementsWithAttribute(
        const QDomElement &elem,
        const QString &attr,
        QList<QDomElement> &foundElements)
{
    if (elem.attributes().contains(attr))
        foundElements.append(elem);

    QDomElement child = elem.firstChildElement();
    while (!child.isNull()) {
        findElementsWithAttribute(child, attr, foundElements);
        child = child.nextSiblingElement();
    }
}

/* -----for debugging
 * ------------------------------------------------------------- */

void LiveSvg::printElements(QList<QDomElement> elements)
{
    QDomElement elem;
    qDebug() << "count: " << elements.count();
    foreach (elem, elements) {
        qDebug() << "Tagname" << elem.tagName();
        // and list all attributes
        printAttributes(elem);
    }
}

/* ---------------------------------------------------------------------------------
 */

void LiveSvg::printAttributes(QDomElement elem)
{
    QDomNamedNodeMap var = elem.attributes();
    for (int i = 0; i < var.count(); ++i) {
        QDomAttr attribute = var.item(i).toAttr();
        qDebug() << "Attr: " << attribute.name() << ": " << attribute.value();
    }
}
