/*****************************************************************************
 *
 * Copyright (C) 2009 - 2019  Florian Pose <fp@igh.de>
 *
 * This file is part of the QtPdWidgets library.
 *
 * The QtPdWidgets library is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * The QtPdWidgets library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser
 * General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with the QtPdWidgets Library. If not, see
 * <http://www.gnu.org/licenses/>.
 *
 ****************************************************************************/

#ifndef PD_ROTOR_H
#define PD_ROTOR_H

#include "Transmission.h"

#include <pdcom5/Variable.h>
#include <pdcom5/Subscription.h>

#include <QFrame>

#include <memory>

namespace Pd {

/****************************************************************************/

class Q_DECL_EXPORT Rotor:
    public QFrame
{
    Q_OBJECT
    Q_PROPERTY(QString background
            READ getBackground WRITE setBackground RESET resetBackground)
    Q_PROPERTY(QString rotor
            READ getRotor WRITE setRotor RESET resetRotor)
    Q_PROPERTY(QString foreground
            READ getForeground WRITE setForeground RESET resetForeground)
    Q_PROPERTY(QPointF rotorCenter
            READ getRotorCenter WRITE setRotorCenter RESET resetRotorCenter)
    Q_PROPERTY(double globalAngle
            READ getGlobalAngle WRITE setGlobalAngle RESET resetGlobalAngle)

    public:
        Rotor(QWidget * = 0);
        ~Rotor();

        QSize sizeHint() const;

        const QString &getBackground() const;
        void setBackground(const QString &);
        void resetBackground();

        const QString &getRotor() const;
        void setRotor(const QString &);
        void resetRotor();

        const QString &getForeground() const;
        void setForeground(const QString &);
        void resetForeground();

        QPointF getRotorCenter() const;
        void setRotorCenter(QPointF);
        void resetRotorCenter();

        double getGlobalAngle() const;
        void setGlobalAngle(double);
        void resetGlobalAngle();

        /** Subscribe to a process variable.
         */
        void setSpeedVariable(
                PdCom::Variable pv, /**< Process variable. */
                const PdCom::Selector &selector = {}, /**< Selector. */
                const Transmission & = Pd::event_mode, /**< Transmission. */
                double scale = 1.0, /**< Scale factor. */
                double offset = 0.0, /**< Offset (applied after scaling). */
                double tau = 0.0 /**< PT1 filter time constant. A value less
                                    or equal to 0.0 means, that no filter is
                                    applied. */
                );

        void setSpeedVariable(
                PdCom::Process *process, /**< Process. */
                const QString &path, /**< Variable path. */
                const PdCom::Selector &selector = {}, /**< Selector. */
                const Transmission & = Pd::event_mode, /**< Transmission. */
                double scale = 1.0, /**< Scale factor. */
                double offset = 0.0, /**< Offset (applied after scaling). */
                double tau = 0.0 /**< PT1 filter time constant. A value less
                                    or equal to 0.0 means, that no filter is
                                    applied. */
                );

        void clearSpeedVariable();

    private:
        struct Impl;
        std::unique_ptr<Impl> impl;

        bool event(QEvent *);
        void resizeEvent(QResizeEvent *);
        void paintEvent(QPaintEvent *);

    private slots:
        void timeout();
};

} // namespace

/****************************************************************************/

#endif
