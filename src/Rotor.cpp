/*****************************************************************************
 *
 * Copyright (C) 2009 - 2019  Florian Pose <fp@igh-essen.com>
 *
 * This file is part of the QtPdWidgets library.
 *
 * The QtPdWidgets library is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * The QtPdWidgets library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser
 * General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with the QtPdWidgets Library. If not, see
 * <http://www.gnu.org/licenses/>.
 *
 ****************************************************************************/

#include "Rotor.h"
using Pd::Rotor;

#include "ScalarSubscriber.h"

#include <pdcom5/Subscriber.h>

#include <QtGui>
#include <QSvgRenderer>
#include <QTimer>

/****************************************************************************/

#define DEFAULT_ANGLE 0.0

#define PERIOD 0.040 // 25 Hz

/****************************************************************************/

struct Rotor::Impl:
    public ScalarSubscriber
{
    Impl(Rotor *);
    ~Impl();

    Rotor * const rotor;

    QString backgroundPath;
    QString rotorPath;
    QString foregroundPath;
    QPointF rotorCenter;
    double globalAngle;

    bool speedDataPresent;
    double speedValue;

    QTimer timer;
    double rotorAngle;

    double imageScale;
    QPointF rotationOffset;

    QSvgRenderer backgroundRenderer;
    bool backgroundLoaded;
    QSvgRenderer rotorRenderer;
    bool rotorLoaded;
    QSvgRenderer foregroundRenderer;
    bool foregroundLoaded;

    void newValues(std::chrono::nanoseconds) override;
    void stateChange(PdCom::Subscription::State) override;
    void updateScale();
};

/****************************************************************************/

Rotor::Rotor(
        QWidget *parent
        ):
    QFrame(parent),
    impl(std::unique_ptr<Impl>(new Impl(this)))
{
}

/****************************************************************************/

Rotor::~Rotor()
{
}

/****************************************************************************/

/** Gives a hint aboute the optimal size.
 */
QSize Rotor::sizeHint() const
{
    return QSize(100, 100);
}

/****************************************************************************/

const QString &Rotor::getBackground() const
{
    return impl->backgroundPath;
}

/****************************************************************************/

void Rotor::setBackground(const QString &path)
{
    if (impl->backgroundPath == path) {
        return;
    }

    impl->backgroundPath = path;

    if (path.isEmpty()) {
        impl->backgroundRenderer.load(QByteArray());
        impl->backgroundLoaded = false;
    }
    else {
        impl->backgroundLoaded = impl->backgroundRenderer.load(path);
    }

    impl->updateScale();
}

/****************************************************************************/

void Rotor::resetBackground()
{
    setBackground(QString());
}

/****************************************************************************/

const QString &Rotor::getRotor() const
{
    return impl->rotorPath;
}

/****************************************************************************/

void Rotor::setRotor(const QString &path)
{
    if (impl->rotorPath == path) {
        return;
    }

    impl->rotorPath = path;

    if (path.isEmpty()) {
        impl->rotorRenderer.load(QByteArray());
        impl->rotorLoaded = false;
    }
    else {
        impl->rotorLoaded = impl->rotorRenderer.load(path);
    }

    impl->updateScale();
}

/****************************************************************************/

void Rotor::resetRotor()
{
    setRotor(QString());
}

/****************************************************************************/

const QString &Rotor::getForeground() const
{
    return impl->foregroundPath;
}

/****************************************************************************/

void Rotor::setForeground(const QString &path)
{
    if (impl->foregroundPath == path) {
        return;
    }

    impl->foregroundPath = path;

    if (path.isEmpty()) {
        impl->foregroundRenderer.load(QByteArray());
        impl->foregroundLoaded = false;
    }
    else {
        impl->foregroundLoaded = impl->foregroundRenderer.load(path);
    }

    update();
}

/****************************************************************************/

void Rotor::resetForeground()
{
    setForeground(QString());
}

/****************************************************************************/

QPointF Rotor::getRotorCenter() const
{
    return impl->rotorCenter;
}

/****************************************************************************/

void Rotor::setRotorCenter(QPointF p)
{
    if (impl->rotorCenter == p) {
        return;
    }

    impl->rotorCenter = p;
    update();
}

/****************************************************************************/

void Rotor::resetRotorCenter()
{
    setRotorCenter(QPointF(0.0, 0.0));
}

/****************************************************************************/

double Rotor::getGlobalAngle() const
{
    return impl->globalAngle;
}

/****************************************************************************/

void Rotor::setGlobalAngle(double a)
{
    if (impl->globalAngle == a) {
        return;
    }

    impl->globalAngle = a;
    impl->updateScale();
}

/****************************************************************************/

void Rotor::resetGlobalAngle()
{
    setGlobalAngle(DEFAULT_ANGLE);
}

/****************************************************************************/

/** Subscribes to a ProcessVariable.
 */
void Rotor::setSpeedVariable(
        PdCom::Variable pv,
        const PdCom::Selector &selector,
        const Transmission &transmission,
        double gain,
        double offset,
        double tau
        )
{
    clearSpeedVariable();

    if (pv.empty()) {
        return;
    }

    impl->setVariable(pv, selector, transmission, gain, offset, tau);
}

/****************************************************************************/

/** Subscribes to a ProcessVariable.
 */
void Rotor::setSpeedVariable(
        PdCom::Process *process,
        const QString &path,
        const PdCom::Selector &selector,
        const Transmission &transmission,
        double gain,
        double offset,
        double tau
        )
{
    clearSpeedVariable();

    if (not process or path.isEmpty()) {
        return;
    }

    impl->setVariable(process, path, selector, transmission, gain, offset,
            tau);
}

/****************************************************************************/

/** Unsubscribe from a Variable.
 */
void Rotor::clearSpeedVariable()
{
    impl->clearVariable();
}

/****************************************************************************/

/** Event function.
 */
bool Rotor::event(
        QEvent *event /**< Event flags. */
        )
{
    return QFrame::event(event);
}

/****************************************************************************/

void Rotor::resizeEvent(QResizeEvent *)
{
    impl->updateScale();
}

/****************************************************************************/

void Rotor::paintEvent(QPaintEvent *event)
{
    QFrame::paintEvent(event);

    QPainter p(this);

    p.setRenderHint(QPainter::Antialiasing);

    if (impl->imageScale == 0.0) {
        return;
    }

    p.scale(impl->imageScale, impl->imageScale);
    p.translate(-impl->rotationOffset);
    p.rotate(impl->globalAngle);

    QSize size;

    if (impl->backgroundPath.isEmpty()) {
        size = impl->rotorRenderer.defaultSize();
    }
    else {
        size = impl->backgroundRenderer.defaultSize();
    }

    QRect renderRect(QPoint(), size);
    impl->backgroundRenderer.render(&p, renderRect);

    p.save();
    p.translate(impl->rotorCenter);
    p.rotate(-impl->rotorAngle);
    p.translate(-impl->rotorCenter);
    impl->rotorRenderer.render(&p, renderRect);
    p.restore();

    impl->foregroundRenderer.render(&p, renderRect);
}

/****************************************************************************/

void Rotor::timeout()
{
    if (impl->speedDataPresent && impl->speedValue != 0.0) {
        impl->rotorAngle += impl->speedValue * PERIOD;
        update();
    }
}

/*****************************************************************************
 * Implementation
 ****************************************************************************/

Rotor::Impl::Impl(Rotor *rotor):
    rotor(rotor),
    rotorCenter(0.0, 0.0),
    globalAngle(DEFAULT_ANGLE),
    speedDataPresent(false),
    rotorAngle(0.0),
    imageScale(0.0),
    backgroundRenderer(rotor),
    backgroundLoaded(false),
    rotorRenderer(rotor),
    rotorLoaded(false),
    foregroundRenderer(rotor),
    foregroundLoaded(false)
{
    connect(&timer, SIGNAL(timeout()), rotor, SLOT(timeout()));

    timer.setSingleShot(false);
    timer.start(PERIOD * 1000);

    updateScale();
}

/****************************************************************************/

Rotor::Impl::~Impl()
{
    timer.stop();
    rotor->clearSpeedVariable();
}

/****************************************************************************/

/** This virtual method is called by the ProcessVariable, if its value
 * changes.
 */
void Rotor::Impl::newValues(std::chrono::nanoseconds)
{
    double value;
    PdCom::details::copyData(&value,
            PdCom::details::TypeInfoTraits<double>::type_info.type,
            getData(), getVariable().getTypeInfo().type, 1);
    value = value * scale + offset;

    if (speedDataPresent) {
        double newValue;

        if (getFilterConstant() > 0.0) {
            newValue =
                getFilterConstant() * (value - speedValue) + speedValue;
        } else {
            newValue = value;
        }

        speedValue = newValue;
    } else {
        speedValue = value; // bypass filter
        speedDataPresent = true;
    }
}

/****************************************************************************/

/** Notification for variable deletion.
 *
 * This virtual function is called by the Variable, when it is about to be
 * destroyed.
 */
void Rotor::Impl::stateChange(PdCom::Subscription::State state)
{
    if (state != PdCom::Subscription::State::Active) {
        speedDataPresent = false;
        rotor->update();
    }
}

/****************************************************************************/

void Rotor::Impl::updateScale()
{
    /* workaround for designer not accepting QString properties as resources.
     * try to reload SVG data on next opportunity. */

    if (!backgroundPath.isEmpty() && !backgroundLoaded) {
        backgroundLoaded = backgroundRenderer.load(backgroundPath);
    }
    if (!rotorPath.isEmpty() && !rotorLoaded) {
        rotorLoaded = rotorRenderer.load(rotorPath);
    }
    if (!foregroundPath.isEmpty() && !foregroundLoaded) {
        foregroundLoaded = foregroundRenderer.load(foregroundPath);
    }

    QSize size;

    if (backgroundPath.isEmpty()) {
        size = rotorRenderer.defaultSize();
    }
    else {
        size = backgroundRenderer.defaultSize();
    }

    QRect renderRect(QPoint(), size);
    QMatrix rot;

    rot.rotate(globalAngle);
    QRect rotBound = rot.mapRect(renderRect);

    if (rotBound.width() > 0) {
        imageScale = 1.0 * rotor->contentsRect().width() / rotBound.width();
        rotationOffset = rotBound.topLeft();
    }
    else {
        imageScale = 0.0;
    }

    rotor->update();
}

/****************************************************************************/
