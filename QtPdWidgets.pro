#-----------------------------------------------------------------------------
#
# vim: syntax=config
#
# Copyright (C) 2012-2021  Florian Pose <fp@igh.de>
#
# This file is part of the QtPdWidgets library.
#
# The QtPdWidgets library is free software: you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public License as
# published by the Free Software Foundation, either version 3 of the License,
# or (at your option) any later version.
#
# The QtPdWidgets library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser
# General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with the QtPdWidgets Library. If not, see
# <http://www.gnu.org/licenses/>.
#
#-----------------------------------------------------------------------------

TEMPLATE = lib
TARGET = QtPdWidgets

# good to know: if compiling for android the unix target is selected as well

# On release:
# - Change version in Doxyfile
# - Change version in .spec file
# - Add a NEWS entry
VERSION = 2.0.0

MAJOR = $$section(VERSION, ., 0, 0)
TARGET_MAJOR = $${TARGET}$${MAJOR} # with major number
BASENAME = $$TARGET # without major number

unix {
    TARGET = $${TARGET_MAJOR}
}

greaterThan(QT_MAJOR_VERSION, 4) {
    CONFIG += debug_and_release
    QT += widgets
    !android {
        QT += designer
    }
    DEFINES += PD_QT5 # needed for moc processing of src/WidgetCollection.h
}
else {
    CONFIG += designer release
}

CONFIG += plugin

QT += network xml svg

greaterThan(QT_MAJOR_VERSION, 4) {
    QT += quick
}

# Path definitions (can be overridden via qmake command-line) ----------------

# Installation prefix (used for header installation only)
isEmpty(PD_INSTALL_PREFIX) {
    PD_INSTALL_PREFIX = $$[QT_INSTALL_PREFIX]
}

# Library installation path
isEmpty(PD_INSTALL_LIBS) {
    PD_INSTALL_LIBS = $$[QT_INSTALL_LIBS]
}

# DLL installation path
isEmpty(PD_INSTALL_BINS) {
    PD_INSTALL_BINS = $$[QT_INSTALL_BINS]
}

# Plugin installation path (i. e. the Qt designer plugin)
isEmpty(PD_INSTALL_PLUGINS) {
    PD_INSTALL_PLUGINS = $$[QT_INSTALL_PLUGINS]
}

# QML installation path

#the evaluation of isEmpty() does not work properly
#on qmake properties, so use a helper variable
HELPER = $$[QT_INSTALL_QML]
   
isEmpty(PD_INSTALL_QML) {
    isEmpty(HELPER) {
        # fallback for older Qt5 versions
        PD_INSTALL_QML = $$[QT_INSTALL_LIBS]/qt5/qml
    }
    else {
        PD_INSTALL_QML = $$[QT_INSTALL_QML]
    }
}

message("PD_INSTALL_QML"$$PD_INSTALL_QML)

!isEmpty(PD_LIB_PREFIX) {
    INCLUDEPATH += $${PD_LIB_PREFIX}/include
    LIBS += -L$${PD_LIB_PREFIX}/lib
}

# Compilation flags and directories ------------------------------------------

DEPENDPATH += .
MOC_DIR = .moc
OBJECTS_DIR = .obj

!android {
    CONFIG += c++14
}

INCLUDEPATH += $${PWD}/$${TARGET_MAJOR}

LIBS += -lpdcom5 -lexpat

win32 {
    LIBS += -lwsock32 # for gethostname in Process.cpp
    CONFIG += dll
}

# Output code coverage -------------------------------------------------------

!isEmpty(PD_COVERAGE){
    QMAKE_CXXFLAGS_DEBUG -= -O2
    QMAKE_CXXFLAGS_RELEASE -= -O2
    QMAKE_CXXFLAGS += --coverage -g -O0
    LIBS += -lgcov
}

# Install designer plugin ----------------------------------------------------

target.path = $${PD_INSTALL_PLUGINS}/designer

isEmpty(PD_DISABLE_DESIGNER){
    INSTALLS += target
}

# Install libraries and DLLs -------------------------------------------------

libraries.path = $$PD_INSTALL_LIBS

unix | android { # android just for clarification (see comment above)
    libraries.files = $${OUT_PWD}/lib$${TARGET_MAJOR}.so

    INSTALLS += libraries
}


win32 {
    CONFIG(release, debug|release):DIR = "release"
    CONFIG(debug, debug|release):DIR = "debug"

    libraries.files = "$${OUT_PWD}/$${DIR}/lib$${TARGET_MAJOR}.dll.a"

    dlls.path = $$PD_INSTALL_BINS
    dlls.files = "$${OUT_PWD}/$${DIR}/$${TARGET_MAJOR}.dll"

    INSTALLS += dlls libraries
}

# install headers ------------------------------------------------------------

inst_headers.path = $${PD_INSTALL_PREFIX}/include/$${TARGET_MAJOR}

# place files here which are depended on QWidget
QWIDGETDEPHEADERFILES = \
    $${TARGET_MAJOR}/Bar.h \
    $${TARGET_MAJOR}/CheckBox.h \
    $${TARGET_MAJOR}/ClipImage.h \
    $${TARGET_MAJOR}/CursorEditWidget.h \
    $${TARGET_MAJOR}/Dial.h \
    $${TARGET_MAJOR}/Digital.h \
    $${TARGET_MAJOR}/DoubleSpinBox.h \
    $${TARGET_MAJOR}/Export.h \
    $${TARGET_MAJOR}/Graph.h \
    $${TARGET_MAJOR}/Image.h \
    $${TARGET_MAJOR}/Led.h \
    $${TARGET_MAJOR}/MultiLed.h \
    $${TARGET_MAJOR}/NoPdTouchEdit.h \
    $${TARGET_MAJOR}/PushButton.h \
    $${TARGET_MAJOR}/RadioButton.h \
    $${TARGET_MAJOR}/Rotor.h \
    $${TARGET_MAJOR}/Scale.h \
    $${TARGET_MAJOR}/Settings.h \
    $${TARGET_MAJOR}/SpinBox.h \
    $${TARGET_MAJOR}/Svg.h \
    $${TARGET_MAJOR}/TableView.h \
    $${TARGET_MAJOR}/Tank.h \
    $${TARGET_MAJOR}/Text.h \
    $${TARGET_MAJOR}/Time.h \
    $${TARGET_MAJOR}/TimeScale.h \
    $${TARGET_MAJOR}/TouchEdit.h \
    $${TARGET_MAJOR}/TouchEditDialog.h \
    $${TARGET_MAJOR}/XYGraph.h

inst_headers.files = \
    $${TARGET_MAJOR}/Message.h \
    $${TARGET_MAJOR}/MessageModel.h \
    $${TARGET_MAJOR}/Process.h \
    $${TARGET_MAJOR}/ScalarSubscriber.h \
    $${TARGET_MAJOR}/ScalarVariable.h \
    $${TARGET_MAJOR}/ScalarVariant.h \
    $${TARGET_MAJOR}/TableColumn.h \
    $${TARGET_MAJOR}/TableModel.h \
    $${TARGET_MAJOR}/Translator.h \
    $${TARGET_MAJOR}/Transmission.h \
    $${TARGET_MAJOR}/ValueRing.h \
    $${TARGET_MAJOR}/VectorVariant.h \
    $${TARGET_MAJOR}/Widget.h

greaterThan(QT_MAJOR_VERSION, 4) {
    inst_headers.files += $${TARGET_MAJOR}/LiveSvg.h
}

!android {
    inst_headers.files += $${QWIDGETDEPHEADERFILES}
}

INSTALLS += inst_headers

# CMake Config for find_package(qtpdwidgets2) --------------------------------

QMAKE_SUBSTITUTES += qtpdwidgets2-config.cmake.in

cmake_files.path = $$PD_INSTALL_LIBS/cmake/$$lower($${TARGET_MAJOR})
cmake_files.files = qtpdwidgets2-config.cmake

INSTALLS += cmake_files

# install QML files ----------------------------------------------------------

QML_PATH = $${PD_INSTALL_QML}/de/igh/PdQmlWidgets.2

inst_qml.path = $${QML_PATH}
inst_qml.files = \
    qml/QmlPdWidgets2.h \
    qml/de_igh_pdQmlWidgets.pri \
    qml/de_igh_pdQmlWidgets.qrc \
    qml/FontAwesome.otf \
    qml/qmldir \
    qml/Sprintf.js \
    qml/PdBar.qml \
    qml/PdCheckBox.qml \
    qml/PdComboBox.qml \
    qml/PdDigital.qml \
    qml/PdVectorDigital.qml \
    qml/PdLabel.qml \
    qml/PdText.qml \
    qml/PdImage.qml \
    qml/PdLed.qml \
    qml/PdMultiLed.qml \
    qml/PdPushButton.qml \
    qml/PdToolButton.qml \
    qml/PdButton.qml \
    qml/PdSwitch.qml \
    qml/PdStatusIndicator.qml \
    qml/PdSlider.qml \
    qml/PdTimeLabel.qml \
    qml/PdVectorLed.qml \
    qml/PdTouchEdit.qml \
    qml/TouchEdit.qml \
    qml/TouchEditDialog.qml

inst_qml_style.path = $${QML_PATH}/style
inst_qml_style.files = \
    qml/style/qmldir \
    qml/style/Solarized.qml

inst_qml_quick1.path = $${QML_PATH}/quick1
inst_qml_quick1.files = \
    qml/quick1/qmldir \
    qml/quick1/PdLed.qml \
    qml/quick1/PdMultiLed.qml

inst_qml_material.path = $${QML_PATH}/+material
inst_qml_material.files = \
    qml/+material/qmldir \
    qml/+material/PdBar.qml \
    qml/+material/PdToolButton.qml \
    qml/+material/PdTouchEdit.qml \
    qml/+material/TouchEditDialog.qml \

inst_qml_universal.path = $${QML_PATH}/+universal
inst_qml_universal.files = \
    qml/+universal/qmldir \
    qml/+universal/TouchEditDialog.qml

INSTALLS += \
    inst_qml \
    inst_qml_style \
    inst_qml_quick1 \
    inst_qml_material \
    inst_qml_universal

# define headers and sources -------------------------------------------------

HEADERS += \
    $${inst_headers.files}

!android {
    HEADERS += \
        src/BarSection.h \
        src/BarStack.h
}

# place files here which are dependent on QWidget
QWIDGETDEPSOURCES = \
    src/Bar.cpp \
    src/BarSection.cpp \
    src/BarStack.cpp \
    src/CheckBox.cpp \
    src/ClipImage.cpp \
    src/CursorEditWidget.cpp \
    src/Dial.cpp \
    src/Digital.cpp \
    src/DoubleSpinBox.cpp \
    src/Graph.cpp \
    src/Image.cpp \
    src/Led.cpp \
    src/MultiLed.cpp \
    src/NoPdTouchEdit.cpp \
    src/PushButton.cpp \
    src/RadioButton.cpp \
    src/Rotor.cpp \
    src/Scale.cpp \
    src/Settings.cpp \
    src/SpinBox.cpp \
    src/Svg.cpp \
    src/TableView.cpp \
    src/Time.cpp \
    src/TimeScale.cpp \
    src/Tank.cpp \
    src/Text.cpp \
    src/TouchEdit.cpp \
    src/TouchEditDialog.cpp \
    src/XYGraph.cpp

SOURCES += \
    src/Message.cpp \
    src/MessageModel.cpp \
    src/Process.cpp \
    src/ScalarSubscriber.cpp \
    src/ScalarVariant.cpp \
    src/TableColumn.cpp \
    src/TableModel.cpp \
    src/Translator.cpp \
    src/Transmission.cpp \
    src/VectorVariant.cpp \
    src/Widget.cpp

greaterThan(QT_MAJOR_VERSION, 4) {
    SOURCES += src/LiveSvg.cpp
}

!android {
    SOURCES += $${QWIDGETDEPSOURCES}
}

!android | isEmpty(PD_DISABLE_PLUGIN) {
    HEADERS += \
        src/Plugin.h \
        src/WidgetCollection.h

    SOURCES += \
        src/Plugin.cpp \
        src/WidgetCollection.cpp
}

RESOURCES = QtPdWidgets.qrc QtPdWidgets_ts.qrc

# translations ---------------------------------------------------------------

TRANSLATIONS = \
    QtPdWidgets_de.ts \
    QtPdWidgets_nl.ts

QM_FILES = $$replace(TRANSLATIONS, "ts$", "qm")

CODECFORTR = UTF-8

# additional files to install ------------------------------------------------

ADDITIONAL_DISTFILES = \
    AUTHORS \
    COPYING \
    COPYING.LESSER \
    Doxyfile \
    NEWS \
    images/* \
    QtPdWidgets.spec \
    README \
    TODO \
    mydist.sh

DISTFILES += $${ADDITIONAL_DISTFILES}

# building documentation -----------------------------------------------------

unix {
    dox.target = dox
    dox.commands = doxygen Doxyfile
    QMAKE_EXTRA_TARGETS += dox

    tags.target = tags
    tags.commands = etags --members $${SOURCES} $${HEADERS}
    QMAKE_EXTRA_TARGETS += tags

    MYDISTFILES = \
        $${ADDITIONAL_DISTFILES} \
        $${BASENAME}.pro \
        $${HEADERS} \
        $${QM_FILES} \
        $${RESOURCES} \
        $${SOURCES} \
        $${TRANSLATIONS} \
        example/MainWindow.cpp \
        example/MainWindow.h \
        example/MainWindow.ui \
        example/main.cpp \
        example/test.pro

    mydist.target = mydist
    mydist.commands = ./mydist.sh $${BASENAME}-$${VERSION} $${MYDISTFILES}
    QMAKE_EXTRA_TARGETS += mydist
}
    
#-----------------------------------------------------------------------------
