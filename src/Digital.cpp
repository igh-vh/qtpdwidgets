/*****************************************************************************
 *
 * Copyright (C) 2009 - 2013  Florian Pose <fp@igh-essen.com>
 *
 * This file is part of the QtPdWidgets library.
 *
 * The QtPdWidgets library is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * The QtPdWidgets library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser
 * General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with the QtPdWidgets Library. If not, see
 * <http://www.gnu.org/licenses/>.
 *
 ****************************************************************************/

#include "Digital.h"

#include <QtGui>

#include <cmath>

using Pd::Digital;

/****************************************************************************/

#define DEFAULT_ALIGNMENT      (Qt::AlignRight | Qt::AlignVCenter)
#define DEFAULT_DECIMALS       0
#define DEFAULT_SUFFIX         ""
#define DEFAULT_TIME_DISPLAY   (None)
#define DEFAULT_BASE           (10)

#define SECONDS_PER_MINUTE (60.0)
#define SECONDS_PER_HOUR   (SECONDS_PER_MINUTE * 60.0)

/****************************************************************************/

struct Digital::Impl
{
    Impl(Digital *);

    Digital * const digital;

    bool dataPresent; /**< True, if data were received. */
    double value; /**< Current value. */
    bool redraw; /**< Value shall be redrawn on next redraw event. */

    Qt::Alignment alignment; /**< Text alignment. */
    quint32 decimals; /**< Number of decimal digits. */
    QString suffix; /**< Suffix, that is appended to the displayed
                      string. The suffix is appended without a separator
                      (like in other Qt classes), so if you want to
                      specify a unit, you'll have to set suffix to
                      " kN", for example. */
    TimeDisplay timeDisplay; /**< Time display. */
    int base; /**< Number base (10 = decimal). */
    QString displayText; /**< Displayed text. */

    void drawText(QPaintEvent *, QPainter &);
    void outputValue();
    void retranslate();
};

/****************************************************************************/

/** Constructor.
 */
Digital::Digital(
        QWidget *parent /**< Parent widget. */
        ):
    QFrame(parent),
    impl(std::unique_ptr<Impl>(new Impl(this)))
{
    setMinimumSize(40, 25);
    setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Fixed);

    connect(getTimer(), SIGNAL(timeout()), this, SLOT(redrawEvent()));

    impl->retranslate();
}

/****************************************************************************/

/** Destructor.
 */
Digital::~Digital()
{
}

/****************************************************************************/

void Digital::clearData()
{
    impl->dataPresent = false;
    impl->outputValue();
}

/****************************************************************************/

/**
 * \return The current #value.
 */
double Digital::getValue() const
{
    return impl->value;
}

/****************************************************************************/

/**
 * \return The text #alignment.
 */
Qt::Alignment Digital::getAlignment() const
{
    return impl->alignment;
}

/****************************************************************************/

/** Sets the text #alignment.
 */
void Digital::setAlignment(Qt::Alignment a)
{
    if (impl->alignment != a) {
        impl->alignment = a;
        update(contentsRect());
    }
}

/****************************************************************************/

/** Resets the number of #decimals.
 */
void Digital::resetAlignment()
{
    setAlignment(DEFAULT_ALIGNMENT);
}

/****************************************************************************/

/**
 * \return The number of displayed #decimals.
 */
quint32 Digital::getDecimals() const
{
    return impl->decimals;
}

/****************************************************************************/

/** Sets the number of #decimals.
 */
void Digital::setDecimals(quint32 decimals)
{
    if (impl->decimals != decimals) {
        impl->decimals = decimals;
        impl->outputValue();
    }
}

/****************************************************************************/

/** Resets the number of #decimals.
 */
void Digital::resetDecimals()
{
    setDecimals(DEFAULT_DECIMALS);
}

/****************************************************************************/

/**
 * \return The #suffix.
 */
const QString &Digital::getSuffix() const
{
    return impl->suffix;
}

/****************************************************************************/

/** Sets the #suffix to display after the value.
 */
void Digital::setSuffix(const QString &suffix)
{
    if (impl->suffix != suffix) {
        impl->suffix = suffix;
        impl->outputValue();
    }
}

/****************************************************************************/

/** Resets the #suffix to display after the value.
 */
void Digital::resetSuffix()
{
    setSuffix(DEFAULT_SUFFIX);
}

/****************************************************************************/

Digital::TimeDisplay Digital::getTimeDisplay() const
{
    return impl->timeDisplay;
}

/****************************************************************************/

/** Sets the #timeDisplay method.
 */
void Digital::setTimeDisplay(TimeDisplay timeDisplay)
{
    if (impl->timeDisplay != timeDisplay) {
        impl->timeDisplay = timeDisplay;
        impl->outputValue();
    }
}

/****************************************************************************/

/** Resets the #timeDisplay method.
 */
void Digital::resetTimeDisplay()
{
    setTimeDisplay(DEFAULT_TIME_DISPLAY);
}

/****************************************************************************/

/**
 * \return The #base.
 */
int Digital::getBase() const
{
    return impl->base;
}

/****************************************************************************/

/** Sets the number #base.
 */
void Digital::setBase(int base)
{
    if (impl->base != base) {
        impl->base = base;
        impl->outputValue();
    }
}

/****************************************************************************/

/** Resets the number #base.
 */
void Digital::resetBase()
{
    setBase(DEFAULT_BASE);
}

/****************************************************************************/

/** Gives a hint aboute the optimal size.
 */
QSize Digital::sizeHint() const
{
    return QSize(60, 25);
}

/****************************************************************************/

/** Event function.
 */
bool Digital::event(
        QEvent *event /**< Event flags. */
        )
{
    switch (event->type()) {
        case QEvent::LanguageChange:
            impl->retranslate();
            impl->outputValue(); // FIXME here necessary?
            break;
        case QEvent::LocaleChange:
            impl->outputValue();
            break;
        default:
            break;
    }

    return QFrame::event(event);
}

/****************************************************************************/

/** Paint function.
 */
void Digital::paintEvent(
        QPaintEvent *event /**< Paint event flags. */
        )
{
    QFrame::paintEvent(event);
    QPainter painter(this);
    impl->drawText(event, painter);
}

/****************************************************************************/

/** This virtual method is called by the ProcessVariable, if its value
 * changes.
 */
void Digital::newValues(std::chrono::nanoseconds)
{
    double v;
    PdCom::details::copyData(&v,
            PdCom::details::TypeInfoTraits<double>::type_info.type,
            getData(), getVariable().getTypeInfo().type, 1);
    v = v * scale + offset;

    if (impl->dataPresent) {
        double newValue;
#ifdef PD_DEBUG_DIGITAL
        qDebug() << "digital " << v << " filter "  << getFilterConstant();
#endif

        if (getFilterConstant() > 0.0) {
            newValue = getFilterConstant() * (v - impl->value) + impl->value;
        } else {
            newValue = v;
        }

        if (newValue != impl->value) {
            impl->value = newValue;
            impl->redraw = true;
        }
    } else {
        impl->value = v; // bypass filter
        impl->dataPresent = true;
        impl->outputValue();
    }
}

/****************************************************************************/

/** Redraw event slot, that is called by the redraw timer.
 */
void Digital::redrawEvent()
{
    if (impl->redraw) {
        impl->redraw = false;
        impl->outputValue();
    }
}

/*****************************************************************************
 * Implementation
 ****************************************************************************/

Digital::Impl::Impl(Digital *digital):
    digital(digital),
    dataPresent(false),
    value(0.0),
    redraw(false),
    alignment(DEFAULT_ALIGNMENT),
    decimals(DEFAULT_DECIMALS),
    suffix(DEFAULT_SUFFIX),
    timeDisplay(DEFAULT_TIME_DISPLAY),
    base(DEFAULT_BASE)
{
}

/****************************************************************************/

/** Draws the text.
 */
void Digital::Impl::drawText(
        QPaintEvent *event, /**< Paint event flags. */
        QPainter &painter /**< Painter. */
        )
{
    if (event->rect().intersects(digital->contentsRect())) {
        painter.drawText(digital->contentsRect(), alignment, displayText);
    }
}

/****************************************************************************/

/** Displays the current value.
 */
void Digital::Impl::outputValue()
{
    QString str;

    if (dataPresent) {
        switch (timeDisplay) {
            case None:
                if (base == 10 or base < 2 or base > 36) {
                    str = QLocale().toString(value, 'f', decimals);
                }
                else {
                    str = QString::number((unsigned int) value, base);
                }
                break;

            case Seconds:
            case Minutes:
            case Hours:
                {
                    double rest;
                    int tmp;

                    if (value >= 0.0) {
                        rest = value;
                    } else {
                        rest = value * -1.0;
                        str += "-";
                    }

                    // hours
                    tmp = (int) (rest / SECONDS_PER_HOUR);
                    rest -= tmp * SECONDS_PER_HOUR;
                    str += QLocale().toString(tmp);

                    // minutes
                    if (timeDisplay <= Minutes) {
                        tmp = (int) (rest / SECONDS_PER_MINUTE);
                        rest -= tmp * SECONDS_PER_MINUTE;
                        str += ":";
                        if (tmp < 10.0) {
                            str += "0";
                        }
                        str += QLocale().toString(tmp);
                    }

                    // seconds
                    if (timeDisplay == Seconds) {
                        str += ":";
                        if (rest < 10.0) {
                            str += "0";
                        }

                        double digitValue = pow(10.0, -(int) decimals);
                        rest = floor(rest / digitValue) * digitValue;
                        str += QLocale().toString(rest, 'f', decimals);
                    }
                }
                break;

            default:
                break;
        }

        str += suffix;
    }

    if (displayText != str) {
        displayText = str;
        digital->update(digital->contentsRect());
    }
}

/****************************************************************************/

/** Retranslate the widget.
 */
void Digital::Impl::retranslate()
{
    digital->setWindowTitle(Pd::Digital::tr("Digital display"));
}

/****************************************************************************/
