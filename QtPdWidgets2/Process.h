/*****************************************************************************
 *
 * Copyright (C) 2009  Florian Pose <fp@igh-essen.com>
 *
 * This file is part of the QtPdWidgets library.
 *
 * The QtPdWidgets library is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * The QtPdWidgets library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser
 * General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with the QtPdWidgets Library. If not, see
 * <http://www.gnu.org/licenses/>.
 *
 ****************************************************************************/

#ifndef PD_PROCESS_H
#define PD_PROCESS_H

#include <QTcpSocket>

#include <pdcom5.h>
#include <pdcom5/Process.h>

#ifndef PDCOM_VERSION_CODE
# error "No PDCOM_VERSION_CODE found."
#elif \
    !PDCOM_DEVEL \
    && (PDCOM_VERSION_CODE < PDCOM_VERSION(5, 0, 0) \
    || PDCOM_VERSION_CODE >= PDCOM_VERSION(6, 0, 0))
# error "Invalid PdCom version."
#endif

namespace Pd {

/****************************************************************************/

/** PdCom::Process implementation for Qt.
 */
class Q_DECL_EXPORT Process:
    public QObject, public PdCom::Process
{
    Q_OBJECT

    public:
        Process();
        virtual ~Process();

        void setApplicationName(const QString &);
        void connectToHost(const QString &, quint16 = 2345);
        void disconnectFromHost();

        /** State of the process connection.
         */
        enum ConnectionState {
            Disconnected, /**< Process disconnected. */
            Connecting, /**< Currently connecting. */
            Connected, /**< Process connection established. */
            ConnectError, /**< An error happened while connecting. */
            ConnectedError /**< An error happened, after the connection was
                             established. */
        };
        ConnectionState getConnectionState() const;
        bool isConnected() const;
        const QString &getErrorString() const;
        QString getPeerName() const;

        void find(const QString &);
        void sendBroadcast(const QString &, const QString &attr = "text");

        quint64 getRxBytes() const;
        quint64 getTxBytes() const;

        static Pd::Process *getDefaultProcess();
        static void setDefaultProcess(Pd::Process *);

    private:
        struct Impl;
        std::unique_ptr<Impl> impl;

        /** Virtual from PdCom::Process. */
        std::string applicationName() const override;
        int read(char *, int) override;
        void write(const char *, size_t) override;
        void flush() override;
        void connected() override;
        void broadcastReply(
                const std::string &message,
                const std::string &attr,
                std::chrono::nanoseconds time_ns,
                const std::string &user) override;

        void reset();

        /** Disconnect method inherited from QObject.
         *
         * This is made private, to avoid confusion.
         */
        bool disconnect(
                const char *signal = 0, /**< Signal. */
                const QObject *receiver = 0, /**< Receiver. */
                const char *method = 0 /**< Method. */
                );

    signals:
        /** Connection established.
         *
         * This is emitted after the connection is established.
         */
        void processConnected();

        /** Disconnected gracefully.
         *
         * This is only emitted, after the user called disconnectFromHost().
         */
        void disconnected();

        /** Connection error.
         *
         * This is emitted after a connection error or when the connection was
         * closed due to a parser error.
         */
        void error();

        void broadcastReceived(
                const QString &message,
                const QString &attr,
                std::uint64_t time_ns,
                const QString &user);

    private slots:
        void socketConnected();
        void socketDisconnected();
        void socketError();
        void socketRead();
};

/****************************************************************************/

} // namespace

#if QT_VERSION < 0x050000
Q_DECLARE_METATYPE(Pd::Process *)
#endif

#endif
