/*****************************************************************************
 *
 * Copyright (C) 2009 - 2014  Florian Pose <fp@igh-essen.com>
 *
 * This file is part of the QtPdWidgets library.
 *
 * The QtPdWidgets library is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * The QtPdWidgets library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser
 * General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with the QtPdWidgets Library. If not, see
 * <http://www.gnu.org/licenses/>.
 *
 ****************************************************************************/

#include "Image.h"
using Pd::Image;

#include <QtGui>

/****************************************************************************/

struct Image::Impl
{
    Impl(Image *parent):
        parent(parent),
        value(0),
        dataPresent(false),
        pixmapHash(NULL),
        angle(0.0)
    {
    }

    Image * const parent;

    int value; /**< The current value from the process. */
    bool dataPresent; /**< There is a data value to display. */
    const PixmapHash *pixmapHash; /**< The PixmapHash to use. */
    QPixmap defaultPixmap; /**< Default pixmap. */
    qreal angle; /**< Designer angle. */
    QPixmap displayPixmap; /**< Current pixmap. */

    class Transformation;
    class FixedTranslation;
    class VariableTranslation;
    class FixedRotation;
    class VariableRotation;
    QList<Transformation *> transformationList; /**< List of
                                                  transformations. */

    /** Displays a new Pixmap depending on the process #value.
     *
     * \todo default image?
     */
    void updatePixmap()
    {
        if (dataPresent && pixmapHash && pixmapHash->contains(value)) {
            displayPixmap = pixmapHash->value(value);
        }
        else {
            displayPixmap = defaultPixmap;
        }

        parent->update();
    }

    /** Retranslate the widget.
     */
    void retranslate()
    {
        parent->setWindowTitle(Pd::Image::tr("Image"));
    }

};

/****************************************************************************/

class Image::Impl::Transformation
{
    public:
        Transformation(Pd::Image *image): image(image) {}
        virtual ~Transformation() {}

        virtual void push(QPainter &) = 0;

    protected:
        Image * const image;
};

/****************************************************************************/

class Image::Impl::FixedTranslation:
    public Image::Impl::Transformation
{
    public:
        FixedTranslation(Image *image, qreal x, qreal y):
            Transformation(image),
            x(x),
            y(y)
        {}

        virtual void push(QPainter &p) {
            p.translate(x, y);
        }

    private:
        qreal x;
        qreal y;
};

/****************************************************************************/

class Image::Impl::VariableTranslation:
    public Image::Impl::Transformation,
    public ScalarSubscriber
{
    public:
        VariableTranslation(
                Image *image,
                Image::Axis axis,
                PdCom::Variable pv,
                const PdCom::Selector &selector,
                const Transmission &transmission,
                double scale,
                double offset,
                double tau
                ):
            Transformation(image),
            axis(axis),
            value(0.0)
        {
            setVariable(pv, selector, transmission, scale, offset, tau);
        }

        VariableTranslation(
                Image *image,
                Image::Axis axis,
                PdCom::Process *process,
                const QString &path,
                const PdCom::Selector &selector,
                const Transmission &transmission,
                double scale,
                double offset,
                double tau
                ):
            Transformation(image),
            axis(axis),
            value(0.0)
        {
            setVariable(process, path, selector, transmission, scale, offset,
                    tau);
        }

        virtual void push(QPainter &p) {
            switch (axis) {
                case X:
                    p.translate(value, 0.0);
                    break;
                case Y:
                    p.translate(0.0, value);
                    break;
            }
        }

    private:
        Image::Axis axis;
        double value;

        void newValues(std::chrono::nanoseconds) override {
            double newValue;
            PdCom::details::copyData(&newValue,
                    PdCom::details::TypeInfoTraits<double>::type_info.type,
                    getData(), getVariable().getTypeInfo().type, 1);
            value = newValue * scale + offset;
            image->update();
        }
};

/****************************************************************************/

class Image::Impl::FixedRotation:
    public Image::Impl::Transformation
{
    public:
        FixedRotation(Image *image, qreal alpha):
            Transformation(image),
            alpha(alpha)
        {}

        virtual void push(QPainter &p) {
            p.rotate(alpha);
        }

    private:
        qreal alpha;
};

/****************************************************************************/

class Image::Impl::VariableRotation:
    public Image::Impl::Transformation,
    public ScalarSubscriber
{
    public:
        VariableRotation(
                Image *image,
                PdCom::Variable pv,
                const PdCom::Selector &selector,
                const Transmission &transmission,
                double scale,
                double offset,
                double tau
                ):
            Transformation(image),
            value(0.0)
        {
            setVariable(pv, selector, transmission, scale, offset, tau);
        }

        VariableRotation(
                Image *image,
                PdCom::Process *process,
                const QString &path,
                const PdCom::Selector &selector,
                const Transmission &transmission,
                double scale,
                double offset,
                double tau
                ):
            Transformation(image),
            value(0.0)
        {
            setVariable(process, path, selector, transmission, scale, offset,
                    tau);
        }

        virtual void push(QPainter &p) {
            p.rotate(value);
        }

    private:
        double value;

        void newValues(std::chrono::nanoseconds) override {
            double newValue;
            PdCom::details::copyData(&newValue,
                    PdCom::details::TypeInfoTraits<double>::type_info.type,
                    getData(), getVariable().getTypeInfo().type, 1);
            value = newValue * scale + offset;
            image->update();
        }
};

/****************************************************************************/

/** Constructor.
 */
Image::Image(
        QWidget *parent /**< Parent widget. */
        ):
    QFrame(parent),
    impl{std::unique_ptr<Impl>(new Impl(this))}
{
    impl->retranslate();
}

/****************************************************************************/

/** Destructor.
 */
Image::~Image()
{
    clearTransformations();
}

/****************************************************************************/

void Image::clearData()
{
    impl->dataPresent = false;
    impl->updatePixmap();
}

/****************************************************************************/

/**
 * \return The current process #value.
 */
inline int Image::getValue() const
{
    return impl->value;
}

/****************************************************************************/

/** Sets the current #value.
 *
 * If the value has changed, updatePixmap() is called.
 */
void Image::setValue(int value)
{
    if (value != impl->value || !impl->dataPresent) {
        impl->value = value;
        impl->dataPresent = true;
        impl->updatePixmap();
    }
}

/****************************************************************************/

/** Sets the #PixmapHash to use.
 */
void Image::setPixmapHash(const PixmapHash *ph)
{
    if (ph != impl->pixmapHash) {
        impl->pixmapHash = ph;
        impl->updatePixmap();
    }
}

/****************************************************************************/

const QPixmap &Image::getDefaultPixmap() const
{
    return impl->defaultPixmap;
}

/****************************************************************************/

/** Sets the #defaultPixmap.
 */
void Image::setDefaultPixmap(const QPixmap &p)
{
    impl->defaultPixmap = p;
    impl->updatePixmap();
}

/****************************************************************************/

/** Resets the #defaultPixmap.
 */
void Image::resetDefaultPixmap()
{
    setDefaultPixmap(QPixmap());
}

/****************************************************************************/

qreal Image::getAngle() const
{
    return impl->angle;
}

/****************************************************************************/

/** Sets the #angle to use.
 */
void Image::setAngle(qreal a)
{
    if (a != impl->angle) {
        impl->angle = a;
        update();
    }
}

/****************************************************************************/

/** Resets the #angle.
 */
void Image::resetAngle()
{
    setAngle(0.0);
}

/****************************************************************************/

void Image::clearTransformations()
{
    for (QList<Impl::Transformation *>::iterator i =
            impl->transformationList.begin();
            i != impl->transformationList.end();
            i++) {
        delete *i;
    }

    impl->transformationList.clear();
}

/****************************************************************************/

void Image::translate(qreal x, qreal y)
{
    Impl::FixedTranslation *t = new Impl::FixedTranslation(this, x, y);
    impl->transformationList.append(t);
}

/****************************************************************************/

void Image::translate(
        Axis axis,
        PdCom::Variable pv,
        const PdCom::Selector &selector,
        const Transmission &transmission,
        double scale,
        double offset,
        double tau
        )
{
    Impl::VariableTranslation *t = new Impl::VariableTranslation(this, axis,
            pv, selector, transmission, scale, offset, tau);
    impl->transformationList.append(t);
}

/****************************************************************************/

void Image::translate(
        Axis axis,
        PdCom::Process *process,
        const QString &path,
        const PdCom::Selector &selector,
        const Transmission &transmission,
        double scale,
        double offset,
        double tau
        )
{
    Impl::VariableTranslation *t = new Impl::VariableTranslation(this, axis,
            process, path, selector, transmission, scale, offset, tau);
    impl->transformationList.append(t);
}

/****************************************************************************/

void Image::rotate(qreal alpha)
{
    Impl::FixedRotation *t = new Impl::FixedRotation(this, alpha);
    impl->transformationList.append(t);
}

/****************************************************************************/

void Image::rotate(
        PdCom::Variable pv,
        const PdCom::Selector &selector,
        const Transmission &transmission,
        double scale,
        double offset,
        double tau
        )
{
    Impl::VariableRotation *t = new Impl::VariableRotation(this, pv, selector,
            transmission, scale, offset, tau);
    impl->transformationList.append(t);
}

/****************************************************************************/

void Image::rotate(
        PdCom::Process *process,
        const QString &path,
        const PdCom::Selector &selector,
        const Transmission &transmission,
        double scale,
        double offset,
        double tau
        )
{
    Impl::VariableRotation *t = new Impl::VariableRotation(this, process,
            path, selector, transmission, scale, offset, tau);
    impl->transformationList.append(t);
}

/****************************************************************************/

/** Event handler.
 */
bool Image::event(
        QEvent *event /**< Paint event flags. */
        )
{
    if (event->type() == QEvent::LanguageChange) {
        impl->retranslate();
    }

    return QFrame::event(event);
}

/****************************************************************************/

void Image::paintEvent(QPaintEvent *event)
{
    Q_UNUSED(event);

    QPainter p(this);

    p.setRenderHints(
            QPainter::Antialiasing |
            QPainter::SmoothPixmapTransform);

    QRectF targetRect(impl->displayPixmap.rect());
    QRectF sourceRect(impl->displayPixmap.rect());
    targetRect.moveLeft((contentsRect().width() - sourceRect.width()) / 2.0);
    targetRect.moveTop((contentsRect().height() - sourceRect.height()) / 2.0);

    QPointF center = targetRect.center();
    p.translate(center);
    p.rotate(impl->angle);

    for (QList<Impl::Transformation *>::const_iterator i =
            impl->transformationList.begin();
            i != impl->transformationList.end();
            i++) {
        (*i)->push(p);
    }

    p.translate(-center);

    p.drawPixmap(targetRect, impl->displayPixmap, sourceRect);
}

/****************************************************************************/

/** This virtual method is called by the ProcessVariable, if its value
 * changes.
 */
void Image::newValues(std::chrono::nanoseconds)
{
    int32_t value;
    PdCom::details::copyData(&value,
            PdCom::details::TypeInfoTraits<int32_t>::type_info.type,
            getData(), getVariable().getTypeInfo().type, 1);
    value = value * scale + offset;
    setValue(value);
}

/****************************************************************************/
