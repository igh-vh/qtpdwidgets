/*****************************************************************************
 *
 * Copyright (C) 2012  Florian Pose <fp@igh-essen.com>
 *
 * This file is part of the QtPdWidgets library.
 *
 * The QtPdWidgets library is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * The QtPdWidgets library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser
 * General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with the QtPdWidgets Library. If not, see
 * <http://www.gnu.org/licenses/>.
 *
 ****************************************************************************/

#include "MainWindow.h"

#include "QtPdWidgets2/Translator.h"

#include <pdcom5/Subscription.h>

#include <QMessageBox>
#include <QDebug>
#include <math.h>

#include <iostream>
using namespace std;

#include <chrono>
using namespace std::chrono_literals;

#define PERIOD 10ms

/****************************************************************************/

MainWindow::MainWindow(QWidget *parent):
    QMainWindow(parent)
{
    setupUi(this);

    Pd::Widget::setRedrawInterval(100);

    doubleSpinBox->setMinimum(-2);

    bar1->setOrientation(Pd::Bar::Horizontal);
    bar1->setScaleMin(0.0);
    bar1->setScaleMax(40.0);
    bar1->setOrigin(Pd::Bar::OriginMinimum);

    bar2->setScaleMin(-10.0);
    bar2->setScaleMax(40.0);
    //bar2->setOrigin(Pd::Bar::OriginMaximum);

    scroll1->setScaleMax(100);
    scroll1->setScaleMin(0);
    scroll1->setTimeRange(30);

    scroll2->setScaleMax(30.0);
    scroll2->setScaleMin(0.0);
    scroll2->setTimeRange(10);

    scroll3->setScaleMax(100.0);
    scroll3->setScaleMin(0.0);
    scroll3->setTimeRange(1);
    scroll3->setMode(Pd::Graph::Trigger);
    scroll3->setTriggerPosition(0.5);

    xyGraph->setScaleXMin(0.0);
    xyGraph->setScaleXMax(30.0);
    xyGraph->setScaleYMin(0.0);
    xyGraph->setScaleYMax(30.0);
    xyGraph->setTimeRange(1);

    colorHash.insert(0, QColor(200, 200, 200));
    colorHash.insert(1, Qt::yellow);
    multiLed->setHash(&colorHash);

    tableModel = new Pd::TableModel();
    tableCol1 = new Pd::TableColumn("Param1");
    tableModel->addColumn(tableCol1);
    tableCol2 = new Pd::TableColumn("Param2");
    tableModel->addColumn(tableCol2);
    pdTableView->setModel(tableModel);

    connect(&p, SIGNAL(processConnected()), this, SLOT(processConnected()));
    connect(&p, SIGNAL(disconnected()), this, SLOT(processDisconnected()));
    connect(&p, SIGNAL(error()), this, SLOT(processError()));

    t.setSingleShot(true);
    connect(&t, SIGNAL(timeout()), this, SLOT(timeout()));

    actionDisconnect->setEnabled(0);
    actionConnect->trigger();

    QGradientStops stops;
    stops.append(QGradientStop(30.0, Qt::red));
    stops.append(QGradientStop(10.0, Qt::blue));
    pdDial->setGradientStops(stops);
    pdDial->setScaleMin(-10);
    pdDial->setMajorStops(5);
}

/****************************************************************************/

MainWindow::~MainWindow()
{
    p.disconnectFromHost();
}

/****************************************************************************/

void MainWindow::processConnected()
{
    QString path;
    PdCom::Selector all;

    actionDisconnect->setEnabled(1);

    pdImage->clearTransformations();

    path = "/osc/cos";
    //pdImage->translate(Pd::Image::X, pv, 0.05, 50.0);
    try {
        // FIXME throws at runtime!
        pdImage->rotate(&p, path, all, Pd::Poll{1s}, 45.0);
    }
    catch (std::exception &e) {
        qWarning() << "EEE rotate" << e.what();
    }
    pdDial->currentValue.setVariable(&p, path, all, 100ms);

    pdImage->translate(0.0, 10.0);

    doubleSpinBox->setVariable(&p, "/osc/amplitude/Setpoint", all,
            Pd::event_mode, 0.5);


    path = "/osc/amplitude/Setpoint";
    time->setVariable(&p, path, all, Pd::event_mode, 5e3);
    pdTouchEdit->setVariable(&p, path);
    tableCol1->setVariable(&p, path);

    path = "/Taskinfo/0/ExecTime";

#if 1
    try {

        // FIXME
        // throws at compile-time
        //constexpr auto dt = Pd::Transmission(-3ms);
        //scroll1->setVariable(&p, path, all, dt, 1e6);

        // throws at run-time (if negative)
        scroll1->setVariable(&p, path, all, 2ms, 1e6);
    }
    catch (std::exception &e) {
        qWarning() << "EEE graph" << e.what();
    }
    scroll1->addVariable(&p, path, all, 2ms, 2e6, 0, 1.0, Qt::darkGreen);
    scroll2->setVariable(&p, path, all, 2ms, 1e6);
    scroll2->addVariable(&p, path, all, 2ms, 1e6, 0.0, 0.05, Qt::red);
    scroll3->setVariable(&p, path, all, 2ms, 1e6);
    scroll3->addVariable(&p, path, all, 2ms, 1e6, 0.0, 0.05, Qt::red);
#endif
    xyGraph->addVariable(&p, path, all, 10ms, 1e6);
    xyGraph->addVariable(&p, path, all, 10ms, 1e6);


    bar1->clearVariables();
    bar1->addVariable(&p, path, all, 10ms, 1e6, 0.0, 1.0, Qt::darkBlue);
    bar1->addStackedVariable(&p, path, all, 10ms, 1e6, 0.0, 1.0, Qt::green);
    bar1->addVariable(&p, path, all, 10ms, 2e6, 0.0, 1.0, Qt::darkGreen);
    bar1->addVariable(&p, path, all, 10ms, 2e6, 0.0, 0.0, Qt::darkRed);

    QColor c(Qt::darkRed);
    c.setAlpha(150);
    bar2->setVariable(&p, path, all, Pd::event_mode, 1.0, 0.0, 0.0, c);
    c =  Qt::red;
    c.setAlpha(150);
    bar2->addStackedVariable(&p, path, all, 10ms, 1e6, 0.0, 1.0, c);
    bar2->addVariable(&p, path, all, 10ms, 1e6, 0.0, 1.0, Qt::blue);
    bar2->addStackedVariable(&p, path, all, 10ms, 1e6, 0.0, 1.0, c);
    bar2->addVariable(&p, path, all, 10ms, 1e6, 0.0, 10.0, Qt::darkBlue);

    digital->setVariable(&p, path, all, 10ms, 1e6, 0.0, 5.0);
    digital_2->setVariable(&p, path, all, PERIOD, 1e6, 0.0, 5.0);
    digital_3->setVariable(&p, path, all, PERIOD, 1e6, 0.0, 5.0);
    digital_4->setVariable(&p, path, all, PERIOD, 1e6, 0.0, 5.0);
    digital_5->setVariable(&p, path, all, PERIOD, 1e6, 0.0, 5.0);
    digital_6->setVariable(&p, path, all, PERIOD, 1e6, 0.0, 5.0);
    digital_7->setVariable(&p, path, all, PERIOD, 1e6, 0.0, 5.0);
    digital_8->setVariable(&p, path, all, PERIOD, 1e6, 0.0, 5.0);
    digital_9->setVariable(&p, path, all, PERIOD, 1e6, 0.0, 5.0);
    digital_10->setVariable(&p, path, all, PERIOD, 1e6, 0.0, 5.0);
    digital_11->setVariable(&p, path, all, PERIOD, 1e6, 0.0, 5.0);
    digital_12->setVariable(&p, path, all, PERIOD, 1e6, 0.0, 5.0);
    digital_13->setVariable(&p, path, all, PERIOD, 1e6, 0.0, 5.0);
    digital_14->setVariable(&p, path, all, PERIOD, 1e6, 0.0, 5.0);
    digital_15->setVariable(&p, path, all, PERIOD, 1e6, 0.0, 5.0);
    digital_16->setVariable(&p, path, all, PERIOD, 1e6, 0.0, 5.0);
    digital_17->setVariable(&p, path, all, PERIOD, 1e6, 0.0, 5.0);
    digital_18->setVariable(&p, path, all, PERIOD, 1e6, 0.0, 5.0);
    digital_19->setVariable(&p, path, all, PERIOD, 1e6, 0.0, 5.0);
    digital_20->setVariable(&p, path, all, PERIOD, 1e6, 0.0, 5.0);
    digital_21->setVariable(&p, path, all, PERIOD, 1e6, 0.0, 5.0);
    digital_22->setVariable(&p, path, all, PERIOD, 1e6, 0.0, 5.0);
    digital_23->setVariable(&p, path, all, PERIOD, 1e6, 0.0, 5.0);
    digital_24->setVariable(&p, path, all, PERIOD, 1e6, 0.0, 5.0);
    digital_25->setVariable(&p, path, all, PERIOD, 1e6, 0.0, 5.0);
    digital_26->setVariable(&p, path, all, PERIOD, 1e6, 0.0, 5.0);
    digital_27->setVariable(&p, path, all, PERIOD, 1e6, 0.0, 5.0);
    digital_28->setVariable(&p, path, all, PERIOD, 1e6, 0.0, 5.0);
    digital_29->setVariable(&p, path, all, PERIOD, 1e6, 0.0, 5.0);
    digital_30->setVariable(&p, path, all, PERIOD, 1e6, 0.0, 5.0);
    digital_31->setVariable(&p, path, all, PERIOD, 1e6, 0.0, 5.0);
    digital_32->setVariable(&p, path, all, PERIOD, 1e6, 0.0, 5.0);
    digital_33->setVariable(&p, path, all, PERIOD, 1e6, 0.0, 5.0);
    digital_34->setVariable(&p, path, all, PERIOD, 1e6, 0.0, 5.0);
    digital_35->setVariable(&p, path, all, PERIOD, 1e6, 0.0, 5.0);
    digital_36->setVariable(&p, path, all, PERIOD, 1e6, 0.0, 5.0);
    digital_37->setVariable(&p, path, all, PERIOD, 1e6, 0.0, 5.0);
    digital_38->setVariable(&p, path, all, PERIOD, 1e6, 0.0, 5.0);
    digital_39->setVariable(&p, path, all, PERIOD, 1e6, 0.0, 5.0);
    digital_40->setVariable(&p, path, all, PERIOD, 1e6, 0.0, 5.0);
    digital_41->setVariable(&p, path, all, PERIOD, 1e6, 0.0, 5.0);
    digital_42->setVariable(&p, path, all, PERIOD, 1e6, 0.0, 5.0);
    digital_43->setVariable(&p, path, all, PERIOD, 1e6, 0.0, 5.0);
    digital_44->setVariable(&p, path, all, PERIOD, 1e6, 0.0, 5.0);
    digital_45->setVariable(&p, path, all, PERIOD, 1e6, 0.0, 5.0);
    digital_46->setVariable(&p, path, all, PERIOD, 1e6, 0.0, 5.0);
    digital_47->setVariable(&p, path, all, PERIOD, 1e6, 0.0, 5.0);
    digital_48->setVariable(&p, path, all, PERIOD, 1e6, 0.0, 5.0);
    digital_49->setVariable(&p, path, all, PERIOD, 1e6, 0.0, 5.0);
    digital_50->setVariable(&p, path, all, PERIOD, 1e6, 0.0, 5.0);
    digital_51->setVariable(&p, path, all, PERIOD, 1e6, 0.0, 5.0);
    digital_52->setVariable(&p, path, all, PERIOD, 1e6, 0.0, 5.0);
    digital_53->setVariable(&p, path, all, PERIOD, 1e6, 0.0, 5.0);
    digital_54->setVariable(&p, path, all, PERIOD, 1e6, 0.0, 5.0);
    digital_55->setVariable(&p, path, all, PERIOD, 1e6, 0.0, 5.0);
    digital_56->setVariable(&p, path, all, PERIOD, 1e6, 0.0, 5.0);
    digital_57->setVariable(&p, path, all, PERIOD, 1e6, 0.0, 5.0);
    digital_58->setVariable(&p, path, all, PERIOD, 1e6, 0.0, 5.0);
    digital_59->setVariable(&p, path, all, PERIOD, 1e6, 0.0, 5.0);
    digital_60->setVariable(&p, path, all, PERIOD, 1e6, 0.0, 5.0);
    digital_61->setVariable(&p, path, all, PERIOD, 1e6, 0.0, 5.0);
    digital_62->setVariable(&p, path, all, PERIOD, 1e6, 0.0, 5.0);
    digital_63->setVariable(&p, path, all, PERIOD, 1e6, 0.0, 5.0);
    digital_64->setVariable(&p, path, all, PERIOD, 1e6, 0.0, 5.0);
    digital_65->setVariable(&p, path, all, PERIOD, 1e6, 0.0, 5.0);
    digital_66->setVariable(&p, path, all, PERIOD, 1e6, 0.0, 5.0);
    digital_67->setVariable(&p, path, all, PERIOD, 1e6, 0.0, 5.0);
    digital_68->setVariable(&p, path, all, PERIOD, 1e6, 0.0, 5.0);
    digital_69->setVariable(&p, path, all, PERIOD, 1e6, 0.0, 5.0);
    digital_70->setVariable(&p, path, all, PERIOD, 1e6, 0.0, 5.0);
    digital_71->setVariable(&p, path, all, PERIOD, 1e6, 0.0, 5.0);
    digital_72->setVariable(&p, path, all, PERIOD, 1e6, 0.0, 5.0);
    digital_73->setVariable(&p, path, all, PERIOD, 1e6, 0.0, 5.0);
    digital_74->setVariable(&p, path, all, PERIOD, 1e6, 0.0, 5.0);
    digital_75->setVariable(&p, path, all, PERIOD, 1e6, 0.0, 5.0);
    digital_76->setVariable(&p, path, all, PERIOD, 1e6, 0.0, 5.0);

    path = "/osc/amplitude/Limit";
    tableCol2->setVariable(&p, path);

    path = "/osc/enable";
    pushButton->setVariable(&p, path);
    checkBox->setVariable(&p, path);
    led->setVariable(&p, path);
    multiLed->setVariable(&p, path);
    pdRadioButton1->setVariable(&p, path);
    pdRadioButton2->setVariable(&p, path);

    t.start(1000);
}

/****************************************************************************/

void MainWindow::processDisconnected()
{
    actionConnect->setEnabled(1);
    actionDisconnect->setEnabled(0);
}

/****************************************************************************/

void MainWindow::processError()
{
    actionConnect->setEnabled(true);

    QMessageBox::critical(this, tr("Connection error"),
            tr("Failed to connect to data source."));
}

/****************************************************************************/

void MainWindow::on_actionConnect_triggered()
{
    actionConnect->setEnabled(false);
    p.connectToHost("localhost", 2345);
}

/****************************************************************************/

void MainWindow::on_actionDisconnect_triggered()
{
    actionDisconnect->setEnabled(false);
    p.disconnectFromHost();
}

/****************************************************************************/

void MainWindow::on_actionGerman_triggered()
{
    Pd::loadTranslation("de");
}

/****************************************************************************/

void MainWindow::on_actionEnglish_triggered()
{
    Pd::loadTranslation("en");
}

/****************************************************************************/

void MainWindow::on_sliderLevel_valueChanged(int value)
{
    scroll3->setManualTriggerLevel(value / 50.0);
    bar1->setScaleMax(value / 20); // from 0 to 100
}

/****************************************************************************/

void MainWindow::on_sliderPos_valueChanged(int value)
{
    scroll3->setTriggerPosition(value / 1000.0);
}

/****************************************************************************/

void MainWindow::on_sliderRange_valueChanged(int value)
{
    double timeRange = pow(10, 2.0 * value / 1000.0 - 2);
    scroll3->setTimeRange(timeRange);
}

/****************************************************************************/

void MainWindow::on_sliderScale_valueChanged(int value)
{
    scroll3->setScaleMax(value / 10.0);
}

/****************************************************************************/

void MainWindow::on_checkBoxAuto_stateChanged(int s)
{
    bool check = s == Qt::Checked;
    sliderLevel->setEnabled(!check);
    scroll3->setTriggerLevelMode(
            check ? Pd::Graph::AutoLevel : Pd::Graph::ManualLevel);
    pdTouchEdit->setEnabled(check);
}

/****************************************************************************/

void MainWindow::timeout()
{
    qDebug() << "t";
}

/****************************************************************************/

void MainWindow::on_pushButton_clicked()
{
#if 0
    Pd::ValueRing<double> values = scroll3->getValues();
    unsigned int i;

    cout.precision(16);
    for (i = 0; i < values.getLength(); i++) {
        cout << (double) values[i].first << " " << values[i].second << endl;
    }
#endif

    t.start(0);

    tableCol1->setHeader("Test");
}

/****************************************************************************/
