/*****************************************************************************
 *
 * Copyright (C) 2009 - 2012  Florian Pose <fp@igh-essen.com>
 *
 * This file is part of the QtPdWidgets library.
 *
 * The QtPdWidgets library is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * The QtPdWidgets library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser
 * General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with the QtPdWidgets Library. If not, see
 * <http://www.gnu.org/licenses/>.
 *
 ****************************************************************************/

#include "Text.h"
using Pd::Text;
using Pd::TextCondition;

#include <QtGui>
#include <QStyle>

#define DEFAULT_ALIGNMENT (Qt::AlignLeft | Qt::AlignVCenter)

/****************************************************************************/

struct TextCondition::Impl
{
    Impl(TextCondition *parentCond, Text *parentText):
        parentCond{parentCond},
        parentText{parentText},
        invert{false}
    {
    }

    TextCondition * const parentCond;
    Text * const parentText;
    QString text;
    bool invert;
};

/****************************************************************************/

/** Condition constructor.
 */
TextCondition::TextCondition(
        Text *parent /**< Parent text widget. */
        ):
    impl{std::unique_ptr<Impl>{new Impl(this, parent)}}
{
}

/****************************************************************************/

/** Condition destructor.
 */
TextCondition::~TextCondition()
{
}

/****************************************************************************/

const QString &TextCondition::getText() const
{
    return impl->text;
}

/****************************************************************************/

void TextCondition::setText(const QString &t)
{
    impl->text = t;

    impl->parentText->conditionChanged();
}

/****************************************************************************/

bool TextCondition::getInvert() const
{
    return impl->invert;
}

/****************************************************************************/

void TextCondition::setInvert(bool i)
{
    impl->invert = i;

    impl->parentText->conditionChanged();
}

/****************************************************************************/

struct Text::Impl
{
    Impl(Text *parent):
        parent{parent},
        alignment(DEFAULT_ALIGNMENT),
        processValue(0),
        dataPresent(false),
        hash(NULL),
        conditionIndex(0),
        conditionActive(false)
    {
    }

    Text * const parent;

    Qt::Alignment alignment; /**< Text alignment. */
    QString prefix; /**< Prefix to display before the #displayValue text.
                     */
    QString suffix; /**< Suffix to display after the #displayValue text.
                     */
    int processValue; /**< The current value from the process. */
    bool dataPresent; /**< There is a process value to display. */
    const Hash *hash; /**< The hash to use. */
    Value displayValue; /**< Value object to display. */
    QString displayText; /**< Display text. */
    QColor displayColor; /**< Display color. */
    QFont displayFont; /**< Display font. */
    QList<TextCondition *> conditions;
    int conditionIndex;
    QTimer conditionTimer;
    bool conditionActive;


    /** Looks up the text to display and calls updateDisplayText(), if necessary.
     *
     * This method is public, because it has to be called on language changes.
     */
    void updateValueText()
    {
        if (dataPresent && hash && hash->contains(processValue)) {
            displayValue = hash->value(processValue);
            displayValue.text = prefix + displayValue.text + suffix;
        } else {
            displayValue = Value();
        }

        updateDisplayText();
    }

    /** Updates the widget, if the text changed.
     */
    void updateDisplayText()
    {
        bool lastCondActive = conditionActive;
        bool newCondActive = false;

        /* Display condition text, if the current condition is not true. */
        if (conditionIndex < conditions.size()) {
            TextCondition *cond = conditions[conditionIndex];
            if (cond->hasData() && !(cond->getValue() ^ cond->getInvert())) {
                newCondActive = true;
                displayText = cond->getText();
            }
        }

        if (!newCondActive) {
            /* Otherwise display hash text. */
            displayText = displayValue.text;
            displayColor = displayValue.color;
            displayFont = displayValue.font;
        }

        conditionActive = newCondActive;
        if (newCondActive != lastCondActive) {
            parent->style()->unpolish(parent);
            parent->style()->polish(parent);
        }

        parent->update();
    }

    void findCondition()
    {
        if (conditionIndex >= conditions.size()) {
            conditionIndex = 0;
            updateDisplayText();
            return;
        }

        int lastIndex = conditionIndex;

        /* Find a false condition to display. */
        TextCondition *cond = conditions[conditionIndex];
        while (!cond->hasData() || (cond->getValue() ^ cond->getInvert())) {
            conditionIndex++;
            if (conditionIndex >= conditions.size()) {
                conditionIndex = 0;
            }
            if (conditionIndex == lastIndex) {
                // no condition found
                conditionTimer.stop();
                updateDisplayText();
                return;
            }
            cond = conditions[conditionIndex];
        }

        /* found a condition */
        updateDisplayText();

        if (!conditionTimer.isActive()) {
            conditionTimer.start();
        }
    }

    /** Retranslate the widget.
     */
    void retranslate()
    {
        parent->setWindowTitle(Pd::Text::tr("Text"));
    }

};

/****************************************************************************/

/** Constructor.
 */
Text::Text(
        QWidget *parent /**< Parent widget. */
        ): QFrame(parent),
    impl{std::unique_ptr<Impl>{new Impl(this)}}
{
    impl->updateValueText();

    impl->conditionTimer.setSingleShot(false);
    impl->conditionTimer.setInterval(2000);

    connect(&impl->conditionTimer, SIGNAL(timeout()),
            this, SLOT(conditionTimeout()));

    impl->retranslate();
}

/****************************************************************************/

/** Destructor.
 */
Text::~Text()
{
    clearConditions();
}

/****************************************************************************/

Qt::Alignment Text::getAlignment() const
{
    return impl->alignment;
}

/****************************************************************************/

/** Sets the #alignment.
 */
void Text::setAlignment(Qt::Alignment a)
{
    if (a != impl->alignment) {
        impl->alignment = a;
        update();
    }
}

/****************************************************************************/

/** Resets the #prefix.
 */
void Text::resetAlignment()
{
    setAlignment(DEFAULT_ALIGNMENT);
}

/****************************************************************************/

const QString &Text::getPrefix() const
{
    return impl->prefix;
}

/****************************************************************************/

/** Sets the #prefix.
 */
void Text::setPrefix(const QString &p)
{
    if (p != impl->prefix) {
        impl->prefix = p;
        impl->updateDisplayText();
    }
}

/****************************************************************************/

/** Resets the #prefix.
 */
void Text::resetPrefix()
{
    setPrefix("");
}

/****************************************************************************/

const QString &Text::getSuffix() const
{
    return impl->suffix;
}

/****************************************************************************/

/** Sets the #suffix.
 */
void Text::setSuffix(const QString &s)
{
    if (s != impl->suffix) {
        impl->suffix = s;
        impl->updateDisplayText();
    }
}

/****************************************************************************/

/** Resets the #suffix.
 */
void Text::resetSuffix()
{
    setSuffix("");
}

/****************************************************************************/

void Text::clearData()
{
    impl->dataPresent = false;
    impl->updateValueText();
}

/****************************************************************************/

int Text::getValue() const
{
    return impl->processValue;
}

/****************************************************************************/

/** Sets the current #processValue.
 *
 * If the value changed, updateValueText() is called.
 */
void Text::setValue(int v)
{
    if (impl->processValue != v || !impl->dataPresent) {
        impl->processValue = v;
        impl->dataPresent = true;
        impl->updateValueText();
    }
}

/****************************************************************************/

/** Sets the hash to use.
 */
void Text::setHash(
        const Hash *h /**< The new hash. */
        )
{
    if (h != impl->hash) {
        impl->hash = h;
        impl->updateValueText();
    }
}

/****************************************************************************/

TextCondition *Text::addCondition(
        PdCom::Variable pv,
        const QString &text,
        bool invert
        )
{
    TextCondition *cond = new TextCondition(this);
    cond->setText(text);
    cond->setInvert(invert);
    cond->setVariable(pv);
    impl->conditions.append(cond);

    connect(cond, SIGNAL(valueChanged()), this, SLOT(conditionChanged()));

    return cond;
}

/****************************************************************************/

void Text::clearConditions()
{
    impl->conditionTimer.stop();

    QList<TextCondition *>::const_iterator i = impl->conditions.constBegin();
    while (i != impl->conditions.constEnd()) {
        delete *i;
        ++i;
    }

    impl->conditions.clear();
    impl->conditionIndex = 0;
}

/****************************************************************************/

bool Text::getConditionActive() const
{
    return impl->conditionActive;
}

/****************************************************************************/

/** Event handler.
 */
bool Text::event(
        QEvent *event /**< Event flags. */
        )
{
    if (event->type() == QEvent::LanguageChange) {
        impl->retranslate();
    }

    return QFrame::event(event);
}

/****************************************************************************/

/** Paint function.
 */
void Text::paintEvent(
        QPaintEvent *event /**< Paint event flags. */
        )
{
    QFrame::paintEvent(event);

    QPainter painter(this);

    if (event->rect().intersects(contentsRect())) {
        if (!impl->conditionActive) { // use style if condition active
            QPen pen(painter.pen());
            if (impl->displayColor.isValid()) {
                pen.setColor(impl->displayColor);
            }
            painter.setPen(pen);
            painter.setFont(impl->displayFont);
        }
        painter.drawText(contentsRect(), impl->alignment | Qt::TextWordWrap,
                impl->displayText);
    }
}

/****************************************************************************/

/** This virtual method is called by the ProcessVariable, if its value
 * changes.
 */
void Text::newValues(std::chrono::nanoseconds)
{
    int32_t v;
    PdCom::details::copyData(&v,
            PdCom::details::TypeInfoTraits<int32_t>::type_info.type,
            getData(), getVariable().getTypeInfo().type, 1);
    v = v * scale + offset;

    if (impl->dataPresent) {
        double newValue;

        if (getFilterConstant() > 0.0) {
            newValue = getFilterConstant() * (v - impl->processValue)
                + impl->processValue;
        } else {
            newValue = v;
        }

        setValue(newValue);
    }
    else {
        setValue(v);
    }
}

/****************************************************************************/

void Text::conditionChanged()
{
    impl->findCondition();
}

/****************************************************************************/

void Text::conditionTimeout()
{
    impl->conditionIndex++;
    if (impl->conditionIndex >= impl->conditions.size()) {
        impl->conditionIndex = 0;
    }

    impl->findCondition();
}

/****************************************************************************/

/** Overloads the insert function of QHash.
 */
void Text::Hash::insert(
        int value, /**< Process value. */
        const QString &t, /**< Text to display. */
        QColor c, /**< Text color. */
        const QFont &f /**< Font to use. */
        )
{
    QHash<int, Value>::insert(value, Value(t, c, f));
}

/****************************************************************************/

void Text::updateValueText()
{
    impl->updateValueText();
}

/****************************************************************************/
