/*****************************************************************************
 *
 * Copyright (C) 2021 Richard Hacker (lerichi at gmx dot net),
 *                    Florian Pose (fp at igh dot de),
 *                    Bjarne von Horn (vh at igh dot de).
 *
 * This file is part of the QtPdWidgets library.
 *
 * The QtPdWidgets library is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * The QtPdWidgets library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser
 * General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with the QtPdWidgets Library. If not, see
 * <http://www.gnu.org/licenses/>.
 *
 ****************************************************************************/

/** @file */

#ifndef PDCOM5_SUBSCRIPTION_H
#define PDCOM5_SUBSCRIPTION_H

#include <string>

#include "Selector.h"
#include "Variable.h"

/****************************************************************************/

namespace PdCom {

class Process;
class Subscriber;

class Subscription
{
    public:
        Subscription(
                Subscriber &subscriber,
                const Variable &variable,
                const Selector &selector = {});

        Subscription(
                Subscriber &subscriber,
                Process &process,
                const std::string &path,
                const Selector &selector = {});

        enum class State {
            Invalid = 0,
            Pending,
            Active,
        };

        State getState() const noexcept { return state_; }

        void poll();

        Variable getVariable() const { return *variable_; }

        const void *getData() const;

        // mocked methods
        Variable *getVariablePtr() const { return variable_; }
        void mockValue();

    private:
        State state_ = State::Invalid;
        Subscriber &subscriber_;
        Process *process_;
        std::string path_;
        Variable *variable_;
        Selector selector_;
};

} // namespace PdCom

/****************************************************************************/

#endif
