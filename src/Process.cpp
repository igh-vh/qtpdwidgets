/*****************************************************************************
 *
 * Copyright (C) 2009-2021  Florian Pose <fp@igh.de>
 *
 * This file is part of the QtPdWidgets library.
 *
 * The QtPdWidgets library is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * The QtPdWidgets library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser
 * General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with the QtPdWidgets Library. If not, see
 * <http://www.gnu.org/licenses/>.
 *
 ****************************************************************************/

#ifdef __WIN32__
#include <windows.h> // GetUserName(), gethostname()
#include <lmcons.h> // UNLEN
#else
#include <unistd.h> // getlogin()
#endif

#include <string>
#include <list>

#include "Process.h"

#define DEBUG_DATA 0

using Pd::Process;


/****************************************************************************/

struct Process::Impl
{
    Impl(Process *process):
        process(process),
        appName("QtPdWidgets2"),
        socketValid(false),
        connectionState(Disconnected),
        rxBytes(0),
        txBytes(0)
    {
        // set the default process to the always last instance of process
        defaultProcess = process;
    }

    Process * const process;

    QString appName; /**< Our application name, that is announced to the
                       server. Default: QtPdWidgets.  */
    QTcpSocket socket; /**< TCP socket to the process. */
    bool socketValid; /**< Connection state of the socket. */
    ConnectionState connectionState; /**< The current connection state. */
    QString errorString; /**< Error reason. Set, before error() is
                           emitted. */
    quint64 rxBytes;
    quint64 txBytes;

    static Pd::Process *defaultProcess; /**< last created process
                                          is the default process */
};

/****************************************************************************/

Pd::Process *Pd::Process::Impl::defaultProcess = nullptr;

/****************************************************************************/

/** Constructor.
 */
Process::Process():
    PdCom::Process(),
    impl{std::unique_ptr<Impl>{new Impl{this}}}
{
    connect(&impl->socket, SIGNAL(connected()),
            this, SLOT(socketConnected()));
    connect(&impl->socket, SIGNAL(disconnected()),
            this, SLOT(socketDisconnected()));
    connect(&impl->socket, SIGNAL(error(QAbstractSocket::SocketError)),
            this, SLOT(socketError()));
    connect(&impl->socket, SIGNAL(readyRead()),
            this, SLOT(socketRead()));
}

/****************************************************************************/

/** Destructor.
 */
Process::~Process()
{
    // reset default process if this instance is the default
    if (impl->defaultProcess == this) {
        impl->defaultProcess = nullptr;
    }

    disconnectFromHost();
}

/****************************************************************************/

/** Sets the application name.
 */
void Process::setApplicationName(const QString &name)
{
    impl->appName = name;
}

/****************************************************************************/

/** Starts to connect to a process.
 */
void Process::connectToHost(const QString &address, quint16 port)
{
    impl->connectionState = Connecting;
    impl->socket.connectToHost(address, port);
}

/****************************************************************************/

/** Disconnects from a process.
 */
void Process::disconnectFromHost()
{
    switch (impl->connectionState) {
        case Connecting:
        case Connected:
            impl->socketValid = false;
            impl->connectionState = Disconnected;
            impl->rxBytes = 0;
            impl->txBytes = 0;
            reset();
            impl->socket.disconnectFromHost();
            emit disconnected();
            break;

        default:
            break;
    }
}

/****************************************************************************/

/**
 * \return The #connectionState.
 */
Process::ConnectionState Process::getConnectionState() const
{
    return impl->connectionState;
}

/****************************************************************************/

/**
 * \return \a true, if the process is connected.
 */
bool Process::isConnected() const
{
    return impl->connectionState == Connected;
}

/****************************************************************************/

/**
 * \return Error reason after the error() signal was emitted.
 */
const QString &Process::getErrorString() const
{
    return impl->errorString;
}

/****************************************************************************/

/**
 * \return Host name of the process.
 */
QString Process::getPeerName() const
{
    return impl->socket.peerName();
}

/****************************************************************************/

/** Wrapper function for Process::findVariable.
 */
void Process::find(const QString &path)
{
    PdCom::Process::find(path.toLocal8Bit().constData());
}

/****************************************************************************/

/** Send a broadcast message.
 */
void Process::sendBroadcast(const QString &msg, const QString &attr)
{
    broadcast(msg.toLocal8Bit().constData(),
            attr.toLocal8Bit().constData());
}

/****************************************************************************/

quint64 Process::getRxBytes() const
{
    return impl->rxBytes;
}

/****************************************************************************/

quint64 Process::getTxBytes() const
{
    return impl->txBytes;
}

/****************************************************************************/

Pd::Process *Process::getDefaultProcess()
{
    return Pd::Process::Impl::defaultProcess;
}

/****************************************************************************/

/** Set default process "manually"
 */
void Process::setDefaultProcess(Pd::Process *process)
{
    Pd::Process::Impl::defaultProcess = process;
}

/*****************************************************************************
 * private methods
 ****************************************************************************/

std::string Process::applicationName() const
{
    return impl->appName.toLocal8Bit().constData();
}

/****************************************************************************/

/** Read data from the socket.
 */
int Process::read(char *buf, int count)
{
    qint64 ret(impl->socket.read(buf, count));
    if (ret > 0) {
        impl->rxBytes += ret;
    }
    return ret;
}

/****************************************************************************/

/** Sends data via the socket.
 *
 * This is the implementation of the virtual PdCom::Process
 * method to enable the Process object to send data to the process.
 */
void Process::write(const char *buf, size_t count)
{
#if DEBUG_DATA
    qDebug() << "Writing:" << count << QByteArray(buf, count);
#endif
    while (count > 0) {
        qint64 ret(impl->socket.write(buf, count));
        if (ret <= 0) {
            qWarning("write() failed.");
            impl->socketValid = false;
            impl->rxBytes = 0;
            impl->txBytes = 0;
            reset();
            impl->socket.disconnectFromHost();
            emit error();
            return;
        }
        count -= ret;
        buf += ret;
        impl->txBytes += ret;
    }
}

/****************************************************************************/

/** Flushed the socket.
 */
void Process::flush()
{
#if DEBUG_DATA
    qDebug() << "Flushing not implemented.";
#endif
}

/****************************************************************************/

/** The process is connected and ready.
 *
 * This virtual function from PdCom::Process has to be overloaded to let
 * subclasses know about this event.
 */
void Process::connected()
{
    impl->connectionState = Connected;
    emit processConnected();
}

/****************************************************************************/

/** Broadcast Reply.
 */
void Process::broadcastReply(
        const std::string &message,
        const std::string &attr,
        std::chrono::nanoseconds time_ns,
        const std::string &user)

{
    emit broadcastReceived(
            QString::fromStdString(message),
            QString::fromStdString(attr),
            time_ns.count(),
            QString::fromStdString(user));
}

/****************************************************************************/

/** Resets the PdCom process.
 */
void Process::reset()
{
    try {
        PdCom::Process::reset();
    } catch (std::exception &e) {
        // do nothing
    }
}

/****************************************************************************/

/** Socket connection established.
 *
 * This is called, when the pure socket connection was established and the
 * Process object can start using it.
 */
void Process::socketConnected()
{
    impl->socketValid = true;
    impl->socket.setSocketOption(QAbstractSocket::KeepAliveOption, 1);
}

/****************************************************************************/

/** Socket disconnected.
 *
 * The socket was closed and the process has to be told, that it is
 * disconnected.
 */
void Process::socketDisconnected()
{
    switch (impl->connectionState) {
        case Connecting:
            impl->socketValid = false;
            impl->connectionState = ConnectError;
            impl->rxBytes = 0;
            impl->txBytes = 0;
            reset();
            emit error();
            break;

        case Connected:
            impl->socketValid = false;
            impl->connectionState = Disconnected;
            impl->rxBytes = 0;
            impl->txBytes = 0;
            reset();
            emit disconnected();
            break;

        default:
            break;
    }
}

/****************************************************************************/

/** There was a socket error.
 *
 * The error could come up either while connecting the socket, or when the
 * socket was already connected.
 */
void Process::socketError()
{
    impl->errorString = impl->socket.errorString();

    switch (impl->connectionState) {
        case Connecting:
            impl->socketValid = false;
            impl->connectionState = ConnectError;
            impl->rxBytes = 0;
            impl->txBytes = 0;
            reset();
            emit error();
            break;

        case Connected:
            impl->socketValid = false;
            impl->connectionState = ConnectedError;
            impl->rxBytes = 0;
            impl->txBytes = 0;
            reset();
            emit error();
            break;

        default:
            break;
    }
}

/****************************************************************************/

/** The socket has new data to read.
 */
void Process::socketRead()
{
    try {
        while (impl->socket.bytesAvailable() > 0) {
            asyncData();
        }
    }
    catch (std::exception &e) {
        impl->errorString = "Exception during asyncData(): ";
        impl->errorString += e.what();
        qCritical() << impl->errorString;
        impl->socketValid = false;
        if (impl->connectionState == Connected) {
            impl->connectionState = ConnectedError;
        } else {
            impl->connectionState = ConnectError;
        }
        reset();
        impl->socket.disconnectFromHost();
        emit error();
    }
    catch (...) {
        impl->errorString = "Unknown exception during asyncData()";
        qCritical() << impl->errorString;
        impl->socketValid = false;
        if (impl->connectionState == Connected) {
            impl->connectionState = ConnectedError;
        } else {
            impl->connectionState = ConnectError;
        }
        reset();
        impl->socket.disconnectFromHost();
        emit error();
    }
}

/****************************************************************************/
