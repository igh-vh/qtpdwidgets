/****************************************************************************
**
** QML-Widgets for qtPdWidgets
**
** Copyright (C) 2021 Wilhelm Hagemeister
** Contact: hm@igh.de
**
** 
**
****************************************************************************/

import QtQuick 2.7         
import QtQuick.Controls 2.3

import de.igh.pd 2.0

/** Simple Bar without scale but with a label inside showing the value
 */

Control {
    id:control
    /** variable: var
     *
     * connection to the process variable: to be used like
     *
     * @code{.qml}
     * 
     * variable {
     *     connection: {
     *         "process":pdProcess,
     *         "path":"/control/value",
     *         "scale":1,
     *         "offset":0,
     *         "period":0.1,
     *         "transmission":Pd.periodic
     *     }
     * }
     * @endcode
     */
    property alias variable: scalar
    property alias connection: scalar.connection
    
    /** type:string
     * Suffix is appended to the value without space!
     */
    property var suffix:""
    
    /** type:int
     * Number of decimals digitals for the label
     */
    property var decimals:2
    
    /** type:bool
     * Draw bars originating from zero otherwise from the minium. 
     */
    property bool startZero:true

    property int orientation:Qt.Horizontal
    /** type:bool
     * Show the value label if true.
     */
    property alias labelVisible:label.visible

    /** type:int
     * pixes size for label; applies only for vertical orientation!
     */
    property int fontPixelSize:12 
    
    /** type:doube
     * This property holds the starting value of the control. Default: 0
     */
    property double from: 0
    
    /** type:double
     * This property holds the end value of the control. Default: 100
     */
     property double to: 100

    /** type:var
     * color of bar
     */
    property color color: control.palette.dark

    /** type:var
     * color of background
     */
    property color backgroundColor: control.palette.button

    /** type:double
     * The position is expressed as a fraction of the value, 
     * in the range 0.0 - 1.0.
     */
    readonly property double visualPosition: 1.0 * (value - from)/(to - from)

    /** type: var
     * Raw Process value
     */
    readonly property alias value: scalar.value

    enabled:scalar.connected

    background: Rectangle {
	color: control.backgroundColor
	implicitWidth: orientation==Qt.Horizontal?100:20
	implicitHeight: orientation==Qt.Horizontal?20:100
    }
    
    contentItem: Item {
	Rectangle {
	    color: control.color
            function scaleOrigin(l) {
		if(control.from < 0 && control.startZero) {
		    return (Math.min(control.visualPosition,
				     control.from/(control.from - control.to)) * l)
		} else {
		    return 0
		}
	    }
	    
            function scale(l) {
		if(control.from < 0 && control.startZero) {
		    return (Math.abs(control.visualPosition-
				     control.from/(control.from - control.to)) * l)
		} else {
		    return control.visualPosition * l
		}
	    }
            x: {
		if(control.orientation == Qt.Horizontal) {
		    return scaleOrigin(parent.width)
		} else {  //vertical
		    return 0
		}
            }

            width: {
		if(control.orientation == Qt.Horizontal) {
		    return scale(parent.width)
		} else {
		    return parent.width
		}
	    }
	    
	    y: {
		if(control.orientation == Qt.Horizontal) {
		    return 1
		} else {
		    return parent.height - height - scaleOrigin(parent.height) 
		}
	    }
            height: {
		if(control.orientation == Qt.Horizontal) {
		    return parent.height-2
		} else {
		    return scale(parent.height)
		}
	    }
	}
	Label {
	    id:label
            anchors.centerIn:parent
            text:scalar.value.toFixed(control.decimals)+control.suffix
            font.pixelSize: {
		if(control.orientation == Qt.Horizontal) {
		    return control.height*8/10
		} else {
		    return control.fontPixelSize
		}
	    }
	}
    }
    PdScalar {
	id:scalar
    }
} //Progressbar 
