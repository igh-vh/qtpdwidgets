/*****************************************************************************
 *
 * Copyright (C) 2009 - 2019  Florian Pose <fp@igh.de>
 *
 * This file is part of the QtPdWidgets library.
 *
 * The QtPdWidgets library is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * The QtPdWidgets library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser
 * General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with the QtPdWidgets Library. If not, see
 * <http://www.gnu.org/licenses/>.
 *
 ****************************************************************************/

#include "Tank.h"
using Pd::Tank;
using Pd::TankMedium;

#include "ScalarSubscriber.h"

#include <QtGui>

/****************************************************************************/

#define BORDER 2
#define PAD 3

#define DEFAULT_MEDIUM_COLOR QColor(0, 0, 255, 192)

#define DEFAULT_STYLE VerticalCylinder
#define DEFAULT_LABEL_WIDTH 100
#define DEFAULT_LABEL_POSITION Right
#define DEFAULT_CAP_HEIGHT 50
#define DEFAULT_MAX_LEVEL 1.0
#define DEFAULT_MAX_VOLUME 1.0
#define DEFAULT_LEVEL_DECIMALS 3
#define DEFAULT_VOLUME_DECIMALS 3
#define DEFAULT_BACKGROUND_COLOR Qt::white
#define DEFAULT_LEVEL_SUFFIX " m"
#define DEFAULT_VOLUME_SUFFIX " m³"

/****************************************************************************/

struct Tank::Impl
{
    Impl(Tank *);

    Tank * const tank;

    Style style;
    int labelWidth;
    LabelPosition labelPosition;
    int capHeight;
    double maxLevel;
    double maxVolume;
    int levelDecimals;
    int volumeDecimals;
    QColor backgroundColor;
    QString levelSuffix;
    QString volumeSuffix;

    typedef QList<TankMedium *> MediaList;
    MediaList media;

    QRect tankRect;
    QRect labelArea;
    QRectF labelRect;
    QPainterPath background;
    QPainterPath foreground;

    void updateLayout();
    void updatePhase();
    void drawVerticalCylinder();
    void drawHorizontalCylinder();
    void drawCuboid();
    void paint();
};

/****************************************************************************/

struct TankMedium::Impl
{
    Impl(TankMedium *, Tank *);
    ~Impl();

    TankMedium * const medium;
    Tank * const tank;

    QColor color;

    struct Value:
        public ScalarSubscriber
    {
        Value(TankMedium::Impl *mediumImpl):
            mediumImpl{mediumImpl},
            dataPresent{false},
            value{0.0}
        {}

        TankMedium::Impl * const mediumImpl;
        bool dataPresent;
        double value;

        void newValues(std::chrono::nanoseconds) override
        {
            double v;
            PdCom::details::copyData(&v,
                    PdCom::details::TypeInfoTraits<double>::type_info.type,
                    getData(), getVariable().getTypeInfo().type, 1);
            v = v * scale + offset;

            if (dataPresent) {
                double newValue;

                if (getFilterConstant() > 0.0) {
                    newValue = getFilterConstant() * (v - value) + value;
                } else {
                    newValue = v;
                }

                value = newValue;
            } else {
                value = v; // bypass filter
                dataPresent = true;
            }

            mediumImpl->tank->impl->updatePhase();
            mediumImpl->tank->update();
        }

        void stateChange(PdCom::Subscription::State state) override
        {
            if (state != PdCom::Subscription::State::Active) {
                dataPresent = false;
                mediumImpl->tank->update();
            }
        }

    }
    level, volume;

    QPainterPath phase;
    QPainterPath surface;
    double height;

    QString label() const;

    void updateVerticalCylinderPhase(float, QRectF, QRectF);
    void updateHorizontalCylinderPhase(float, QRectF, QRectF);
    void updateCuboidPhase(float);
};

/****************************************************************************/

struct Label {
    TankMedium *medium;
    double height;
    unsigned int group;
    double movedHeight;
    bool found;

    bool operator<(const Label &other) const {
        return height < other.height;
    }
};

/****************************************************************************/

TankMedium::TankMedium(
        Tank *tank
        ):
    impl(std::unique_ptr<Impl>(new Impl(this, tank)))
{
}

/****************************************************************************/

TankMedium::~TankMedium()
{
    clearVolumeVariable();
    clearLevelVariable();
}

/****************************************************************************/

QColor TankMedium::getColor() const
{
    return impl->color;
}

/****************************************************************************/

void TankMedium::setColor(QColor c)
{
    if (c == impl->color) {
        return;
    }

    impl->color = c;
    impl->tank->update();
}

/****************************************************************************/

/** Subscribes to a ProcessVariable.
 */
void TankMedium::setLevelVariable(
        PdCom::Variable pv,
        const PdCom::Selector &selector,
        const Transmission &transmission,
        double scale,
        double offset,
        double tau
        )
{
    clearLevelVariable();

    if (pv.empty()) {
        return;
    }

    impl->level.setVariable(pv, selector, transmission, scale, offset, tau);
}

/****************************************************************************/

/** Subscribes to a ProcessVariable.
 */
void TankMedium::setLevelVariable(
        PdCom::Process *process,
        const QString &path,
        const PdCom::Selector &selector,
        const Transmission &transmission,
        double scale,
        double offset,
        double tau
        )
{
    clearLevelVariable();

    if (not process or path.isEmpty()) {
        return;
    }

    impl->level.setVariable(process, path, selector, transmission, scale,
            offset, tau);
}

/****************************************************************************/

/** Subscribes to a ProcessVariable.
 */
void TankMedium::setVolumeVariable(
        PdCom::Variable pv,
        const PdCom::Selector &selector,
        const Transmission &transmission,
        double scale,
        double offset,
        double tau
        )
{
    clearVolumeVariable();

    if (pv.empty()) {
        return;
    }

    impl->volume.setVariable(pv, selector, transmission, scale, offset, tau);
}

/****************************************************************************/

/** Subscribes to a ProcessVariable.
 */
void TankMedium::setVolumeVariable(
        PdCom::Process *process,
        const QString &path,
        const PdCom::Selector &selector,
        const Transmission &transmission,
        double scale,
        double offset,
        double tau
        )
{
    clearVolumeVariable();

    if (not process or path.isEmpty()) {
        return;
    }

    impl->volume.setVariable(process, path, selector, transmission, scale,
            offset, tau);
}

/****************************************************************************/

/** Unsubscribe from a Variable.
 */
void TankMedium::clearLevelVariable()
{
    impl->level.clearVariable();
    impl->tank->update();
}

/****************************************************************************/

/** Unsubscribe from a Variable.
 */
void TankMedium::clearVolumeVariable()
{
    impl->volume.clearVariable();
    impl->tank->update();
}

/*****************************************************************************
 * Implementation
 ****************************************************************************/

TankMedium::Impl::Impl(TankMedium *medium, Tank *tank):
    medium(medium),
    tank(tank),
    color(DEFAULT_MEDIUM_COLOR),
    level{this},
    volume{this},
    height(0.0)
{
}

/****************************************************************************/

TankMedium::Impl::~Impl()
{
}

/****************************************************************************/

QString TankMedium::Impl::label() const
{
    QString label;

    bool first = true;
    if (level.dataPresent) {
        label += QLocale().toString(level.value, 'f',
                tank->impl->levelDecimals);
        label += tank->impl->levelSuffix;
        first = false;
    }
    if (volume.dataPresent) {
        if (!first) {
            label += "\n";
        }
        label += QLocale().toString(volume.value, 'f',
                tank->impl->volumeDecimals);
        label += tank->impl->volumeSuffix;
    }

    return label;
}

/****************************************************************************/

void TankMedium::Impl::updateVerticalCylinderPhase(float prevHeight,
        QRectF arcRect, QRectF bottomPhaseRect)
{
    QRect tankRect = tank->impl->tankRect;
    int capHeight = tank->impl->capHeight;

    float relLevel = 0.0;
    if (level.dataPresent) {
        relLevel = level.value / tank->impl->maxLevel;
    }
    else if (volume.dataPresent) {
        relLevel = volume.value / tank->impl->maxVolume;
    }
    if (relLevel < 0.0) {
        relLevel = 0.0;
    }
    else if (relLevel > 1.0) {
        relLevel = 1.0;
    }

    height = relLevel * (tankRect.height() - PAD);

    QRectF phaseRect = arcRect.adjusted(PAD, 0, -PAD, 0);
    phaseRect.moveTop(tankRect.bottom() + 1 - height - capHeight * 5 / 4);

    QRectF interRect(tankRect);
    interRect.setTop(tankRect.bottom() + 1 - height);
    QPainterPath interPath;
    interPath.addRect(interRect);

    QRectF surfaceRect;
    float R = phaseRect.width() / 2.0;
    float H = capHeight - PAD;
    float hs = H - height;
    float al;
    if (hs > 0.0) { // not yet in cylindrical section
        float r = sqrt(H * H - hs * hs) * R / H;
        float c = phaseRect.left() + R;
        al = atan2(r * H / R, hs) * 180.0 / M_PI;
        float sh = capHeight / 2 * sin(al * M_PI / 180.0);
        surfaceRect = QRectF(c - r,
                tankRect.bottom() + 1 - PAD - height - sh / 2.0,
                2 * r, sh);
    }
    else {
        surfaceRect = phaseRect;
        surfaceRect.moveTop(tankRect.bottom() + 1 -
                PAD - height - capHeight / 4);
        al = 90.0;
    }

    QRectF prevSurfaceRect = phaseRect;
    prevSurfaceRect.moveTop(tankRect.bottom() + 1
            - PAD - prevHeight - capHeight / 4);

    phase = QPainterPath();
    if (prevHeight == 0.0) {
        phase.moveTo(QPoint(surfaceRect.left(),
                    tankRect.bottom() + 1 - PAD - height)); // left
        if (hs < 0.0) {
            phase.lineTo(bottomPhaseRect.left(), // left edge
                    tankRect.bottom() + 1 - capHeight);
        }
        phase.arcTo(bottomPhaseRect, 270.0 - al, 2.0 * al); // bottom curve
        if (hs < 0.0) {
            phase.lineTo(surfaceRect.right(),
                    tankRect.bottom() + 1 - PAD - height); // right edge
        }
        phase.arcTo(surfaceRect, 0.0, -180.0); // back
    }
    else {
        phase.moveTo(QPoint(surfaceRect.left(),
                    tankRect.bottom() + 1 - PAD - height)); // left
        phase.lineTo(bottomPhaseRect.left(), // left edge
                tankRect.bottom() + 1 - PAD - prevHeight);
        phase.arcTo(prevSurfaceRect, 180.0, 180.0); // bottom curve
        phase.lineTo(surfaceRect.right(),
                tankRect.bottom() + 1 - PAD - height); // right edge
        phase.arcTo(surfaceRect, 0.0, -180.0); // back
    }

    surface = QPainterPath();
    surface.addEllipse(surfaceRect);
}

/****************************************************************************/

void TankMedium::Impl::updateHorizontalCylinderPhase(float prevHeight,
        QRectF arcRect, QRectF sideRect)
{
    Q_UNUSED(arcRect);
    Q_UNUSED(prevHeight); // FIXME

    QRect tankRect = tank->impl->tankRect;
    int capHeight = tank->impl->capHeight;

    float relLevel = 0.0;
    if (level.dataPresent) {
        relLevel = level.value / tank->impl->maxLevel;
    }
    else if (volume.dataPresent) {
        relLevel = volume.value / tank->impl->maxVolume;
    }
    if (relLevel < 0.0) {
        relLevel = 0.0;
    }
    else if (relLevel > 1.0) {
        relLevel = 1.0;
    }

    double maxHeight = tankRect.height() - 2 * PAD;

    height = relLevel * maxHeight;

    phase = QPainterPath();

    // lower line
    phase.moveTo(tankRect.bottomLeft() + QPoint(capHeight, 1 - PAD));
    phase.lineTo(tankRect.bottomRight() + QPoint(-capHeight, 1 - PAD));

    sideRect.moveLeft(tankRect.right() + 1 - 2 * (capHeight - PAD) - PAD);

    double al = 90.0;
    double H = maxHeight / 2.0;

    surface = QPainterPath();

    double r = H - height;
    al = acos(r / H) * 180.0 / M_PI;
    phase.arcTo(sideRect, 270.0, al);

    double w = capHeight / 2.0 * sin(al * M_PI / 180.0);
    double f;
    if (r != 0.0) {
        f = r * tan(al * M_PI / 180);
    }
    else {
        f = H;
    }
    double c = capHeight - PAD;
    double x = c * f / H;
    double t = height - w / 2.0;
    double q = H - t;
    double o = sqrt(H * H - q * q);
    double i = capHeight / 4.0; // FIXME
    double u = i * o / H;
    QRect bowRect;
    bowRect.setLeft(tankRect.right() + 1 - capHeight - 2.0 * u - x);
    bowRect.setWidth(2.0 * (u + x));
    bowRect.setTop(tankRect.bottom() + 1 - PAD - height - w / 2.0);
    bowRect.setHeight(w);
    phase.arcTo(bowRect, 0.0, -90.0);
    surface.moveTo(QPoint(bowRect.right(),
                tankRect.bottom() + 1 - PAD - height));
    surface.arcTo(bowRect, 0.0, -90.0);

    phase.lineTo(tankRect.bottomLeft()
            + QPoint(capHeight - u, 1 - PAD - height + w / 2.0));
    surface.lineTo(tankRect.bottomLeft()
            + QPoint(capHeight - u, 1 - PAD - height + w / 2.0));

    bowRect.setLeft(tankRect.left() + capHeight - x);
    bowRect.setWidth(2 * (x - u));
    phase.arcTo(bowRect, 270.0, -90.0);
    surface.arcTo(bowRect, 270.0, -90.0);

    sideRect.moveLeft(tankRect.left() + PAD);
    phase.arcTo(sideRect, 270.0 - al, al);

    bowRect.setWidth(2 * (x + u));
    surface.arcTo(bowRect, 180.0, -90.0);

    surface.lineTo(tankRect.bottomRight()
            + QPoint(1 - capHeight + u, - PAD - height - w / 2.0)); // FIXME

    bowRect.setLeft(tankRect.right() + 2 - capHeight + 2.0 * u - x);
    bowRect.setWidth(2.0 * (x - u));
    surface.arcTo(bowRect, 90.0, -90.0);
}

/****************************************************************************/

void TankMedium::Impl::updateCuboidPhase(float prevHeight)
{
    QRect tankRect = tank->impl->tankRect;
    int capHeight = tank->impl->capHeight;

    float relLevel = 0.0;
    if (level.dataPresent) {
        relLevel = level.value / tank->impl->maxLevel;
    }
    else if (volume.dataPresent) {
        relLevel = volume.value / tank->impl->maxVolume;
    }
    if (relLevel < 0.0) {
        relLevel = 0.0;
    }
    else if (relLevel > 1.0) {
        relLevel = 1.0;
    }

    height = relLevel * (tankRect.height() - capHeight - 2 * PAD);

    phase = QPainterPath();

    phase.moveTo(tankRect.bottomLeft() + QPoint(PAD, -prevHeight - PAD));
    phase.lineTo(tankRect.bottomRight() +
            QPoint(-capHeight - PAD, -prevHeight - PAD));
    phase.lineTo(tankRect.bottomRight() +
            QPoint(-PAD, -capHeight - prevHeight - PAD));
    phase.lineTo(tankRect.bottomRight() +
            QPoint(-PAD, -capHeight - PAD - height));
    phase.lineTo(tankRect.bottomRight() +
            QPoint(-capHeight - PAD, -PAD - height));
    phase.lineTo(tankRect.bottomLeft() + QPoint(PAD, -height - PAD));
    phase.lineTo(tankRect.bottomLeft() + QPoint(PAD, -prevHeight - PAD));

    phase.moveTo(tankRect.bottomRight() +
            QPoint(-capHeight - PAD, -PAD - height));
    phase.lineTo(tankRect.bottomRight() +
            QPoint(-capHeight - PAD, -prevHeight - PAD));


    surface = QPainterPath();
    surface.moveTo(tankRect.bottomLeft() + QPoint(PAD, -height - PAD));
    surface.lineTo(tankRect.bottomRight() +
            QPoint(-capHeight - PAD, -PAD - height));
    surface.lineTo(tankRect.bottomRight() +
            QPoint(-PAD, -capHeight - PAD - height));
    surface.lineTo(tankRect.bottomLeft() +
            QPoint(PAD + capHeight, -capHeight - PAD - height));
    surface.lineTo(tankRect.bottomLeft() + QPoint(PAD, -height - PAD));
}

/****************************************************************************/

/****************************************************************************/

/*****************************************************************************
 * Tank
 ****************************************************************************/

Tank::Tank(
        QWidget *parent
        ):
    QFrame(parent),
    impl(std::unique_ptr<Impl>(new Impl(this)))
{
}

/****************************************************************************/

Tank::~Tank()
{
    clearMedia();
}

/****************************************************************************/

/** Gives a hint aboute the optimal size.
 */
QSize Tank::sizeHint() const
{
    QSize ret;

    switch (impl->style) {
        case VerticalCylinder:
            ret = QSize(250, 150);
            break;
        case HorizontalCylinder:
            ret = QSize(150, 350);
            break;
        case Cuboid:
            ret = QSize(250, 250);
            break;
    };

    return ret;
}

/****************************************************************************/

Tank::Style Tank::getStyle() const
{
    return impl->style;
}

/****************************************************************************/

void Tank::setStyle(Style s)
{
    if (s == impl->style) {
        return;
    }

    impl->style = s;
    updateGeometry();
    impl->updateLayout();
}

/****************************************************************************/

void Tank::resetStyle()
{
    setStyle(DEFAULT_STYLE);
}

/****************************************************************************/

int Tank::getLabelWidth() const
{
    return impl->labelWidth;
}

/****************************************************************************/

void Tank::setLabelWidth(int width)
{
    if (width == impl->labelWidth) {
        return;
    }

    impl->labelWidth = width;
    impl->updateLayout();
}

/****************************************************************************/

void Tank::resetLabelWidth()
{
    setLabelWidth(DEFAULT_LABEL_WIDTH);
}

/****************************************************************************/

Tank::LabelPosition Tank::getLabelPosition() const
{
    return impl->labelPosition;
}

/****************************************************************************/

void Tank::setLabelPosition(LabelPosition pos)
{
    if (pos == impl->labelPosition || (pos != Right && pos != Left)) {
        return;
    }

    impl->labelPosition = pos;
    impl->updateLayout();
}

/****************************************************************************/

void Tank::resetLabelPosition()
{
    setLabelPosition(DEFAULT_LABEL_POSITION);
}

/****************************************************************************/

int Tank::getCapHeight() const
{
    return impl->capHeight;
}

/****************************************************************************/

void Tank::setCapHeight(int height)
{
    if (height == impl->capHeight || height < 0) {
        return;
    }

    impl->capHeight = height;
    impl->updateLayout();
}

/****************************************************************************/

void Tank::resetCapHeight()
{
    setCapHeight(DEFAULT_CAP_HEIGHT);
}

/****************************************************************************/

double Tank::getMaxLevel() const
{
    return impl->maxLevel;
}

/****************************************************************************/

void Tank::setMaxLevel(double level)
{
    if (level <= 0.0 or level == impl->maxLevel) {
        return;
    }

    impl->maxLevel = level;
    impl->updatePhase();
}

/****************************************************************************/

void Tank::resetMaxLevel()
{
    setMaxLevel(DEFAULT_MAX_LEVEL);
}

/****************************************************************************/

double Tank::getMaxVolume() const
{
    return impl->maxVolume;
}

/****************************************************************************/

void Tank::setMaxVolume(double vol)
{
    if (vol <= 0.0 or vol == impl->maxVolume) {
        return;
    }

    impl->maxVolume = vol;
    impl->updatePhase();
}

/****************************************************************************/

void Tank::resetMaxVolume()
{
    setMaxVolume(DEFAULT_MAX_VOLUME);
}

/****************************************************************************/

int Tank::getLevelDecimals() const
{
    return impl->levelDecimals;
}

/****************************************************************************/

void Tank::setLevelDecimals(int d)
{
    if (d == impl->levelDecimals) {
        return;
    }

    impl->levelDecimals = d;
    update();
}

/****************************************************************************/

void Tank::resetLevelDecimals()
{
    setLevelDecimals(DEFAULT_LEVEL_DECIMALS);
}

/****************************************************************************/

int Tank::getVolumeDecimals() const
{
    return impl->volumeDecimals;
}

/****************************************************************************/

void Tank::setVolumeDecimals(int d)
{
    if (d == impl->volumeDecimals) {
        return;
    }

    impl->volumeDecimals = d;
    update();
}

/****************************************************************************/

void Tank::resetVolumeDecimals()
{
    setVolumeDecimals(DEFAULT_VOLUME_DECIMALS);
}

/****************************************************************************/

void Tank::setBackgroundColor(QColor c)
{
    if (impl->backgroundColor == c) {
        return;
    }

    impl->backgroundColor = c;
    update();
}

/****************************************************************************/

QColor Tank::getBackgroundColor() const
{
    return impl->backgroundColor;
}

/****************************************************************************/

void Tank::resetBackgroundColor()
{
    setBackgroundColor(DEFAULT_BACKGROUND_COLOR);
}

/****************************************************************************/

const QString &Tank::getLevelSuffix() const
{
    return impl->levelSuffix;
}

/****************************************************************************/

void Tank::setLevelSuffix(const QString &str)
{
    if (str == impl->levelSuffix) {
        return;
    }

    impl->levelSuffix = str;
    update();
}

/****************************************************************************/

void Tank::resetLevelSuffix()
{
    setLevelSuffix(DEFAULT_LEVEL_SUFFIX);
}

/****************************************************************************/

const QString &Tank::getVolumeSuffix() const
{
    return impl->volumeSuffix;
}

/****************************************************************************/

void Tank::setVolumeSuffix(const QString &str)
{
    if (str == impl->volumeSuffix) {
        return;
    }

    impl->volumeSuffix = str;
    update();
}

/****************************************************************************/

void Tank::resetVolumeSuffix()
{
    setLevelSuffix(DEFAULT_VOLUME_SUFFIX);
}

/****************************************************************************/

TankMedium *Tank::addMedium()
{
    TankMedium *m = new TankMedium(this);
    impl->media.append(m);
    return m;
}

/****************************************************************************/

void Tank::clearMedia()
{
    for (Impl::MediaList::const_iterator i = impl->media.begin();
            i != impl->media.end(); i++) {
        delete *i;
    }

    impl->media.clear();
}

/****************************************************************************/

/** Event function.
 */
bool Tank::event(
        QEvent *event /**< Event flags. */
        )
{
    return QFrame::event(event);
}

/****************************************************************************/

void Tank::resizeEvent(QResizeEvent *)
{
    impl->updateLayout();
}

/****************************************************************************/

void spreadGroup(QList<Label> &list,
        unsigned int group, int labelHeight)
{
    int sumHeight = 0;
    unsigned int count = 0;

    for (QList<Label>::const_iterator labelData = list.begin();
            labelData != list.end(); labelData++) {
        if (labelData->group == group) {
            sumHeight += labelData->height;
            count++;
        }
    }

    if (!count) {
        return;
    }

    int off = sumHeight / count - (labelHeight * (count - 1) / 2);
    int index = 0;

    for (QList<Label>::iterator labelData = list.begin();
            labelData != list.end(); labelData++) {
        if (labelData->group == group) {
            labelData->movedHeight = off + labelHeight * index++;
        }
    }
}

/****************************************************************************/

void Tank::paintEvent(QPaintEvent *event)
{
    QFrame::paintEvent(event);
    impl->paint();
}

/*****************************************************************************
 * Implementation
 ****************************************************************************/

Tank::Impl::Impl(Tank *tank):
    tank(tank),
    style(DEFAULT_STYLE),
    labelWidth(DEFAULT_LABEL_WIDTH),
    labelPosition(DEFAULT_LABEL_POSITION),
    capHeight(DEFAULT_CAP_HEIGHT),
    maxLevel(DEFAULT_MAX_LEVEL),
    maxVolume(DEFAULT_MAX_VOLUME),
    levelDecimals(DEFAULT_LEVEL_DECIMALS),
    volumeDecimals(DEFAULT_VOLUME_DECIMALS),
    backgroundColor(DEFAULT_BACKGROUND_COLOR),
    levelSuffix(DEFAULT_LEVEL_SUFFIX),
    volumeSuffix(DEFAULT_VOLUME_SUFFIX)
{
    labelRect.setHeight(40);
}

/****************************************************************************/

void Tank::Impl::updateLayout()
{
    QRect borderRect =
        tank->contentsRect().adjusted(BORDER, BORDER, -BORDER, -BORDER);

    tankRect = borderRect;
    labelArea = borderRect;

    if (labelPosition == Right) {
        tankRect.setWidth(borderRect.width() - labelWidth);
        labelArea.setLeft(borderRect.right() + 1 - labelWidth);
        labelRect.moveLeft(labelArea.left() + 10);
        labelRect.setWidth(labelWidth - 20);
    }
    else {
        tankRect.setLeft(borderRect.left() + labelWidth);
        labelRect.moveLeft(labelArea.left() + 10);
        labelRect.setWidth(labelWidth - 20);
    }

    background = QPainterPath(); // clear
    foreground = QPainterPath();

    background.setFillRule(Qt::WindingFill);

    switch (style) {
        case VerticalCylinder:
            drawVerticalCylinder();
            break;
        case HorizontalCylinder:
            drawHorizontalCylinder();
            break;
        case Cuboid:
            drawCuboid();
            break;
    }

    updatePhase();
    tank->update();
}

/****************************************************************************/

void Tank::Impl::updatePhase()
{
    float lastHeight = 0.0;

    switch (style) {
        case VerticalCylinder:
            {
                QRectF arcRect(tankRect);
                arcRect.setHeight(capHeight / 2);

                QRectF bottomRect(tankRect);
                bottomRect.setTop(bottomRect.bottom() + 1 - 2 * capHeight);

                QRectF bottomPhaseRect =
                    bottomRect.adjusted(PAD, PAD, -PAD, -PAD);

                for (MediaList::const_iterator i = media.begin();
                        i != media.end(); i++) {
                    TankMedium *m = *i;

                    m->impl->updateVerticalCylinderPhase(lastHeight,
                            arcRect, bottomPhaseRect);
                    lastHeight = m->impl->height;
                }
            }
            break;

        case HorizontalCylinder:
            {
                QRectF arcRect(tankRect);
                arcRect.setWidth(capHeight / 2);

                QRectF sideRect(tankRect);
                sideRect.setWidth(2 * capHeight);

                QRectF sidePhaseRect =
                    sideRect.adjusted(PAD, PAD, -PAD, -PAD);

                for (MediaList::const_iterator i = media.begin();
                        i != media.end(); i++) {
                    TankMedium *m = *i;

                    m->impl->updateHorizontalCylinderPhase(lastHeight,
                            arcRect, sidePhaseRect);
                    lastHeight = m->impl->height;
                }
            }
            break;

        case Cuboid:
            for (MediaList::const_iterator i = media.begin();
                    i != media.end(); i++) {
                TankMedium *m = *i;

                m->impl->updateCuboidPhase(lastHeight);
                lastHeight = m->impl->height;
            }
            break;
    }
}

/****************************************************************************/

void Tank::Impl::drawVerticalCylinder()
{
    background.moveTo(tankRect.topLeft() + QPoint(0, capHeight));
    background.lineTo(tankRect.bottomLeft() + QPoint(0, -capHeight));

    QRect bottomRect(tankRect);
    bottomRect.setTop(bottomRect.bottom() + 1 - 2 * capHeight);
    background.arcTo(bottomRect, 180.0, 180.0);

    background.lineTo(tankRect.topRight() + QPoint(1, capHeight)); // FIXME

    QRect topRect(tankRect);
    topRect.setBottom(topRect.top() + 2 * capHeight);
    background.arcTo(topRect, 0.0, 180.0);

    QRect arcRect(tankRect);
    arcRect.setHeight(capHeight / 2);

    // upper arc
    arcRect.moveTop(tankRect.top() + capHeight * 3 / 4);
    background.moveTo(tankRect.topRight() + QPoint(0, capHeight));
    background.arcTo(arcRect, 0.0, 180.0);

    foreground.moveTo(background.currentPosition());
    foreground.arcTo(arcRect, 180.0, 180.0);

    // lower arc
    arcRect.moveTop(tankRect.bottom() + 1 - capHeight * 5 / 4);
    background.moveTo(tankRect.bottomRight() - QPoint(0, capHeight));
    background.arcTo(arcRect, 0.0, 180.0);

    foreground.moveTo(background.currentPosition());
    foreground.arcTo(arcRect, 180.0, 180.0);
}

/****************************************************************************/

void Tank::Impl::drawHorizontalCylinder()
{
    // lower line
    background.moveTo(tankRect.bottomLeft() + QPoint(capHeight, 1));
    background.lineTo(tankRect.bottomRight() + QPoint(-capHeight, 1));

    // right outer arc
    QRect rightRect(tankRect);
    rightRect.setLeft(rightRect.right() + 1 - 2 * capHeight);
    background.arcTo(rightRect, 270.0, 180.0);

    // upper line
    background.lineTo(tankRect.topLeft() + QPoint(capHeight, 0));

    // left outer arc
    QRect leftRect(tankRect);
    leftRect.setRight(leftRect.left() + 2 * capHeight);
    background.arcTo(leftRect, 90.0, 180.0);
    QRect arcRect(tankRect);
    arcRect.setWidth(capHeight / 2);

    // left inner arc
    arcRect.moveLeft(tankRect.left() + capHeight * 3 / 4);
    background.moveTo(tankRect.bottomLeft() + QPoint(capHeight, 0));
    background.arcTo(arcRect, 270.0, 180.0);

    foreground.moveTo(background.currentPosition());
    foreground.arcTo(arcRect, 90.0, 180.0);

    // right inner arc
    arcRect.moveLeft(tankRect.right() + 1 - capHeight * 5 / 4);
    background.moveTo(tankRect.bottomRight() + QPoint(-capHeight, 0));
    background.arcTo(arcRect, 270.0, 180.0);

    foreground.moveTo(background.currentPosition());
    foreground.arcTo(arcRect, 90.0, 180.0);
}

/****************************************************************************/

void Tank::Impl::drawCuboid()
{
    background.moveTo(tankRect.topLeft() + QPoint(0, capHeight));
    background.lineTo(tankRect.topLeft() + QPoint(capHeight, 0));
    background.moveTo(tankRect.bottomLeft());
    background.lineTo(tankRect.bottomLeft() + QPoint(capHeight, -capHeight));

    QRect rect(tankRect);
    rect.setLeft(rect.left() + capHeight);
    rect.setBottom(rect.bottom() - capHeight);
    background.addRect(rect);

    foreground.moveTo(tankRect.topRight() + QPoint(-capHeight, capHeight));
    foreground.lineTo(tankRect.topRight());
    foreground.moveTo(tankRect.bottomRight() + QPoint(-capHeight, 0));
    foreground.lineTo(tankRect.bottomRight() + QPoint(0, -capHeight));

    rect = tankRect;
    rect.setTop(rect.top() + capHeight);
    rect.setRight(rect.right() - capHeight);
    foreground.addRect(rect);
}

/****************************************************************************/

void Tank::Impl::paint()
{
    QList<Label> labelList;
    unsigned int group = 0;

    QPainter p(tank);
    QPen stylePen(p.pen());

    p.setRenderHint(QPainter::Antialiasing);

    QPen backgroundPen(stylePen);
    backgroundPen.setWidth(2);
    p.setPen(backgroundPen);
    p.setBrush(backgroundColor);

    p.drawPath(background);

    for (MediaList::const_iterator i = media.begin();
            i != media.end(); i++) {
        TankMedium *m = *i;

        QPen phasePen(stylePen);
        phasePen.setWidth(1);
        p.setPen(phasePen);

        p.setBrush(m->getColor());
        p.drawPath(m->impl->phase);

        QPainterPath surfacePath = m->impl->surface;
        if (!surfacePath.isEmpty()) {
            QColor surfaceColor = m->getColor().lighter(120);
            p.setBrush(surfaceColor);
            p.drawPath(surfacePath);
        }

        if (labelRect.isValid()) {
            Label labelData;
            labelData.medium = m;
            labelData.height = m->impl->height;
            labelData.movedHeight = labelData.height;
            labelData.group = group++;
            labelList.append(labelData);
        }
    }

    p.setPen(backgroundPen);
    p.setBrush(QBrush());
    p.drawPath(foreground);

    // sort labels by minimum value
    std::stable_sort(labelList.begin(), labelList.end());

    // eliminate overlaps
    bool foundOverlaps;
    do {
        foundOverlaps = false;
        double lastHeight = -1.0, lastGroup = 0;

        for (QList<Label>::iterator labelData =
                labelList.begin();
                labelData != labelList.end(); labelData++) {

            if (lastHeight >= 0) {
                if (labelData->movedHeight - lastHeight <
                        labelRect.height()) {
                    foundOverlaps = true;
                    labelData->group = lastGroup; // merge groups
                    spreadGroup(labelList, labelData->group,
                            labelRect.height() + 2);
                    break;
                }
            }
            lastHeight = labelData->movedHeight;
            lastGroup = labelData->group;
        }
    }
    while (foundOverlaps);

    // push out-of-range labels into the drawing rect from bottom side
    if (!labelList.empty()) {
        QList<Label>::iterator labelData = labelList.begin();
        int bottom = labelData->movedHeight - labelRect.height() / 2;
        if (bottom < 0) {
            labelData->movedHeight -= bottom;
            double lastHeight = labelData->movedHeight;
            labelData++;
            for (; labelData != labelList.end(); labelData++) {
                if (labelData->movedHeight - lastHeight >= labelRect.height()) {
                    break;
                }
                labelData->movedHeight = lastHeight + labelRect.height();
                lastHeight = labelData->movedHeight;
            }
        }
    }

    // push out-of-range labels into the drawing rect from top side
    if (!labelList.empty()) {
        QList<Label>::iterator labelData = labelList.end();
        labelData--;
        int over = labelData->movedHeight + labelRect.height() / 2
            - (tankRect.height() - 2 * PAD);
        if (over > 0) {
            labelData->movedHeight -= over;
            double lastHeight = labelData->movedHeight;

            while (labelData != labelList.begin()) {
                labelData--;

                if (lastHeight - labelData->movedHeight >=
                        labelRect.height()) {
                    break;
                }
                labelData->movedHeight = lastHeight - labelRect.height();
                lastHeight = labelData->movedHeight;
            }
        }
    }

    // draw labels
    for (QList<Label>::const_iterator labelData =
            labelList.begin();
            labelData != labelList.end(); labelData++) {
        p.setPen(stylePen);
        p.setBrush(tank->palette().color(QPalette::Window));

        QRectF textRect(labelRect);
        textRect.moveTop(tankRect.bottom() + 1 - PAD
                - labelData->movedHeight - labelRect.height() / 2);

        p.drawRoundedRect(textRect, 5.0, 5.0);

        p.drawText(textRect, Qt::AlignCenter,
                labelData->medium->impl->label());
    }
}

/****************************************************************************/
