/*****************************************************************************
 *
 * Copyright (C) 2022 Florian Pose (fp at igh dot de),
 *
 * This file is part of the QtPdWidgets library.
 *
 * The QtPdWidgets library is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * The QtPdWidgets library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser
 * General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with the QtPdWidgets Library. If not, see
 * <http://www.gnu.org/licenses/>.
 *
 ****************************************************************************/

#include "Process.h"

#include "Subscription.h"

#include <QDebug>

using namespace PdCom;

/****************************************************************************/

Process::Process()
{
}

/****************************************************************************/

Process::~Process()
{
}

/****************************************************************************/

void Process::addVariable(Variable *variable)
{
    qDebug() << "Process" << this << "adding variable" << variable
        << "with path" << variable->getPath().c_str();
    variables_.push_back(variable);
}

/****************************************************************************/

Variable *Process::getVariable(const std::string &path)
{
    for (auto var : variables_) {
        if (var->getPath() == path) {
            return var;
        }
    }
    return nullptr;
}

/****************************************************************************/

void Process::addSubscription(Subscription *subscription)
{
    qDebug() << "Process" << this << "adding subscription" << subscription;
    subscriptions_.push_back(subscription);
}

/****************************************************************************/

void Process::mockValue(Variable *variable)
{
    // find subscriptions
    for (auto sub : subscriptions_) {
        if (sub->getVariablePtr() == variable) {
            sub->mockValue();
        }
    }
}

/****************************************************************************/
